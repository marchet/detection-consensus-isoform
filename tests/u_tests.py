#!/usr/bin/env python

#----------------------------------------------------------------------#
#Isoform detection and consensus in ONT long reads using MSA
#Copyright (C) Camille Marchet

#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#contact: camille.marchet@irisa.fr
#----------------------------------------------------------------------#

from __future__ import print_function

try:
    from builtins import zip
    from builtins import str
    from builtins import object
except ImportError:
    pass
import matplotlib.pyplot as pyplot
import argparse
from pathlib import Path
import numpy
import sys, os, importlib
import shlex, subprocess
from subprocess import Popen, PIPE, STDOUT
import re
from collections import Counter
import importlib.machinery






# test 1
def test_getReadsCluster(imported):
	alignment = [("0", (">header_read0", "AAAAAA")),
	             ("1", (">header_read1", "AATAAA")),
	             ("2", (">header_read2", "CCCCCC"))]
	newMsa = imported.MSA()
	newMsa.getMSA(alignment)
	cluster0 = [0,1]
	cluster1 = [2]
	read0Cluster = newMsa.getReadsCluster(cluster0)
	#~ print(read0Cluster)
	read1Cluster = newMsa.getReadsCluster(cluster1)
	#~ print(read1Cluster)
	#~ #read0Cluster = imported.getReadsCluster(fasta, cluster0)
	#~ #read1Cluster = imported.getReadsCluster(fasta, cluster1)
	if len(read0Cluster) < 1 or len(read1Cluster) < 1:
		return False
	else:
		if read0Cluster[0][0] == ">header_read0" and read0Cluster[0][1] == "AAAAAA" and  read0Cluster[1][0] == ">header_read1" and read0Cluster[1][1] == "AATAAA" and read1Cluster[0][0] == ">header_read2" and read1Cluster[0][1] == "CCCCCC":
			return True
		else:
			return False


# test 2
def test_getConsensusFromAlignment(imported):
	alignment = [("0", (">0", "aaaacttct")),
	             ("1", (">1", "aaa..tt..")),
	             ("2", (">2", "caa..tt.t")),
	             ("3", (">3", "aaa.ctt.t")),
	             ("4", (">4", "ac..cct.t")),
	             ("5", (">5", "ac.acct.t"))]
	consensus = imported.getConsensusFromAlignment(alignment)
	if consensus == "aaacttt":
		return True
	else:
		return False




# test 3
def test_getMSA(imported):
	alignment = [("0", (">0", "aaaacttct")),
	             ("1", (">1", "aaa..tt..")),
	             ("2", (">2", "caa..tt.t"))]
	newMsa = imported.MSA()
	newMsa.getMSA(alignment)
	#~ if len(newMsa.reads) != 3:
	if len(newMsa.rows) != 3:
		return False
	for i,line in enumerate(newMsa.rows):
	#~ for i,line in enumerate(newMsa.reads):
		if i == 0:
			if not line == ("0", (">0", "aaaacttct")):
				return False
		if i == 1:
			if not line == ("1", (">1", "aaa..tt..")):
				return False
		if i == 2:
			if not line == ("2", (">2", "caa..tt.t")):
				return False
	return True
	


#test 5
def test_findGapStretches(imported):
	                          #        *                    *
	                          #012345678901234567890123456789012345678
	alignment = [("0", (">0", "aaaat.aaaccca.ccccctcc..ccccctttttttt..")),
	             ("1", (">1", ".aaaaa.atccc.ccaccc.cccgccc.cctttat.ttt")),
	             ("2", (">2", "aa..aaaa......................ttattt.ct"))]
	                          #        *                    *
	newMsa = imported.MSA()
	newMsa.getMSA(alignment)
	#~ newMsa.buildMSAMatrix()
	gaps = newMsa.findGapStretches()
	if len(gaps) != 3:
		return False
	if 0 not in gaps.keys():
		return False
	else:
		if len(gaps[0]) != 4:
			return False
		else:
			if len(gaps[0][0]) != 0 or len(gaps[0][1]) != 0 or len(gaps[0][2]) != 0 or len(gaps[0][3]) != 0 :
				return False
	if 1 not in gaps.keys():
		return False
	else:
		if len(gaps[1]) != 7:
			return False
		else:
			if len(gaps[1][0]) != 0 or len(gaps[1][1]) != 0 or len(gaps[1][2]) != 0 or len(gaps[1][3]) != 0 or len(gaps[1][4]) != 0 or len(gaps[1][5]) != 0 or len(gaps[1][6]) != 0 :
				return False
	if 2 not in gaps.keys():
		return False
	else:
		if len(gaps[2]) != 4:
			return False
		else:
			if len(gaps[2][0]) != 0 or gaps[2][1] != [8,29] or len(gaps[2][2]) != 0 or len(gaps[2][3]) != 0 :
				return False
	return True


#test6
def test_findGapStretches2(imported):
	                          #     *        *    *       *
	                          #012345678901234567890123456789012345678
	alignment = [("0", (">0", "aaaat..........ccca.........tatttcttt.."))]
	newMsa = imported.MSA()
	newMsa.getMSA(alignment)
	#~ newMsa.buildMSAMatrix()
	gaps = newMsa.findGapStretches()
	if len(gaps) != 1:
		return False
	if 0 not in gaps.keys():
		return False
	else:
		if gaps[0][0] != [5, 14] or gaps[0][1] != [19, 27] or len(gaps[0][2]) != 0 :
			print(gaps)
			return False
	return True


#test7
def test_prolongateNstretches(imported):
	                          #     *                                                           *
	                          #01234567890123456789012345678901234567890123456789012345678901234567890123456
	alignment = [("0", (">0", "aaaat.................................ccca........................tatttcttt..")),
				 ("1", (">1", "aaaatcccccccccccccccccccc.............ccca.............ttttttttttttatttcttt.."))]
	newMsa = imported.MSA()
	newMsa.getMSA(alignment)
	#~ newMsa.buildMSAMatrix()
	gaps = newMsa.findGapStretches()
	newMsa.prolongateNstretches(gaps)
	if newMsa.gapStretchesPositions != [5,65]:
		return False
	return True



#test8
def test_findRegions(imported):
		                      #    **                                                           **
	                          #01234567890123456789012345678901234567890123456789012345678901234567890123456
	alignment = [("0", (">0", "aaaat.................................ccca........................tatttcttt..")),
				 ("1", (">1", "aaaatcccccccccccccccccccc.............ccca.............ttttttttttttatttcttt..")),
				 ("2", (">1", "aaaat.................................ccca.........................atttcttt..")),
				 ("3", (">1", "aaaa..................................ccca........................tatttcttt.."))]
	newMsa = imported.MSA()
	newMsa.getMSA(alignment)
	#~ newMsa.buildMSAMatrix()
	gaps = newMsa.findGapStretches()
	newMsa.prolongateNstretches(gaps)
	regions = newMsa.findRegions()
	if not regions == [[4, 5], [65, 66]]:
		return False
	return True


#test 9
def test_findPositionRegions(imported):
		                      #     *                                                           *
	                          #01234567890123456789012345678901234567890123456789012345678901234567890123456
	alignment = [("0", (">0", "aaaat.................................ccca........................tatttcttt..")),
				 ("1", (">1", "aaaatcccccccccccccccccccc.............ccca.............ttttttttttttatttcttt..")),
				 ("2", (">1", "aaaat.................................ccca.........................atttcttt..")),
				 ("3", (">1", "aaaa..................................ccca........................tatttcttt.."))]
	newMsa = imported.MSA()
	newMsa.getMSA(alignment)
	#~ newMsa.buildMSAMatrix()
	gaps = newMsa.findGapStretches()
	newMsa.prolongateNstretches(gaps)
	regions = newMsa.findRegions()
	newMsa.findPositionRegions(regions)
	if newMsa.regionsPositions != [5,65]:
		return False
	return True

#test 10
#here only a few nt before the first gap: we expect only two intervals
def test_getIntervals(imported):
		                      #     *                                                           *
	                          #01234567890123456789012345678901234567890123456789012345678901234567890123456
	alignment = [("0", (">0", "aaaat.................................ccca........................tatttcttt..")),
				 ("1", (">1", "aaaatcccccccccccccccccccc.............ccca.............ttttttttttttatttcttt..")),
				 ("2", (">1", "aaaat.................................ccca.........................atttcttt..")),
				 ("3", (">1", "aaaa..................................ccca........................tatttcttt.."))]
	newMsa = imported.MSA()
	newMsa.getMSA(alignment)
	#~ newMsa.buildMSAMatrix()
	gaps = newMsa.findGapStretches()
	newMsa.prolongateNstretches(gaps)
	regions = newMsa.findRegions()
	newMsa.findPositionRegions(regions)
	newMsa.getIntervals()
	if len(newMsa.intervals) != 2:
		return False
	if 0 not in newMsa.intervals.keys():
		return False
	if 1 not in  newMsa.intervals.keys():
		return False
	return True



	
#test 11
#here we expect 3 intervals
def test_getIntervals2(imported):
		                      #           *                                                           *
	                          #012345678901234567890123456789012345678901234567890123456789012345678901234567890
	alignment = [("0", (">0", "aaaaaaaaaat.................................ccca........................tatttcttt..")),
				 ("1", (">1", "aaaaaaaaaatcccccccccccccccccccc.............ccca.............ttttttttttttatttcttt..")),
				 ("2", (">1", "aaaaaa...at.................................ccca.........................atttcttt..")),
				 ("3", (">1", "aaaa..aaccctt...............................ccca........................tatttcttt.."))]
	newMsa = imported.MSA()
	newMsa.getMSA(alignment)
	#~ newMsa.buildMSAMatrix()
	gaps = newMsa.findGapStretches()
	newMsa.prolongateNstretches(gaps)
	regions = newMsa.findRegions()
	newMsa.findPositionRegions(regions)
	newMsa.getIntervals()
	if len(newMsa.intervals) != 3:
		return False
	if 0 not in newMsa.intervals.keys():
		return False
	if 1 not in  newMsa.intervals.keys():
		return False
	if 2 not in  newMsa.intervals.keys():
		return False
	for k in newMsa.intervals.keys():
		if newMsa.intervals[k]["gap"]:
			if not newMsa.intervals[k]["positions"] == [11, 71]:
				return False
	return True




#test 12
def test_extractSubSequences(imported):
		                          #     *                 *
	                          #012345678901234567890123456789012345678
	alignment = [("0", (">0", "aaaat..........ccca.........tatttcttt.."))]
	newMsa = imported.MSA()
	newMsa.getMSA(alignment)
	#~ newMsa.buildMSAMatrix()
	seq = newMsa.extractSubSequences(9,27)
	if seq != ["......ccca........."]:
		return False
	return True




#test 13
def test_clusterReadsByGapProfile(imported):
		                      #           *                                                           *
	                          #012345678901234567890123456789012345678901234567890123456789012345678901234567890
	alignment = [("0", (">0", "aaaaaaaaaat.................................ccca........................tatttcttt..")),
				 ("1", (">1", "aaaaaaaaaatcccccccccccccccccccc.............ccca.............ttttttttttttatttcttt..")),
				 ("2", (">1", "aaaaaa...at.................................ccca.........................atttcttt..")),
				 ("3", (">1", "aaaa..aaccctt...............................ccca........................tatttcttt.."))]
	newMsa = imported.MSA()
	newMsa.getMSA(alignment)
	#~ newMsa.buildMSAMatrix()
	gaps = newMsa.findGapStretches()
	newMsa.prolongateNstretches(gaps)
	regions = newMsa.findRegions()
	newMsa.findPositionRegions(regions)
	newMsa.getIntervals()
	clusters = newMsa.clusterReadsByGapProfile()
	if len(clusters) != 2:
		return False
	for k in clusters.keys():
		
		if len(clusters[k]) == 3:
			if clusters[k] != [0, 2, 3]:
				return False
		elif  len(clusters[k]) == 1:
			if clusters[k] != [1]:
				return False
		else:
			return False
	return True



#test 14
def test_writeConsensusByGapProfile(imported):
	installDirectory = os.path.dirname(os.path.abspath(sys.argv[0])) + "/.."
	outDirectory = os.path.dirname(os.path.abspath(sys.argv[0]))
	fasta = [('0','aaaaaaaacaaaaaaaaaaattttttttttttttttttttt'),
	         ('1','aaaaaaaaaaaaaaaaaaaattttttttttttttttttttt'),
	         ('2','aaaaaaaaaaaaaaaaaaaaggggggggggggggggggggggggggggggggggggggggggggggggggggggggttttttttttttttttttttt'),
	         ('3','aaaaaaaaaaaaaaaaaaaaggggggggggggggggggggggggggggggggggggggggggggggggggggggggttttttttttttttttttttt'),
	         ('4','aaaaaaaaaaaaaaaaaaaaggggggggggggggggggggggggggggggggggggggggggggggggggggggggttttttttttttttttttttt')]
	#todo we don't need this fasta anywhere, change the function
	alignment = imported.computePOA(os.path.dirname(os.path.abspath(sys.argv[0])) + "/msa_test0.fa", installDirectory, outDirectory)
	newMsa = imported.MSA()
	newMsa.getMSA(alignment)
	#~ newMsa.buildMSAMatrix()
	gaps = newMsa.findGapStretches()
	newMsa.prolongateNstretches(gaps)
	regions = newMsa.findRegions()
	newMsa.findPositionRegions(regions)
	newMsa.getIntervals()
	clusters = newMsa.clusterReadsByGapProfile()
	newMsa.writeConsensusByGapProfile(installDirectory, outDirectory)
	# newMsa.writeConsensusByGapProfile(fasta, installDirectory, outDirectory)
	outFile = Path(outDirectory + "/corrected_by_MSA0.fa")
	if not outFile.is_file():
		return False
	outFile = Path(outDirectory + "/corrected_by_MSA1.fa")
	if not outFile.is_file():
		return False
	out0 = open(outDirectory + "/corrected_by_MSA0.fa", 'r')
	out1 = open(outDirectory + "/corrected_by_MSA1.fa", 'r')
	toReturn = True
	no = ''
	for lines in out0.readlines():
		if ">" not in lines:
			if no == '0':
				if not lines.rstrip() == 'aaaaaaaacaaaaaaaaaaattttttttttttttttttttt':
					toReturn = False
			elif no == '1':
				if not lines.rstrip() =='aaaaaaaaaaaaaaaaaaaattttttttttttttttttttt':
					toReturn = False
			else:
				toReturn = False
		else:
			no = lines.split(">")[1].split(' ')[0].rstrip()
	no = ""
	for lines in out1.readlines():
		if ">" not in lines:
			if no == '3' or no == '4' or no == '2':
				if not lines.rstrip() == 'aaaaaaaaaaaaaaaaaaaaggggggggggggggggggggggggggggggggggggggggggggggggggggggggttttttttttttttttttttt':
					toReturn = False
			else:
				toReturn = False
		else:
			no = lines.split(">")[1].split(' ')[0].rstrip()
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/corrected_by_MSA*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/cluster*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/msa_profile*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/msaAllReads.fa"])
	except subprocess.CalledProcessError:
		return False
	return toReturn
	


#test 15 #todo test with only one cluster
def test_writeConsensusByGapProfile2(imported):
	installDirectory = os.path.dirname(os.path.abspath(sys.argv[0])) + "/.."
	outDirectory = os.path.dirname(os.path.abspath(sys.argv[0]))
	fasta = [('0','aaaacaaataaaaattttttttttttttttttttt'),
	         ('1','aagacaaataaaaattttttgtttttttttttt'),
	         ('2','aaaaaaataagaattttttgtttcttttttttttt')]
	#todo we don't need this fasta anywhere, change the function
	alignment = imported.computePOA(os.path.dirname(os.path.abspath(sys.argv[0])) + "/msa_test1.fa", installDirectory, outDirectory)
	newMsa = imported.MSA()
	newMsa.getMSA(alignment)
	#~ newMsa.buildMSAMatrix()
	gaps = newMsa.findGapStretches()
	newMsa.prolongateNstretches(gaps)
	regions = newMsa.findRegions()
	newMsa.findPositionRegions(regions)
	newMsa.getIntervals()
	clusters = newMsa.clusterReadsByGapProfile()
	newMsa.writeConsensusByGapProfile(installDirectory, outDirectory)
	# newMsa.writeConsensusByGapProfile(fasta, installDirectory, outDirectory)
	outFile = Path(outDirectory + "/corrected_by_MSA0.fa")
	if not outFile.is_file():
		return False
	out0 = open(outDirectory + "/corrected_by_MSA0.fa", 'r')
	toReturn = True
	for lines in out0.readlines():
		if ">" not in lines:
			if no == '0' or no == '1' or no == '2':
				if not lines.rstrip() == 'aaaacaaataaaaattttttgtttttttttttttt':
					input(lines)
					toReturn = False
			else:
				toReturn = False
		else:
			no = lines.split(">")[1].split(' ')[0].rstrip()
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/corrected_by_MSA*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/cluster*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/msa_profile*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/msaAllReads.fa"])
	except subprocess.CalledProcessError:
		return False
	return toReturn


#test16
def test_computePartialMsa(imported):
	toReturn = True
	installDirectory = os.path.dirname(os.path.abspath(sys.argv[0])) + "/.."
	outDirectory = os.path.dirname(os.path.abspath(sys.argv[0]))
	fasta = [('0','aaaaaaaacaaaaaaaaaaattttttttttttttttttttt'),
	         ('1','aaaaaaaaaaaaaaaaaaaattttttttttttttttttttt'),
	         ('2','aaaaaaaaaaaaaaaaaaaaggggggggggggggggggggggggggggggggggggggggggggggggggggggggttttttttttttttttttttt'),
	         ('3','aaaaaaaaaaaaaaaaaaaaggggggggggggggggggggggggggggggggggggggggggggggggggggggggttttttttttttttttttttt'),
	         ('4','aaaaaaaaaaaaaaaaaaaaggggggggggggggggggggggggggggggggggggggggggggggggggggggggttttttttttttttttttttt')]
	#todo we don't need this fasta anywhere, change the function
	alignment = imported.computePOA(os.path.dirname(os.path.abspath(sys.argv[0])) + "/msa_test0.fa", installDirectory, outDirectory)
	newMsa = imported.MSA()
	newMsa.getMSA(alignment)
	#~ newMsa.buildMSAMatrix()
	gaps = newMsa.findGapStretches()
	newMsa.prolongateNstretches(gaps)
	regions = newMsa.findRegions()
	newMsa.findPositionRegions(regions)
	newMsa.getIntervals()
	#~ newMsa.computePartialMsa(fasta, installDirectory, outDirectory)
	newMsa.computePartialMsa(installDirectory, outDirectory)
	outFile = Path(outDirectory + "/exonChainFasta0.fa")
	if not outFile.is_file():
		return False
	exon0 = open(outDirectory + "/exonChainFasta0.fa", 'r')
	for lines in exon0.readlines():
		if ">" not in lines:
			if  no == '1' or no == '2' or no == '3' or no == '4':
				if not lines.rstrip() == 'aaaaaaaaaaaaaaaaaaaa':
					toReturn = False
			elif no == '0':
				if not lines.rstrip() == 'aaaaaaaacaaaaaaaaaaa':
					toReturn = False
			else:
				toReturn = False
		else:
			no = lines.split(">")[1][0].rstrip()
	outFile = Path(outDirectory + "/exonChainFasta1.fa")
	if not outFile.is_file():
		return False
	exon1 = open(outDirectory + "/exonChainFasta1.fa", 'r')

	for lines in exon1.readlines():
		if ">" not in lines:
			if  no == '2' or no == '3' or no == '4':
				if not lines.rstrip() == 'gggggggggggggggggggggggggggggggggggggggggggggggggggggggg':
					toReturn = False
			else:
				toReturn = False
		else:
			no = lines.split(">")[1][0].rstrip()
	outFile = Path(outDirectory + "/exonChainFasta2.fa")
	if not outFile.is_file():
		return False
	exon2 = open(outDirectory + "/exonChainFasta2.fa", 'r')
	for lines in exon2.readlines():
		if ">" not in lines:
			if  no == '0' or no == '1' or no == '2' or no == '3' or no == '4':
				if not lines.rstrip() == 'ttttttttttttttttttttt':
					toReturn = False
			else:
				toReturn = False
		else:
			no = lines.split(">")[1][0].rstrip()
	outFile = Path(outDirectory + "/corrected_by_MSA0.fa")
	if not outFile.is_file():
		return False
	consensus0 = open(outDirectory + "/corrected_by_MSA0.fa", 'r')
	for lines in consensus0.readlines():
		if ">" not in lines:
			if  no == '0' or no == '1' :
				if not lines.rstrip() == 'aaaaaaaaaaaaaaaaaaaattttttttttttttttttttt':
					toReturn = False
			else:
				toReturn = False
		else:
			no = lines.split(">")[1][0].rstrip()
	outFile = Path(outDirectory + "/corrected_by_MSA1.fa")
	if not outFile.is_file():
		return False
	consensus1 = open(outDirectory + "/corrected_by_MSA1.fa", 'r')
	for lines in consensus1.readlines():
		if ">" not in lines:
			if  no == '2' or no == '3' or no == '4' :
				if not lines.rstrip() == 'aaaaaaaaaaaaaaaaaaaaggggggggggggggggggggggggggggggggggggggggggggggggggggggggttttttttttttttttttttt':
					toReturn = False
			else:
				toReturn = False
		else:
			no = lines.split(">")[1][0].rstrip()
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/corrected_by_MSA*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/cluster*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/exonChainFasta*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/msaAllReads.fa"])
	except subprocess.CalledProcessError:
		return False
	return toReturn


#test 17
def test_computePartialMsa2(imported):
	toReturn = True
	installDirectory = os.path.dirname(os.path.abspath(sys.argv[0])) + "/.."
	outDirectory = os.path.dirname(os.path.abspath(sys.argv[0]))
	fasta = [('0','aaaacaaataaaaattttttttttttttttttttt'),
	         ('1','aagacaaataaaaattttttgtttttttttttt'),
	         ('2','aaaaaaataagaattttttgtttcttttttttttt')]
	         
	#todo we don't need this fasta anywhere, change the function
	alignment = imported.computePOA(os.path.dirname(os.path.abspath(sys.argv[0])) + "/msa_test1.fa", installDirectory, outDirectory)
	newMsa = imported.MSA()
	newMsa.getMSA(alignment)
	#~ newMsa.buildMSAMatrix()
	gaps = newMsa.findGapStretches()
	newMsa.prolongateNstretches(gaps)
	regions = newMsa.findRegions()
	newMsa.findPositionRegions(regions)
	newMsa.getIntervals()
	newMsa.computePartialMsa(installDirectory, outDirectory)
	#~ newMsa.computePartialMsa(fasta, installDirectory, outDirectory)
	outFile = Path(outDirectory + "/corrected_by_MSA0.fa")
	if not outFile.is_file():
		return False
	consensus0 = open(outDirectory + "/corrected_by_MSA0.fa", 'r')
	for lines in consensus0.readlines():
		if ">" not in lines:
			if  no == '0' or no == '1' or no == '2' :
				if not lines.rstrip() == 'aaaacaaataaaaattttttgtttttttttttttt':
					toReturn = False
			else:
				toReturn = False
		else:
			no = lines.split(">")[1][0].rstrip()
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/corrected_by_MSA*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/cluster*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/exonChainFasta*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/msaAllReads.fa"])
	except subprocess.CalledProcessError:
		return False
	return toReturn


#test 18
def test_detectFastaFastq(imported):
	installDirectory = os.path.dirname(os.path.abspath(sys.argv[0])) + "/.."
	outDirectory = os.path.dirname(os.path.abspath(sys.argv[0]))
	isFasta = imported.detectFastaFastq(os.path.dirname(os.path.abspath(sys.argv[0])) + "/cluster4299")
	if not isFasta:
		return True
	return False
	#~ alignment = imported.computePOA(os.path.dirname(os.path.abspath(sys.argv[0])) + "/cluster4299", installDirectory, outDirectory)

#test 19
def convertReadsToFasta(imported):
	installDirectory = os.path.dirname(os.path.abspath(sys.argv[0])) + "/.."
	outDirectory = os.path.dirname(os.path.abspath(sys.argv[0]))
	inFile = imported.convertReadsToFasta(os.path.dirname(os.path.abspath(sys.argv[0])) + "/cluster4299", installDirectory, outDirectory)
	fasta = open(outDirectory + "/reads_fasta_for_msa.fasta")
	header1 = fasta.readline().rstrip()
	if header1 !=  ">619198e1-cf2b-4891-9da8-25d435e2b4cbfa44000b-eac8-4f64-aea5-c2b893afd339 runid=2366276392a4fac875b6f59ea657b69d5b0565f9 read=14618 ch=183 start_time=2017-12-09T00:53:57Z":
		return False
	seq1 = fasta.readline().rstrip()
	if seq1 !=  "TAAACTTGTCAGAGGTTCCAAGTCAGAGAGGTTCCTAAGCAGTGGTATCAACGCAGAGTACATGGGTTTTAACCTGTGATACAACTGATTTAGCATTTCATACATGACATGTTTGATGTTCTCTAAATGCGGCTGAGCTAAAAAATAAAATAAAACTATTTTGAATGATGAAAGAATAGTTTCAAATTCTATTGCAGACTCGCAATAGCTTCATGAAACCTGGAGTTTATGTTTATTTTCTTAAAGACAATTCAGGGCTTCAAAGAAAAAAAGTACTTGAAGATAGTTCCTCCATTAGATAGAAAATATCTGCATGGGAGATAAATAATCTTCATTTTTTTTTTTTTTTACTTGTGTGGTAATTTATAAATTTGCACGGCATAGTATATTTCTGAAAAAAAGCAATAGACATCATTAAGAAAATAGACACTATTTCTAAATTTTGTTAGAATAAGAAGGCACCACTGATTTGAGGTTGTGTCCTGTTGTCACCGATAGTTGACTTCTGAGTTGTTTATTTCATCATTTACTCTTATTTTATTAAATGTTTTCTGGGTAATAGCACTTAAGACCATTATTGTGGCTTCGGACCAAAACAAAACACACTCATAGTAGTATTTCATCATAGAGATCATTTCACAGTATATGTTGACAATACAGATTCTTTCTCCACCAAGATTTATCCAGATTCCACCTGGCTTGAGTATTCTCCATATTGTATCAATATAATCAATTACATTGTGAGCTGTGTCTATGAAGAAGCAGGTGGCAATACAGTCCCAAGCATTGCATTTGAGTGTAAATCTCCTGAAAGTCTCCTGCTGTCATGGAAAAGTTAGAACCAGGAGGAAGACTGTGGGGGTCAACATCAGGGGAAAAGAATGGGTCGGATCTGATCAGCTGATCTCGATTATTATTGCTAAACTGATGGATCCAGGGATAAAGTTTATATTTGTTAATTTTCAGAACATCTGTTGAGTACAAAGTTGGAAGAAAAGAGCATAAAAAACTCCATTCATTTCATGACAAGCATAACCTAGCAAAGTAGCTACTTCCCCAGGCCAATCTTCCTAGTCCAACACCAGGTACCAGAATATTTACTTTTAGAAGGATCCCATCTCTCCTTTGGAAAATTTTTGATAATCTCTTTAATGATTGGCTTATAGCAGGCGTCCCTTTTCTGCTTTCCCGGTCTCACTCCAGTCTCTGCCTACAAACTGTTTCAGTGTAGATTTTAACTTATCCATGTCAAATGTAGATGCTGGCATAATCTTCCATTGGCATCATCTTCTCCATATTCTTTTATTTTCGAACATATGTATGCAATCATTCACAATGGTCAGCAGTATTTCCTGATTATGGTCAATGCATTTCCGGATCTTGTCCAAGTGAAGAGGAAACTGAGGAAGCAGTTTCTGCTGGTTCTCTGGAAGGGACCGAAACTGTCTTTCTGTTCTGTTCACTCGTTCATGCATGCTGGTGCCGTAGTAGCGGAACGCGTTGATGACCTTCCAGAAGTGCTCGCGCTCCAGCCATCGCCCTCGTCTCCGGCGGTGCCGCGCGCCGGGGCCCTGTGAGCGCGGCCGAGCCCAAACGCGCCCGGCGGAGAACTGCACCTCCACGTCCTCGCTTCGCCCGCCGTCCTGGGCCGGTTGAGACGCGGGCGGAGCACGTCGCCAAAATATCATCGCTGCATCGCCCACGGGCGCGGCCACCGCGTGACCACCCGGCCCAGCTTCCCGGCCTCCCGAGTGCGTGGTGGCGAACGGCCGGGCCTCCTCTCCCAGACGGCGCCGGCCGCGGCCAGGCCCCGCTCGCGGCACGCCCCGCCCCCTGCTGCCCATGTACTCTGCGTTGATACCACTGCTTAGGAACCTCTCTGACTTGGAACCTCAAAGCTCGGTTTGAGCACGCCCAGGCATAGCAATACGTAACTGGAACGT":
		return False
	return True


#test20
def alignments_fastq(imported):
	installDirectory = os.path.dirname(os.path.abspath(sys.argv[0])) + "/.."
	outDirectory = os.path.dirname(os.path.abspath(sys.argv[0]))
	inFile = imported.convertReadsToFasta(os.path.dirname(os.path.abspath(sys.argv[0])) + "/cluster4299", installDirectory, outDirectory)
	alignments = imported.computePOA(inFile, installDirectory, outDirectory)
	if alignments != [
	('0', ('619198e1-cf2b-4891-9da8-25d435e runid=2366276392a4fac875b6f59ea657b69d5b0565f9 read=14618 ch=183 start_time=2017-12-09T00:53:57Z', 't...................................................aaact..tgtcagaggttccaagtcagagaggttcct............................................................................................................................................................................................................................aagcagtgg......................tatca...acgcaga.................gtacatgggttttaacctgtgatacaactgatttagcatttca.tacatgacatgtttgatgttctctaaatgcggctgagctaaaaaataaaataaaactattttgaatgatgaaagaatagtttcaaattctattgcagactc.gcaatagcttcatgaaacctggagtttatgtttattt...tcttaaagacaattcagggcttcaaagaaaaaaagtacttgaagatagttcctccattagatagaaaatatctgcatgggagataaataa..tcttcatttttttttttttttactt.gtgtggtaatttataaatttgcacggcatagtatatttctgaaaaaaagcaatagacatcattaagaaaatagacactatttctaaattttgttag.aataagaaggcaccactgatttgaggttgtgtcctgttgtcaccgatagttgacttctg.agttgtttatttcatcatttactcttattttattaaatgttttctgggtaatagcacttaagaccattattgtggcttc.ggacca.aaaca.aaacacact..catagtagtatttcatcatagaga..tcatttcacagtatatgttgacaatacagatt...ctttctccacca...........................................................................................................a.gatttatccagattccacctggcttgagtattctccatattgtatcaatataatcaattacattgtgagctgtgtctatgaagaagcaggtggcaatacagtcccaagcattgcatt.tgagtgtaaatctcctg.aaagtctcctgctgtcatggaaaagttagaaccaggaggaagactgtgggggtcaacatcaggggaaaagaatgggtcggatctgatcagctgatctc.gattatta...ttgctaaactgatgg.atccagggataaagtttatatttgttaattttcagaacatc.....tgttgagtacaaa...gttggaagaaaagagcataaaaaa.ctccattcatttca..tgacaagcataacctagcaaagtagctacttccccaggccaatcttcct.agtccaacaccaggtaccagaatatttacttttagaaggatcccatctctcctttggaaaatttttgataatctctttaatgattggcttatagcaggcgtcccttttctgctttcccggtctcactccagtctctgcctacaaa.ctgtttcagtgtagattttaacttatccatgtcaaatgtagatgctggcataatctt.ccattggcatcatcttctccatattcttttattttcgaacatatgtatgcaatcattcacaatggtcagcagtatttcctgattat......ggtcaatgcatttccggatcttgtccaagtgaagaggaaactg.....aggaagcagtttctgctggttctctggaaggga..ccgaaactgtctttctgttctgttca....ctcgttcatgcatgctg..gtgccgtagtagcggaacgcgttgatgaccttccagaagtgctcgcgctccagccat..cgccctcgtctccggcggtgccgcgcgccgggg..ccctgtgagcgcggccgagcccaaacgcgcccggcggagaactgcacctccacgtcctcgcttcgccc....gccgtcctgggccggttgagacgcgggcggagcacgtcgccaaaatatca.tcgctgcatcgcccacgggcg...cggcc.accgcgtgaccacccggcccagcttcccggcctcccgagtgcgtggtggcgaacggccgggcctcctctcccagacggcgccggccgcggccaggccccgctcgcggcacgccccgccccctgctg.....cccatgtactctgcgttgataccactgcttaggaacctctc.tgacttggaac.ctcaaa...gctc....ggtttgagcac......gcccaggcatagcaatacgtaactgg.......aacgt')),
	('1', ('353520a9-f3fe-486a-affc-6febccf runid=2366276392a4fac875b6f59ea657b69d5b0565f9 read=7021 ch=498 start_time=2017-12-08T23:30:39Z', '.tg....gtattgctggcgcttcgttccgtttgcattatt.tgaaaccgatc....t..gtttcgaggttccaagtcagagaggttcct......................................................................................................................................................................................................................................................................................................................................ctacacgacat...................................................................................................cttc..........................cgatct....gaccaatca......................................................tatgctacg.accgaa....agttttttttttttttttttttt.........................................................................................................................tttaaggttgtgtcctgttgtcaccgatagttgact.ctggagttgtttatttcatcatttactcttatt.....aaatgttttctgggtaatagcacttaagaccattattgtggcttccggacctaaaccacaaacacact..catagtagtatttcatcatagaga..tcatt.cacagtatatgttgacaatacagattattctttctccacc.tcta..gctgaaatccatactgaagaacaacatttttaatatcct..gtagctcaattctatagacagttcatttgccagatt..caaaatggtatagtagaggaccaagatttatccagattccacctggcttgagtattctccatattgtatcaatataatcaattacattgtgagctgtacctatgaagaagcaggtggcaatacagtcccaggcattgcattctgagt..aaatctcctg.aaagtctcctgctgtcatggaaa.gttagaaccaggaggaagactgtgggggtcaacatcaggg.aaaagaatgggtcggatctgatcagctgatctccgattatt......gctaaactgatgggatccagggataaagtttatatttgttaattt.cagaacatc............................................................................................................................................................atctctcctttggaaaatttttgataatctctttaatgattggcttatagcaggcgtcc..tttctgctttcccggtctcactc.agtctct...tacaaa.ctgtttcagtgtagattttaacttatccatgtcaaatgtagatgctggcataatctttccattggcatc...ttctccatattcttt.attttcgaacatatgcatgcaatcattcacaatggtcagcagtatttcctgattataattatggtcaatacatttc.ggatcttgtccaagtgaagaggaaactg.....aggaaacagtttctgctggttctctggaagggattcagaaactgtctttctgttctgttc.tgctcacgttcatgcatgctgtggtgccgtagtagcggaacgcgttgatgaccttccagaagtgctcgcgctccagccgc..tctcctcgtctc.ggcggtgccgcgcgccggggggccccgcaggcgcggccgagcccaaacgc..ccggcggagaactgcacctccacgtcct.gcttcgccc....gccgtcctgggccggttgagacgcgggcggagcgcgctgtc.........gtcgctgcatcgcc....gccgtacctgac.ccggcgtgacc.gcctgc...gcttcccggc.tcccgagt......................................................................................gcgtccccatgtactctgcgttgataccactgcttaggaacctctc.tgacttggaac.ctctca...ggacaaa.gtttcaagcactgataagcagacgcccagcaatacgtaactg.aacccataatg.')),
	('2', ('7204b23d-5340-4e12-a278-cd5de4d runid=2366276392a4fac875b6f59ea657b69d5b0565f9 read=15796 ch=199 start_time=2017-12-09T01:34:13Z', '.ggttacgtattgctggcg..........tctgcttaagtgttgaaccgatcaacc.gatgtcagaggttccaagtcagagaggttcctctacacgacgctcttcgatctagcagccgtcatgcatggagacgccttttttttttttttttttttttctttttttttttttttttttttttttttttttttttttttttttttgtatttcagatgggcagattttattcttatttttccagtttcttaacactgcttttataaagaagctttggaccttaaatatttatttacatacatttcaaaagggatccagtg.attgatagtataagattcagtttaacatggacaaagactttctctgttactgtagttcatgagttttaacctgtgatacaactgatttagcatttca.tacatgacatgtttgatgttctctaaatgcagctgagctaaaaa.taaaataaaactattttgaatgatgaaagaatagtttcaaattctattgcagactctgcaatagcttcatgaaacctggagtttatgtttattt...tcttaaagacaattcagggcttcaaagaaaaaaagtacttgaagatagttcctccattagata.......tctgcatgg.agataa....tgtcttcatttttttttttttttactttgtgtggta.....taaatttgcacggcatagtatatttctgaaaaaa.gcaatagacatcattaagaaaatagacactatttctaaattttgttaggaataagaaggcaccactgatttgaggttgtgtcctgttgtcaccgatagttgact.ctg.agttgtttatttcatcatttactcttatt.....aaatgttttctgg.taatagcacttaagaccattattgtggcttccggaccacaaaca.aaacacactttcatagtattatttcatcatagagagatcatt.cacagtatatgttgacaatacagatt...ctttctccacc.tctatagctgaaatccatactgaagaacaacatttttaatatcctca.tagctcaattctatagacagttcatttgccagattttcaaaatggtatagtagaggacca.gatttatccagattccacctggcttg.gtattctccatattgtatcaatataatcaattacattgtgagctgtgtctatgaagaagcaggtggcaatacagtcccaggcattgcattctgagt..aaatctcctggaaagtctcctgctgtcatggaaa.gttagaaccag.aggaagactgtgggg.tcaacatcagg..aaaagaatgggtcggatctgatcagctgatctccgattatt.gctaagctaaactgatgg.atccagggataaagtttatatttgttaattt.cagaacatcgttgatgttgagtacaaaagagttggaa..aaagagcataaaaaaactccattcatttc.cttgacaagcataacctagca...tagctacttccc.aggccaatcttcctgaatccagcaccaggtaccagaatatttgcttt.agaaggatcccatctc..ctttggaaaatttt.gataatctctttaatgattggcttatagcaggcgtcc..tttctgctttcc.ggtctcactccagtctct...tacaaaactgtttcagtgtagattttaacttatccatgtcaaatgtagatgctggcataatctttccattggcatc...ttctccatattcttt.attttcgaacatatgtatgcaatcattcacaatggtcagcagtatttcctgattat......ggtcaatgcatttccggatcttgtccaagtgaagaggaaactgggctgaggaagcagtttctgctggttctctggaaggga..ccgagcttgtctttctgttctgttca....ctcgttcatgcatgctg..gtgccgtagtagcggaacgcgttgatgaccttccagaagtgctcgcgctccagc...gccccgctcgtcctcggcggtgccgcgcgccaggg..gcctg.....gcggccgagcccaaacgc..ccggcggagaactgcacctccacgtcctcgcttcgcc.gcctgccgtcctgggccggttgagacgcgggcggagcgcgctgtc.........gtcgctgcatcgcc....gccg...cggcccacggcgtgacccgcctgcctgctttcccggc.tcccgagt..........................................................................................ccccatgtactctgcgttgataccactgcttaggaacctctcctgacttggaactctca..tcggttcagacggttcg.gcaa......acagacgcc.agcaatgcgtaactgg.......ca...'))]:
		return False
	return True


#test21
def stretches_fastq(imported):
	installDirectory = os.path.dirname(os.path.abspath(sys.argv[0])) + "/.."
	outDirectory = os.path.dirname(os.path.abspath(sys.argv[0]))
	inFile = imported.convertReadsToFasta(os.path.dirname(os.path.abspath(sys.argv[0])) + "/cluster4299", installDirectory, outDirectory)
	alignments = imported.computePOA(inFile, installDirectory, outDirectory)
	newMsa = imported.MSA()
	newMsa.getMSA(alignments)
	stretches = newMsa.findGapStretches()
	if stretches != {0: [[1, 51], [], [89, 308], [318, 339], [], [355, 371], [], [], [], [], [], [], [], [], [], [], [], [], [], [1010, 1116], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []], 1: [[], [], [], [], [], [89, 414], [426, 524], [529, 554], [], [574, 627], [], [], [671, 791], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [1422, 1577], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [2169, 2177], [], [], [], [], [], [2249, 2334], [], [], [], [], [], []], 2: [[], [19, 28], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [2169, 2177], [], [], [], [2249, 2338], [], [], [], [], [], []]}:
		#~ print("stretches",stretches)
		return False
	return True


#test22
def prolongateNstretches_fastq(imported):
	installDirectory = os.path.dirname(os.path.abspath(sys.argv[0])) + "/.."
	outDirectory = os.path.dirname(os.path.abspath(sys.argv[0]))
	inFile = imported.convertReadsToFasta(os.path.dirname(os.path.abspath(sys.argv[0])) + "/cluster4299", installDirectory, outDirectory)
	alignments = imported.computePOA(inFile, installDirectory, outDirectory)
	newMsa = imported.MSA()
	newMsa.getMSA(alignments)
	stretches = newMsa.findGapStretches()
	newMsa.prolongateNstretches(stretches)
	if newMsa.gapStretchesPositions != [1, 89, 371, 627, 671, 791, 1010, 1116, 1422, 1577, 2249, 2249, 2334, 2338]:
		return False
	return True

#test23
def regions_fastq(imported):
	installDirectory = os.path.dirname(os.path.abspath(sys.argv[0])) + "/.."
	outDirectory = os.path.dirname(os.path.abspath(sys.argv[0]))
	inFile = imported.convertReadsToFasta(os.path.dirname(os.path.abspath(sys.argv[0])) + "/cluster4299", installDirectory, outDirectory)
	alignments = imported.computePOA(inFile, installDirectory, outDirectory)
	newMsa = imported.MSA()
	newMsa.getMSA(alignments)
	stretches = newMsa.findGapStretches()
	newMsa.prolongateNstretches(stretches)
	regions = newMsa.findRegions()	
	print(regions)
	return True




def printTest(testNumber, testVal):
	print("Test", testNumber, "...", end = ' ')
	if testVal:
		print("passed")
	else:
		print("not passed")


def main():
	currentDirectory = os.path.dirname(os.path.abspath(sys.argv[0]))
	filename = currentDirectory + "/../analyze_MSAv2.py"
	sys.path.append(os.path.dirname(filename))
	mname = os.path.splitext(os.path.basename(filename))[0]
	imported = importlib.import_module(mname)                       
	sys.path.pop()
	print("Starting tests...")
	

	printTest(1, test_getReadsCluster(imported))
	printTest(2, test_getConsensusFromAlignment(imported))
	printTest(3, test_getMSA(imported))
	printTest(5, test_findGapStretches(imported))
	printTest(6, test_findGapStretches2(imported))
	printTest(7, test_prolongateNstretches(imported))
	printTest(8, test_findRegions(imported))
	printTest(9, test_findPositionRegions(imported))
	printTest(10, test_getIntervals(imported))
	printTest(11, test_getIntervals2(imported))
	printTest(12, test_extractSubSequences(imported))
	printTest(13, test_clusterReadsByGapProfile(imported))
	printTest(14, test_writeConsensusByGapProfile(imported))
	printTest(15, test_writeConsensusByGapProfile2(imported))
	printTest(16, test_computePartialMsa(imported))
	printTest(17, test_computePartialMsa2(imported))
	printTest(18, test_detectFastaFastq(imported))
	printTest(19,convertReadsToFasta(imported))
	printTest(20,alignments_fastq(imported))
	printTest(21,stretches_fastq(imported))
	printTest(22,prolongateNstretches_fastq(imported))
	regions_fastq(imported)

		
if __name__ == '__main__':
	main()
