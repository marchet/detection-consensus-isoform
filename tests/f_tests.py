#!/usr/bin/env python

#----------------------------------------------------------------------#
#Isoform detection and consensus in ONT long reads using MSA
#Copyright (C) Camille Marchet

#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#contact: camille.marchet@irisa.fr
#----------------------------------------------------------------------#

from __future__ import print_function

try:
    from builtins import zip
    from builtins import str
    from builtins import object
except ImportError:
    pass
import matplotlib.pyplot as pyplot
import argparse
from pathlib import Path
import numpy
import sys, os, importlib
import shlex, subprocess
from subprocess import Popen, PIPE, STDOUT
import re
from collections import Counter
import importlib.machinery
import glob


def printTest(testNumber, testVal):
	print("Test", testNumber, "...", end = ' ')
	if testVal:
		print("passed")
	else:
		print("not passed")






def test_string_headers_exonsCorrection(imported):
	toReturn = True
	installDirectory = os.path.dirname(os.path.abspath(sys.argv[0])) + "/.."
	outDirectory = os.path.dirname(os.path.abspath(sys.argv[0]))
	fasta = [('exclu_0','aaaaaaaacaaaaaaaaaaattttttttttttttttttttt'),
	         ('exclu_1','aaaaaaaaaaaaaaaaaaaattttttttttttttttttttt'),
	         ('incl_2','aaaaaaaaaaaaaaaaaaaaggggggggggggggggggggggggggggggggggggggggggggggggggggggggttttttttttttttttttttt'),
	         ('incl_3','aaaaaaaaaaaaaaaaaaaaggggggggggggggggggggggggggggggggggggggggggggggggggggggggttttttttttttttttttttt'),
	         ('incl_4','aaaaaaaaaaaaaaaaaaaaggggggggggggggggggggggggggggggggggggggggggggggggggggggggttttttttttttttttttttt')]
	alignment = imported.computePOA(os.path.dirname(os.path.abspath(sys.argv[0])) + "/msa_test0.fa", installDirectory, outDirectory)
	newMsa = imported.MSA()
	newMsa.getMSA(alignment)
	gaps = newMsa.findGapStretches()
	newMsa.prolongateNstretches(gaps)
	regions = newMsa.findRegions()
	newMsa.findPositionRegions(regions)
	newMsa.getIntervals()
	newMsa.computePartialMsa(installDirectory, outDirectory)
	outFile = Path(outDirectory + "/exonChainFasta0.fa")
	if not outFile.is_file():
		return False
	exon0 = open(outDirectory + "/exonChainFasta0.fa", 'r')
	for lines in exon0.readlines():
		if ">" not in lines:
			if  no == '1' or no == '2' or no == '3' or no == '4':
				if not lines.rstrip() == 'aaaaaaaaaaaaaaaaaaaa':
					toReturn = False
			elif no == '0':
				if not lines.rstrip() == 'aaaaaaaacaaaaaaaaaaa':
					toReturn = False
			else:
				toReturn = False
		else:
			no = lines.split(">")[1][0].rstrip()
	outFile = Path(outDirectory + "/exonChainFasta1.fa")
	if not outFile.is_file():
		return False
	exon1 = open(outDirectory + "/exonChainFasta1.fa", 'r')

	for lines in exon1.readlines():
		if ">" not in lines:
			if  no == '2' or no == '3' or no == '4':
				if not lines.rstrip() == 'gggggggggggggggggggggggggggggggggggggggggggggggggggggggg':
					toReturn = False
			else:
				toReturn = False
		else:
			no = lines.split(">")[1][0].rstrip()
	outFile = Path(outDirectory + "/exonChainFasta2.fa")
	if not outFile.is_file():
		return False
	exon2 = open(outDirectory + "/exonChainFasta2.fa", 'r')
	for lines in exon2.readlines():
		if ">" not in lines:
			if  no == '0' or no == '1' or no == '2' or no == '3' or no == '4':
				if not lines.rstrip() == 'ttttttttttttttttttttt':
					toReturn = False
			else:
				toReturn = False
		else:
			no = lines.split(">")[1][0].rstrip()
	outFile = Path(outDirectory + "/corrected_by_MSA0.fa")
	if not outFile.is_file():
		return False
	consensus0 = open(outDirectory + "/corrected_by_MSA0.fa", 'r')
	for lines in consensus0.readlines():
		if ">" not in lines:
			if  no == '0' or no == '1' :
				if not lines.rstrip() == 'aaaaaaaaaaaaaaaaaaaattttttttttttttttttttt':
					toReturn = False
			else:
				toReturn = False
		else:
			no = lines.split(">")[1][0].rstrip()
	outFile = Path(outDirectory + "/corrected_by_MSA1.fa")
	if not outFile.is_file():
		return False
	consensus1 = open(outDirectory + "/corrected_by_MSA1.fa", 'r')
	for lines in consensus1.readlines():
		if ">" not in lines:
			if  no == '2' or no == '3' or no == '4' :
				if not lines.rstrip() == 'aaaaaaaaaaaaaaaaaaaaggggggggggggggggggggggggggggggggggggggggggggggggggggggggttttttttttttttttttttt':
					toReturn = False
			else:
				toReturn = False
		else:
			no = lines.split(">")[1][0].rstrip()
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/corrected_by_MSA*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/cluster*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/exonChainFasta*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/msaAllReads.fa"])
	except subprocess.CalledProcessError:
		return False
	return toReturn



def test_string_headers_profileCorrection(imported):
	toReturn = True
	installDirectory = os.path.dirname(os.path.abspath(sys.argv[0])) + "/.."
	outDirectory = os.path.dirname(os.path.abspath(sys.argv[0]))
	fasta = [('exclu_0','aaaaaaaacaaaaaaaaaaattttttttttttttttttttt'),
	         ('exclu_1','aaaaaaaaaaaaaaaaaaaattttttttttttttttttttt'),
	         ('incl_2','aaaaaaaaaaaaaaaaaaaaggggggggggggggggggggggggggggggggggggggggggggggggggggggggttttttttttttttttttttt'),
	         ('incl_3','aaaaaaaaaaaaaaaaaaaaggggggggggggggggggggggggggggggggggggggggggggggggggggggggttttttttttttttttttttt'),
	         ('incl_4','aaaaaaaaaaaaaaaaaaaaggggggggggggggggggggggggggggggggggggggggggggggggggggggggttttttttttttttttttttt')]
	alignment = imported.computePOA(os.path.dirname(os.path.abspath(sys.argv[0])) + "/msa_test0.fa", installDirectory, outDirectory)
	newMsa = imported.MSA()
	newMsa.getMSA(alignment)
	gaps = newMsa.findGapStretches()
	newMsa.prolongateNstretches(gaps)
	regions = newMsa.findRegions()
	newMsa.findPositionRegions(regions)
	newMsa.getIntervals()
	newMsa.writeConsensusByGapProfile(installDirectory, outDirectory)
	outFile = Path(outDirectory + "/corrected_by_MSA0.fa")
	if not outFile.is_file():
		return False
	consensus0 = open(outDirectory + "/corrected_by_MSA0.fa", 'r')
	for lines in consensus0.readlines():
		if ">" not in lines:
			if   no == '1' :
				if not lines.rstrip() == 'aaaaaaaaaaaaaaaaaaaattttttttttttttttttttt':
					toReturn = False
			elif no == '0':
				if not lines.rstrip() == 'aaaaaaaacaaaaaaaaaaattttttttttttttttttttt':
					toReturn = False
			else:
				toReturn = False
		else:
			no = lines.split(">")[1][0].rstrip()
	outFile = Path(outDirectory + "/corrected_by_MSA1.fa")
	if not outFile.is_file():
		return False
	consensus1 = open(outDirectory + "/corrected_by_MSA1.fa", 'r')
	for lines in consensus1.readlines():
		if ">" not in lines:
			if  no == '2' or no == '3' or no == '4' :
				if not lines.rstrip() == 'aaaaaaaaaaaaaaaaaaaaggggggggggggggggggggggggggggggggggggggggggggggggggggggggttttttttttttttttttttt':
					toReturn = False
			else:
				toReturn = False
		else:
			no = lines.split(">")[1][0].rstrip()
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/corrected_by_MSA*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/cluster*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/msaAllReads.fa"])
	except subprocess.CalledProcessError:
		return False
	return toReturn






def test_errors_profileCorrection(imported):
	toReturn = True
	installDirectory = os.path.dirname(os.path.abspath(sys.argv[0])) + "/.."
	outDirectory = os.path.dirname(os.path.abspath(sys.argv[0]))
	fasta = [('exclu_0','aaaaaaaacaaaaaaaaaaattttttttttttttttttttt'),
	         ('exclu_1','aaaaaaaaaaaaaaaaaaaattttttttttttttttttttt'),
	         ('exclu_2','aaaaaaaaaaaaaaaaaaaatttttttttttttttttttt'),
	         ('incl_2','aaaaaaaaaaaaaaaaaaaggggggggggggggggggggggggggggggggggggggggggggggggggggggttttttttttttttctttttt'),
	         ('incl_3','aaaaaaaaaaaataaaaaaagggggggggggggggggggcggggggggggggggggggggggggggggggggggggttttttttttttttttttt'),
	         ('incl_4','aaaaaaaaaaaaaaaaaaaaggggggggggggggggggggggggggggggggggggggggggggggggggggggggttttttttttttttttttttt')]
	alignment = imported.computePOA(os.path.dirname(os.path.abspath(sys.argv[0])) + "/msa_test3.fa", installDirectory, outDirectory)
	newMsa = imported.MSA()
	newMsa.getMSA(alignment)
	gaps = newMsa.findGapStretches()
	newMsa.prolongateNstretches(gaps)
	regions = newMsa.findRegions()
	newMsa.findPositionRegions(regions)
	newMsa.getIntervals()
	newMsa.writeConsensusByGapProfile(installDirectory, outDirectory)
	outFile = Path(outDirectory + "/corrected_by_MSA0.fa")
	if not outFile.is_file():
		return False
	consensus0 = open(outDirectory + "/corrected_by_MSA0.fa", 'r')
	for lines in consensus0.readlines():
		if ">" not in lines:
			if no == '1' or no == '0' or no =='2':
				if not lines.rstrip() == 'aaaaaaaaaaaaaaaaaaaattttttttttttttttttttt':
					toReturn = False
			else:
				toReturn = False
		else:
			no = lines.split("_")[1][0].rstrip()
	outFile = Path(outDirectory + "/corrected_by_MSA1.fa")
	if not outFile.is_file():
		return False
	consensus1 = open(outDirectory + "/corrected_by_MSA1.fa", 'r')
	for lines in consensus1.readlines():
		if ">" not in lines:
			if  no == '2' or no == '3' or no == '4' :
				if not lines.rstrip() == 'aaaaaaaaaaaaaaaaaaaaggggggggggggggggggggggggggggggggggggggggggggggggggggggggttttttttttttttttttttt':
					toReturn = False
			else:
				toReturn = False
		else:
			no = lines.split("_")[1][0].rstrip()
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/corrected_by_MSA*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/cluster*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/msaAllReads.fa"])
	except subprocess.CalledProcessError:
		return False
	return toReturn


# test 4
def test_simulatedPerfect_profileCorrection(imported):
	toReturn = True
	installDirectory = os.path.dirname(os.path.abspath(sys.argv[0])) + "/.."
	outDirectory = os.path.dirname(os.path.abspath(sys.argv[0]))
	alignment = imported.computePOA(os.path.dirname(os.path.abspath(sys.argv[0])) + "/simulatedLR_size_100_abund_50_perfect.fa", installDirectory, outDirectory)
	newMsa = imported.MSA()
	newMsa.getMSA(alignment)
	gaps = newMsa.findGapStretches()
	newMsa.prolongateNstretches(gaps)
	regions = newMsa.findRegions()
	newMsa.findPositionRegions(regions)
	newMsa.getIntervals()
	newMsa.writeConsensusByGapProfile(installDirectory, outDirectory)
	outFile = Path(outDirectory + "/corrected_by_MSA1.fa")
	if not outFile.is_file():
		return False
	consensus0 = open(outDirectory + "/corrected_by_MSA1.fa", 'r')
	for lines in consensus0.readlines():
		if ">" not in lines:
			if no == '0':
				if not lines.rstrip() == 'GGGCGCTTATGAGCGCCTTGATGTCTGTGTAACGCTTCGTACTGGCATCTCCGAATTGTCCTCGCTGAAAAACTGTCGTGCATAATTACGCGCTAGGGTGTTGAGAATTTCATAAATAAAGCGAATGGCCAAAGCTTCGGAAGTATTATTACCTCCGAATCAAGTCCGGTAGTGCACCAAAAGCTTTCTTCCTCTGGAAATAGTTTTAATCCTTGTGCAGACTAGGGAGTACTGAGGTTGGATGTGCCTCTATGACAGGGCGACCATTAGCGGCACTCTGTGGGCGTCCCAGAATCATAACCTAGTCCCAAATGGACCCGTCATCAAGTAGACCAACGCGGCGCAACCCTTAATTGTTAGTGTCATCCCGTAACCACTCGGATGTGATGACCTAGTATACGTCGATGCGTTATGGGCTCTTTAGTTCTGCCAAGGACACTTCTGTGCACGTACATCTAACGCGGAATCCACACATCGACTGCAACTCATCCCTTATATACTCCCGAGAATATAATCTACGCTAAGATGTATGCCTTCCAGCACCAATTAACCTCGGCCAACATTCGGGAGACGCGGCCCCGTTCACTGGAGAACTCACTAAACGCTCTCGATCTAGAAATAGTAAGCATAATACGGATCCCCATCCCCCCACTTCTCGTCGTATAGCCCGGTTTGAATGCAGGTGTTTCGATGCGGTTTCCGACCGCCCTGCGAAATTTAGATACGGACGCTACACTCGACACTAGTTCGTTGGTTAGAGACCATCCGGTTTTAGTGCGAACTGTTGACGGCTTCGAGCGCAGAAATTGCTGGGACCGCGATTAGAGGGTCTTTTATGTCTGTCAATGGAATAATGACCTCGGACGAAATGACCCGCAAACATCAGTCTAAAGGCTACACCGCGTGACTACACACCGAGCCGGTCTTCATGCCTAAGACCCGCGGGTAGGGTAATCTGTTCCCGCCGTCAAGGCACTACCGTCGTAAGGTGTATCCATACTAAGCATAAAGGATCTTCGCAAAATCGTAGAATAGACCCCCTACGCCCGTGGTGGGTACTTCTGGCGTGTAAGACACGCTCTCACAGAATATCTCTCAGTTGTGTCGATAACTCCCCACGTCCACTTGTCCGACATGTTCTTGGCATGACATGGATCTCACTGCATAGGTTCTGTAGTGGATCGCCGGCTGGGATGTTACGAGCGGCACAGCTTTACCGGCAATATGAGGCAGAGAGTATTGAGGTGTCACGACCCATCGGGGAGACAACTTATGGTACACGGCTTGTTTCCGTCAAGCAAATAACGTGTTTCCAAATTTGAAATGCTAGTAGTAGAGCGCACGCGGGCCCTCGACACAATACCTCATACGCGTATCGCGATGCCGGCTTCGTGTGATCACTCTTCGAAATATCCATACGTGAGCTACTCCACGTAACAACTAAACATCCCAACGCCGGAATACCTCCACCGCCGCTTCAAGCAATCCGTCGGCTAAACACTGGACATACAATCCTAGAGGCAAGTAGTGTCCCTCGTCGACTAGATGGACACCCACCGATCCATCGCATACATCGTAACACGGTTGAAAAAGGACTGGTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'.lower():
					#~ print(lines)
					toReturn = False
					break
		else:
			no = lines.split("_")[1][0].rstrip()
	outFile = Path(outDirectory + "/corrected_by_MSA0.fa")
	if not outFile.is_file():
		return False
	consensus1 = open(outDirectory + "/corrected_by_MSA0.fa", 'r')
	for lines in consensus1.readlines():
		if ">" not in lines:
			if  no == '0' :
				if not lines.rstrip() == 'GGGCGCTTATGAGCGCCTTGATGTCTGTGTAACGCTTCGTACTGGCATCTCCGAATTGTCCTCGCTGAAAAACTGTCGTGCATAATTACGCGCTAGGGTGTTGAGAATTTCATAAATAAAGCGAATGGCCAAAGCTTCGGAAGTATTATTACCTCCGAATCAAGTCCGGTAGTGCACCAAAAGCTTTCTTCCTCTGGAAATAGTTTTAATCCTTGTGCAGACTAGGGAGTACTGAGGTTGGATGTGCCTCTATGACAGGGCGACCATTAGCGGCACTCTGTGGGCGTCCCAGAATCATAACCTAGTCCCAAATGGACCCGTCATCAAGTAGACCAACGCGGCGCAACCCTTAATTGTTAGTGTCATCCCGTAACCACTCGGATGTGATGACCTAGTATACGTCGATGCGTTATGGGCTCTTTAGTTCTGCCAAGGACACTTCTGTGCACGTACATCTAACGCGGAATCCACACATCGACTGCAACTCATCCCTTATATACTCCCGAGAATATAATCTACGCTAAGATGTATGCCTTCCAGCACCAATTAACCTCGGCCAACATTCGGGAGACGCGGCCCCGTTCACTGGAGAACTCACTAAACGCTCTCGATCTAGAAATAGTAAGCATAATACGGATCCCCATCCCCCCACTTCTCGTCGTATAGCCCGGTTTGAATGCAGGTGTTTCGATGCGGTTTCCGACCGCCCTGCGAAATTTAGATACGGACGCTACACTCGACACTAGTTCGTTGGTTAGAGACCATCCGGTTTTAGTGCGAACTGTTGACGGCTTCGAGCGCGACCCTAGAGGCAAATGATTCTCTAGGGACATGCATCCCCTAGTATGTTCGACTTCCCTCTTACCCAGGGTCGCAGAGCATTAAGACTCCCAACCCGGTCAGAAATTGCTGGGACCGCGATTAGAGGGTCTTTTATGTCTGTCAATGGAATAATGACCTCGGACGAAATGACCCGCAAACATCAGTCTAAAGGCTACACCGCGTGACTACACACCGAGCCGGTCTTCATGCCTAAGACCCGCGGGTAGGGTAATCTGTTCCCGCCGTCAAGGCACTACCGTCGTAAGGTGTATCCATACTAAGCATAAAGGATCTTCGCAAAATCGTAGAATAGACCCCCTACGCCCGTGGTGGGTACTTCTGGCGTGTAAGACACGCTCTCACAGAATATCTCTCAGTTGTGTCGATAACTCCCCACGTCCACTTGTCCGACATGTTCTTGGCATGACATGGATCTCACTGCATAGGTTCTGTAGTGGATCGCCGGCTGGGATGTTACGAGCGGCACAGCTTTACCGGCAATATGAGGCAGAGAGTATTGAGGTGTCACGACCCATCGGGGAGACAACTTATGGTACACGGCTTGTTTCCGTCAAGCAAATAACGTGTTTCCAAATTTGAAATGCTAGTAGTAGAGCGCACGCGGGCCCTCGACACAATACCTCATACGCGTATCGCGATGCCGGCTTCGTGTGATCACTCTTCGAAATATCCATACGTGAGCTACTCCACGTAACAACTAAACATCCCAACGCCGGAATACCTCCACCGCCGCTTCAAGCAATCCGTCGGCTAAACACTGGACATACAATCCTAGAGGCAAGTAGTGTCCCTCGTCGACTAGATGGACACCCACCGATCCATCGCATACATCGTAACACGGTTGAAAAAGGACTGGTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'.lower():
					toReturn = False
					break
		else:
			no = lines.split("_")[1][0].rstrip()
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/corrected_by_MSA*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/cluster*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/msaAllReads.fa"])
	except subprocess.CalledProcessError:
		return False
	return toReturn




# test 5
def test_simulatedPerfect_exonCorrection(imported):
	toReturn = True
	installDirectory = os.path.dirname(os.path.abspath(sys.argv[0])) + "/.."
	outDirectory = os.path.dirname(os.path.abspath(sys.argv[0]))
	alignment = imported.computePOA(os.path.dirname(os.path.abspath(sys.argv[0])) + "/simulatedLR_size_100_abund_50_perfect.fa", installDirectory, outDirectory)
	newMsa = imported.MSA()
	newMsa.getMSA(alignment)
	gaps = newMsa.findGapStretches()
	newMsa.prolongateNstretches(gaps)
	regions = newMsa.findRegions()
	newMsa.findPositionRegions(regions)
	newMsa.getIntervals()
	newMsa.computePartialMsa(installDirectory, outDirectory)
	outFile = Path(outDirectory + "/corrected_by_MSA1.fa")
	if not outFile.is_file():
		return False
	consensus0 = open(outDirectory + "/corrected_by_MSA1.fa", 'r')
	for lines in consensus0.readlines():
		if ">" not in lines:
			if no == '0':
				if not lines.rstrip() == 'GGGCGCTTATGAGCGCCTTGATGTCTGTGTAACGCTTCGTACTGGCATCTCCGAATTGTCCTCGCTGAAAAACTGTCGTGCATAATTACGCGCTAGGGTGTTGAGAATTTCATAAATAAAGCGAATGGCCAAAGCTTCGGAAGTATTATTACCTCCGAATCAAGTCCGGTAGTGCACCAAAAGCTTTCTTCCTCTGGAAATAGTTTTAATCCTTGTGCAGACTAGGGAGTACTGAGGTTGGATGTGCCTCTATGACAGGGCGACCATTAGCGGCACTCTGTGGGCGTCCCAGAATCATAACCTAGTCCCAAATGGACCCGTCATCAAGTAGACCAACGCGGCGCAACCCTTAATTGTTAGTGTCATCCCGTAACCACTCGGATGTGATGACCTAGTATACGTCGATGCGTTATGGGCTCTTTAGTTCTGCCAAGGACACTTCTGTGCACGTACATCTAACGCGGAATCCACACATCGACTGCAACTCATCCCTTATATACTCCCGAGAATATAATCTACGCTAAGATGTATGCCTTCCAGCACCAATTAACCTCGGCCAACATTCGGGAGACGCGGCCCCGTTCACTGGAGAACTCACTAAACGCTCTCGATCTAGAAATAGTAAGCATAATACGGATCCCCATCCCCCCACTTCTCGTCGTATAGCCCGGTTTGAATGCAGGTGTTTCGATGCGGTTTCCGACCGCCCTGCGAAATTTAGATACGGACGCTACACTCGACACTAGTTCGTTGGTTAGAGACCATCCGGTTTTAGTGCGAACTGTTGACGGCTTCGAGCGCAGAAATTGCTGGGACCGCGATTAGAGGGTCTTTTATGTCTGTCAATGGAATAATGACCTCGGACGAAATGACCCGCAAACATCAGTCTAAAGGCTACACCGCGTGACTACACACCGAGCCGGTCTTCATGCCTAAGACCCGCGGGTAGGGTAATCTGTTCCCGCCGTCAAGGCACTACCGTCGTAAGGTGTATCCATACTAAGCATAAAGGATCTTCGCAAAATCGTAGAATAGACCCCCTACGCCCGTGGTGGGTACTTCTGGCGTGTAAGACACGCTCTCACAGAATATCTCTCAGTTGTGTCGATAACTCCCCACGTCCACTTGTCCGACATGTTCTTGGCATGACATGGATCTCACTGCATAGGTTCTGTAGTGGATCGCCGGCTGGGATGTTACGAGCGGCACAGCTTTACCGGCAATATGAGGCAGAGAGTATTGAGGTGTCACGACCCATCGGGGAGACAACTTATGGTACACGGCTTGTTTCCGTCAAGCAAATAACGTGTTTCCAAATTTGAAATGCTAGTAGTAGAGCGCACGCGGGCCCTCGACACAATACCTCATACGCGTATCGCGATGCCGGCTTCGTGTGATCACTCTTCGAAATATCCATACGTGAGCTACTCCACGTAACAACTAAACATCCCAACGCCGGAATACCTCCACCGCCGCTTCAAGCAATCCGTCGGCTAAACACTGGACATACAATCCTAGAGGCAAGTAGTGTCCCTCGTCGACTAGATGGACACCCACCGATCCATCGCATACATCGTAACACGGTTGAAAAAGGACTGGTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'.lower():
					toReturn = False
					break
		else:
			no = lines.split("_")[1][0].rstrip()
	outFile = Path(outDirectory + "/corrected_by_MSA0.fa")
	if not outFile.is_file():
		return False
	consensus1 = open(outDirectory + "/corrected_by_MSA0.fa", 'r')
	for lines in consensus1.readlines():
		if ">" not in lines:
			if  no == '0' :
				if not lines.rstrip() == 'GGGCGCTTATGAGCGCCTTGATGTCTGTGTAACGCTTCGTACTGGCATCTCCGAATTGTCCTCGCTGAAAAACTGTCGTGCATAATTACGCGCTAGGGTGTTGAGAATTTCATAAATAAAGCGAATGGCCAAAGCTTCGGAAGTATTATTACCTCCGAATCAAGTCCGGTAGTGCACCAAAAGCTTTCTTCCTCTGGAAATAGTTTTAATCCTTGTGCAGACTAGGGAGTACTGAGGTTGGATGTGCCTCTATGACAGGGCGACCATTAGCGGCACTCTGTGGGCGTCCCAGAATCATAACCTAGTCCCAAATGGACCCGTCATCAAGTAGACCAACGCGGCGCAACCCTTAATTGTTAGTGTCATCCCGTAACCACTCGGATGTGATGACCTAGTATACGTCGATGCGTTATGGGCTCTTTAGTTCTGCCAAGGACACTTCTGTGCACGTACATCTAACGCGGAATCCACACATCGACTGCAACTCATCCCTTATATACTCCCGAGAATATAATCTACGCTAAGATGTATGCCTTCCAGCACCAATTAACCTCGGCCAACATTCGGGAGACGCGGCCCCGTTCACTGGAGAACTCACTAAACGCTCTCGATCTAGAAATAGTAAGCATAATACGGATCCCCATCCCCCCACTTCTCGTCGTATAGCCCGGTTTGAATGCAGGTGTTTCGATGCGGTTTCCGACCGCCCTGCGAAATTTAGATACGGACGCTACACTCGACACTAGTTCGTTGGTTAGAGACCATCCGGTTTTAGTGCGAACTGTTGACGGCTTCGAGCGCGACCCTAGAGGCAAATGATTCTCTAGGGACATGCATCCCCTAGTATGTTCGACTTCCCTCTTACCCAGGGTCGCAGAGCATTAAGACTCCCAACCCGGTCAGAAATTGCTGGGACCGCGATTAGAGGGTCTTTTATGTCTGTCAATGGAATAATGACCTCGGACGAAATGACCCGCAAACATCAGTCTAAAGGCTACACCGCGTGACTACACACCGAGCCGGTCTTCATGCCTAAGACCCGCGGGTAGGGTAATCTGTTCCCGCCGTCAAGGCACTACCGTCGTAAGGTGTATCCATACTAAGCATAAAGGATCTTCGCAAAATCGTAGAATAGACCCCCTACGCCCGTGGTGGGTACTTCTGGCGTGTAAGACACGCTCTCACAGAATATCTCTCAGTTGTGTCGATAACTCCCCACGTCCACTTGTCCGACATGTTCTTGGCATGACATGGATCTCACTGCATAGGTTCTGTAGTGGATCGCCGGCTGGGATGTTACGAGCGGCACAGCTTTACCGGCAATATGAGGCAGAGAGTATTGAGGTGTCACGACCCATCGGGGAGACAACTTATGGTACACGGCTTGTTTCCGTCAAGCAAATAACGTGTTTCCAAATTTGAAATGCTAGTAGTAGAGCGCACGCGGGCCCTCGACACAATACCTCATACGCGTATCGCGATGCCGGCTTCGTGTGATCACTCTTCGAAATATCCATACGTGAGCTACTCCACGTAACAACTAAACATCCCAACGCCGGAATACCTCCACCGCCGCTTCAAGCAATCCGTCGGCTAAACACTGGACATACAATCCTAGAGGCAAGTAGTGTCCCTCGTCGACTAGATGGACACCCACCGATCCATCGCATACATCGTAACACGGTTGAAAAAGGACTGGTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'.lower():
				
					toReturn = False
					break
		else:
			no = lines.split("_")[1][0].rstrip()
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/corrected_by_MSA*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/cluster*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/msaAllReads.fa"])
	except subprocess.CalledProcessError:
		return False
	return toReturn


# test 6
def test_simulated1pc_profileCorrection(imported):
	toReturn = True
	installDirectory = os.path.dirname(os.path.abspath(sys.argv[0])) + "/.."
	outDirectory = os.path.dirname(os.path.abspath(sys.argv[0]))
	alignment = imported.computePOA(os.path.dirname(os.path.abspath(sys.argv[0])) + "/simulatedLR_size_100_abund_50_1pc.fa", installDirectory, outDirectory)
	newMsa = imported.MSA()
	newMsa.getMSA(alignment)
	gaps = newMsa.findGapStretches()
	newMsa.prolongateNstretches(gaps)
	regions = newMsa.findRegions()
	newMsa.findPositionRegions(regions)
	newMsa.getIntervals()
	newMsa.writeConsensusByGapProfile(installDirectory, outDirectory)
	outFile = Path(outDirectory + "/corrected_by_MSA0.fa")
	if not outFile.is_file():
		return False
	consensus0 = open(outDirectory + "/corrected_by_MSA0.fa", 'r')
	for lines in consensus0.readlines():
		if ">" not in lines:
			if no == '0':
				if not lines.rstrip() == 'ACTGGCGTGAATGACGGGGCTCCCGCAAGTCGAAAGGGGAGGAAGCTATCCTGTAAAAAGTGAAGAGAGACATCACGTGGATCGGCTGGTACCACAAAAGACGTGTCCGTTTGACCCATTAACCAGCCGGTTTGGGCTTTGGGCGAGTACTACAGGGTTCCGACAGTCGGCAATCGTTCACAAGAGATGTATGAAGGTAACCCCAGTAGCATCACGTGCCCCATCCGTAGAGTCTTTTTCATACAGTTAAACCACGCTGGCGAATTTGGTAGGAAGGTCGACACGCATAGCCACCTCACTTCGGGGAACGTCTTTCATCACGCCAGACGCAGTGACAACGGATGTGTTCATGTATTGTACCATATTCTTGCGTCAGTTGCACTTGTGAGGCTGATCTACTTGCGTCATAGAAATTGGCGCTAAGAATAAATTTACGCGCCACGACCGTTCAGCCACCACCAACTAGGCATTCCCCGGACCCGAGTATAACCCCGAGCGTCGGGTTACGAGTCATTTAGTATCCCTGTAAGGGCACAAGGACGCCCACCTAGGCTTCCGCGCTAGAGGAACCCTGTATAGGAAAGTATCGATTATCACAAGCCTAAGATGGCTGCCGCATAAGTACCATCCCGGAGGTGCCCGATACCCCACGGCGAGTTTAACGATCTCGCGCCGCTTGAAAGGCCGTACGACTGGGTCTGGGTAAATTGTTGGGTTAGTGATTTCCCCTCTCTGCTGAGAAGTGAGCACAGCTGCATGCGTAAGGCCAGACGGAAGGCGTCCCATGAGAGCAGCGCGACTTTAATAATATGATTTTGTTCGTGTGCTACGTACTAATATTTGTTCGGACCCAATTTCGTGAGGCCGCATATGGTCACAATCCTGAGCCAATAGCCTAGTAAGGGGGACTGAATCAGTCCATGACTGGCGAGGGAACTAGGTGTGATCTAGTTATCTGTAATGTGTTTGACATTTCTTGTTAGTAGATATAATGTCCTATTCAGATTAGGTGGCCTTCGTAGATCTAGGAGCCGTGCTGACCGTGTGGACCATCAACATTCCCGTAAATGAATGAGCGACTCGTTGTAGGTTTAGGCGTACTCAGCGTTGCTATGTCCTTAGTTTCGATCAAACCGGTGGGTCGGTCTAAGAGCACGGCGTGGTTTGGTAACTCTCAAAAAGCGACAGTCAGTTCTGTCCTCGGGCTGCTGCGTAGAAATCCCCATAGGCAGGGAAAAGCTATCATTATAAACCCCACTTGTCAGCAGCGTCTGGTCGTAGTCTCGACATAGTCTCTTTAGGCCCAAGGTTATATAGTCGGGCCTAGGTCTCAAGCAGTGGGGCGCCCAGTTAAAAATGAACCAGACGGCCATTCGCGTCCGACTCCTAATACCCTCGGTTTATGCCTAACCGCGCGACGAATCCACGGTGGGGCATGATGCAAGTGCTTTTTGACGCTCACTGTATTGTGAATAGGGTCCTACCAGAGCCGTACGAACGTAGTTGCCCCGGAGTCGCCATGTGGAAGAGATGGGCCTGGAAAAGAGACTCAGAGAATTACTTTGCATAGCCGCCACTATTCTCCGCCCACGAACGCCCGTGTCTACCACATTTAACCGGGTAGACACGCTCTGGGTTTTCTGATGAATGTCGTTGCTTTATCTCTGATCTACCAATAATTTACTATATGTACATTACTTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'.lower():
					#~ print(lines)
					toReturn = False
					break
		else:
			no = lines.split("_")[1][0].rstrip()
	outFile = Path(outDirectory + "/corrected_by_MSA1.fa")
	if not outFile.is_file():
		return False
	consensus1 = open(outDirectory + "/corrected_by_MSA1.fa", 'r')
	for lines in consensus1.readlines():
		if ">" not in lines:
			if  no == '0' :
				if not lines.rstrip() == 'ACTGGCGTGAATGACGGGGCTCCCGCAAGTCGAAAGGGGAGGAAGCTATCCTGTAAAAAGTGAAGAGAGACATCACGTGGATCGGCTGGTACCACAAAAGACGTGTCCGTTTGACCCATTAACCAGCCGGTTTGGGCTTTGGGCGAGTACTACAGGGTTCCGACAGTCGGCAATCGTTCACAAGAGATGTATGAAGGTAACCCCAGTAGCATCACGTGCCCCATCCGTAGAGTCTTTTTCATACAGTTAAACCACGCTGGCGAATTTGGTAGGAAGGTCGACACGCATAGCCACCTCACTTCGGGGAACGTCTTTCATCACGCCAGACGCAGTGACAACGGATGTGTTCATGTATTGTACCATATTCTTGCGTCAGTTGCACTTGTGAGGCTGATCTACTTGCGTCATAGAAATTGGCGCTAAGAATAAATTTACGCGCCACGACCGTTCAGCCACCACCAACTAGGCATTCCCCGGACCCGAGTATAACCCCGAGCGTCGGGTTACGAGTCATTTAGTATCCCTGTAAGGGCACAAGGACGCCCACCTAGGCTTCCGCGCTAGAGGAACCCTGTATAGGAAAGTATCGATTATCACAAGCCTAAGATGGCTGCCGCATAAGTACCATCCCGGAGGTGCCCGATACCCCACGGCGAGTTTAACGATCTCGCGCCGCTTGAAAGGCCGTACGACTGGGTCTGGGTAAATTGTTGGGTTAGTGATTTCCCCTCTCTGCTGAGAAGTGAGCACAGCTGCATGCGTAAGGCCAGACGGAAGGCGTCCCATGAGAGCAGCGCGACAAGGGGGACTGAATCAGTCCATGACTGGCGAGGGAACTAGGTGTGATCTAGTTATCTGTAATGTGTTTGACATTTCTTGTTAGTAGATATAATGTCCTATTCAGATTAGGTGGCCTTCGTAGATCTAGGAGCCGTGCTGACCGTGTGGACCATCAACATTCCCGTAAATGAATGAGCGACTCGTTGTAGGTTTAGGCGTACTCAGCGTTGCTATGTCCTTAGTTTCGATCAAACCGGTGGGTCGGTCTAAGAGCACGGCGTGGTTTGGTAACTCTCAAAAAGCGACAGTCAGTTCTGTCCTCGGGCTGCTGCGTAGAAATCCCCATAGGCAGGGAAAAGCTATCATTATAAACCCCACTTGTCAGCAGCGTCTGGTCGTAGTCTCGACATAGTCTCTTTAGGCCCAAGGTTATATAGTCGGGCCTAGGTCTCAAGCAGTGGGGCGCCCAGTTAAAAATGAACCAGACGGCCATTCGCGTCCGACTCCTAATACCCTCGGTTTATGCCTAACCGCGCGACGAATCCACGGTGGGGCATGATGCAAGTGCTTTTTGACGCTCACTGTATTGTGAATAGGGTCCTACCAGAGCCGTACGAACGTAGTTGCCCCGGAGTCGCCATGTGGAAGAGATGGGCCTGGAAAAGAGACTCAGAGAATTACTTTGCATAGCCGCCACTATTCTCCGCCCACGAACGCCCGTGTCTACCACATTTAACCGGGTAGACACGCTCTGGGTTTTCTGATGAATGTCGTTGCTTTATCTCTGATCTACCAATAATTTACTATATGTACATTACTTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'.lower():
				
					toReturn = False
					break
		else:
			no = lines.split("_")[1][0].rstrip()
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/corrected_by_MSA*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/cluster*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/msaAllReads.fa"])
	except subprocess.CalledProcessError:
		return False
	return toReturn


#test7
def test_simulated5pc_profileCorrection(imported):
	toReturn = True
	installDirectory = os.path.dirname(os.path.abspath(sys.argv[0])) + "/.."
	outDirectory = os.path.dirname(os.path.abspath(sys.argv[0]))
	alignment = imported.computePOA(os.path.dirname(os.path.abspath(sys.argv[0])) + "/simulatedLR_size_100_abund_50_5pc.fa", installDirectory, outDirectory)
	newMsa = imported.MSA()
	newMsa.getMSA(alignment)
	gaps = newMsa.findGapStretches()
	newMsa.prolongateNstretches(gaps)
	regions = newMsa.findRegions()
	newMsa.findPositionRegions(regions)
	newMsa.getIntervals()
	newMsa.writeConsensusByGapProfile(installDirectory, outDirectory)
	outFile = Path(outDirectory + "/corrected_by_MSA0.fa")
	if not outFile.is_file():
		return False
	consensus0 = open(outDirectory + "/corrected_by_MSA0.fa", 'r')
	for lines in consensus0.readlines():
		if ">" not in lines:
			if no == '0':
				if not lines.rstrip() == 'AGATGTGTCCCGACGCTGTGTGCAGGTAGTAGGAGAAATCCCTCGCTCTGTTACTTTGTCCTATAGAAGTCTAAAGGTACCTAGATCTCACTACGATGACGCAGCACTACCCACTCCAAGCCCCGACCGCGAGGTTTATTCAACCAGGATCCAGGTGTACATCGCAGAACTCCATGACCAAGGCAAATAAAATCTCGCCGGCATCTCCAGCCATGCAGAAGAAGCTTTAAGGCGCGCTTCCCGGAATAGTACTCTAAGACGGTAACGCAGGCTCTTCGAACACACACGGCTATGATTTATCGCATATCTTCAAGCCGGTATGAGCCCAACACTCCTCAAAACCATGCCACCTTCCAGGCGTCTGGACTCCACGCCCTGGTTTGTCTTTCAGACCGTGATTCTAAACCTTAGTTAGATCAAGGATTGTCGGAACAACCCACCGACGGCCTCCCTCCTTAAGGCGTCGATTAACGAGAGTCCCGTATATGAAAGCGCGACCACCCTCTTTGACTGAACATTATAGAGTTTAATCGAATTTTCTACCACGAACAACGAACTTCAGGGGGCCCAGAATCAATACAACGGCGTCCACTGTCAAGCCACCAGGACGCGTGACAGCCTCGGACTACCGGCTACGGGTATCTGCCGTGTGTCATGTATAGGGCGTTACTACAATGCCCTAAGCAGTAGGAAAGCGGCGTATAATTGAAATAACCATAACTCCTTTGCAACATCAGCTGCTCGTTTTGAAATCCGAAAGACTAAACTCACTTTCGGACAACATGGGTGGCTTATAAATGTGGTAATCCAAATGCTGGGGAAACCTGGGTTCCCCGCATGAATTGAGAGCGGCGTGCCATATCCAGTGGTAGTTCCTTGCAATCTGAATAATACCATAGCAACTTGCCGTGTAGGTAAGTTTGGTCGTGCCGGGGCATTTGCGTTAGACATATCGGGCCAGTTACCGGACCTGTGCTACAAAATCTCAAGAGCTTTACCCGGAACGCAGTCGTCCCAGCAACTGCGCCTGGCAGCCAGCGCGACACGATTCAAAGCGTTCCCGCATGACTTTGTAGCGGACTACTGTCCGGGTATTTCACCTATGACTGTATTAAGTTAACGTCTGAGAATCTTAGTCCCACATGCGCAGCGAATTTTGTACATCACACTGCAGCGTTTACCGCCCCAATTAATACTCGAAAGCGTTGGTGTAAACCCGCACGCCGGATACAATCTGACATTTTTTCACTCCATGCCCGCCTCGGGCAGGAGCTCAGGATCCCGCTTGACTGAAAGGCGCTGTACAAATATACAGGAGCATAGACGGTTCAGTTGCAGCTGATACGGCATCTAATCGGAGTCAGATTACATACTAGCGTCGAAGAATTGTCCACTCATCGTCGAAGGCACGGGGAGGTCATGCTACAAAGGTTAATGGATAGCGAGATCGTAACAAGACGGTCCACAGAACAGTGTCGGGACTCGTCATGGGGATGCAACAGCTCTGGGAAGCTTAGCAGTTACTTTATAGAGTTTTCCCCCTGCTGTATCTGCCCCACAATAGTTCGAGTCAGACCGACCGCTTGAAGGCTCCCACTGCCCTCATGCCTTTCAGAGTCACGTGTCTAACGCTCTTACACATACTTGCGTGTCCCAGACGGTACATAAAACCAGGTCACTTTGAATCTAGCGCCTCATCCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'.lower():
					
					print(lines.upper())
					toReturn = False
					break
		else:
			no = lines.split("_")[1][0].rstrip()
	outFile = Path(outDirectory + "/corrected_by_MSA1.fa")
	if not outFile.is_file():
		return False
	consensus1 = open(outDirectory + "/corrected_by_MSA1.fa", 'r')
	for lines in consensus1.readlines():
		if ">" not in lines:
			if  no == '0' :
				if not lines.rstrip() == 'AGATGTGTCCCGACGCTGTGTGCAGGTAGTAGGAGAAATCCCTCGCTCTGTTACTTTGTCCTATAGAAGTCTAAAGGTACCTAGATCTCACTACGATGACGCAGCACTACCCACTCCAAGCCCCGACCGCGAGGTTTATTCAACCAGGATCCAGGTGTACATCGCAGAACTCCATGACCAAGGCAAATAAAATCTCGCCGGCATCTCCAGCCATGCAGAAGAAGCTTTAAGGCGCGCTTCCCGGAATAGTACTCTAAGACGGTAACGCAGGCTCTTCGAACACACACGGCTATGATTTATCGCATATCTTCAAGCCGGTATGAGCCCAACACTCCTCAAAACCATGCCACCTTCCAGGCGTCTGGACTCCACGCCCTGGTTTGTCTTTCAGACCGTGATTCTAAACCTTAGTTAGATCAAGGATTGTCGGAACAACCCACCGACGGCCTCCCTCCTTAAGGCGTCGATTAACGAGAGTCCCGTATATGAAAGCGCGACCACCCTCTTTGACTGAACATTATAGAGTTTAATCGAATTTTCTACCACGAACAACGAACTTCAGGGGGCCCAGAATCAATACAACGGCGTCCACTGTCAAGCCACCAGGACGCGTGACAGCCTCGGACTACCGGCTACGGGTATCTGCCGTGTGTCATGTATAGGGCGTTACTACAATGCCCTAAGCAGTAGGAAAGCGGCGTATAATTGAAATAACCATAACTCCTTTGCAACATCAGCTGCTCGTTTTGAAATCCGAAAGACTAAACTCACTTTCGGACAACATGGGTGGCTTATAAATGAACTTGCCGTGTAGGTAAGTTTGGTCGTGCCGGGGCATTTGCGTTAGACATATCGGGCCAGTTACCGGACCTGTGCTACAAAATCTCAAGAGCTTTACCCGGAACGCAGTCGTCCCAGCAACTGCGCCTGGCAGCCAGCGCGACACGATTCAAAGCGTTCCCGCATGACTTTGTAGCGGACTACTGTCCGGGTATTTCACCTATGACTGTATTAAGTTAACGTCTGAGAATCTTAGTCCCACATGCGCAGCGAATTTTGTACATCACACTGCAGCGTTTACCGCCCCAATTAATACTCGAAAGCGTTGGTGTAAACCCGCACGCCGGATACAATCTGACATTTTTTCACTCCATGCCCGCCTCGGGCAGGAGCTCAGGATCCCGCTTGACTGAAAGGCGCTGTACAAATATACAGGAGCATAGACGGTTCAGTTGCAGCTGATACGGCATCTAATCGGAGTCAGATTACATACTAGCGTCGAAGAATTGTCCACTCATCGTCGAAGGCACGGGGAGGTTCATGCTACAAAGGTTAATGGATAGCGAGATCGTAACAAGACGGTCCACAGAACAGTGTCGGGACTCGTCATGGGGATGCAACAGCTCTGGGAAGCTTAGCAGTTACTTTATAGAGTTTTCCCCCTGCTGTATCTGCCCCACAATAGTTCGAGTCAGACCGACCGCTTGAAGGCTCCCACTGCCCTCATGCCTTCAGAGTCACGTGTCTAACGCTCTTACACATACTTGCGTGTCCCAGACGGTACATAAAACCAGGTCACTTTGAATCTAGCGCCTCATCCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'.lower():

				
				
					toReturn = False
					break
		else:
			no = lines.split("_")[1][0].rstrip()
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/corrected_by_MSA*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/cluster*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/msaAllReads.fa"])
	except subprocess.CalledProcessError:
		return False
	return toReturn



def test_simulated5pc_exonCorrection(imported):
	toReturn = True
	installDirectory = os.path.dirname(os.path.abspath(sys.argv[0])) + "/.."
	outDirectory = os.path.dirname(os.path.abspath(sys.argv[0]))
	alignment = imported.computePOA(os.path.dirname(os.path.abspath(sys.argv[0])) + "/simulatedLR_size_100_abund_50_5pc.fa", installDirectory, outDirectory)
	newMsa = imported.MSA()
	newMsa.getMSA(alignment)
	gaps = newMsa.findGapStretches()
	newMsa.prolongateNstretches(gaps)
	regions = newMsa.findRegions()
	newMsa.findPositionRegions(regions)
	newMsa.getIntervals()
	newMsa.writeConsensusByGapProfile(installDirectory, outDirectory)
	outFile = Path(outDirectory + "/corrected_by_MSA0.fa")
	if not outFile.is_file():
		return False
	consensus0 = open(outDirectory + "/corrected_by_MSA0.fa", 'r')
	for lines in consensus0.readlines():
		if ">" not in lines:
			if no == '0':
				if not lines.rstrip() == 'AGATGTGTCCCGACGCTGTGTGCAGGTAGTAGGAGAAATCCCTCGCTCTGTTACTTTGTCCTATAGAAGTCTAAAGGTACCTAGATCTCACTACGATGACGCAGCACTACCCACTCCAAGCCCCGACCGCGAGGTTTATTCAACCAGGATCCAGGTGTACATCGCAGAACTCCATGACCAAGGCAAATAAAATCTCGCCGGCATCTCCAGCCATGCAGAAGAAGCTTTAAGGCGCGCTTCCCGGAATAGTACTCTAAGACGGTAACGCAGGCTCTTCGAACACACACGGCTATGATTTATCGCATATCTTCAAGCCGGTATGAGCCCAACACTCCTCAAAACCATGCCACCTTCCAGGCGTCTGGACTCCACGCCCTGGTTTGTCTTTCAGACCGTGATTCTAAACCTTAGTTAGATCAAGGATTGTCGGAACAACCCACCGACGGCCTCCCTCCTTAAGGCGTCGATTAACGAGAGTCCCGTATATGAAAGCGCGACCACCCTCTTTGACTGAACATTATAGAGTTTAATCGAATTTTCTACCACGAACAACGAACTTCAGGGGGCCCAGAATCAATACAACGGCGTCCACTGTCAAGCCACCAGGACGCGTGACAGCCTCGGACTACCGGCTACGGGTATCTGCCGTGTGTCATGTATAGGGCGTTACTACAATGCCCTAAGCAGTAGGAAAGCGGCGTATAATTGAAATAACCATAACTCCTTTGCAACATCAGCTGCTCGTTTTGAAATCCGAAAGACTAAACTCACTTTCGGACAACATGGGTGGCTTATAAATGTGGTAATCCAAATGCTGGGGAAACCTGGGTTCCCCGCATGAATTGAGAGCGGCGTGCCATATCCAGTGGTAGTTCCTTGCAATCTGAATAATACCATAGCAACTTGCCGTGTAGGTAAGTTTGGTCGTGCCGGGGCATTTGCGTTAGACATATCGGGCCAGTTACCGGACCTGTGCTACAAAATCTCAAGAGCTTTACCCGGAACGCAGTCGTCCCAGCAACTGCGCCTGGCAGCCAGCGCGACACGATTCAAAGCGTTCCCGCATGACTTTGTAGCGGACTACTGTCCGGGTATTTCACCTATGACTGTATTAAGTTAACGTCTGAGAATCTTAGTCCCACATGCGCAGCGAATTTTGTACATCACACTGCAGCGTTTACCGCCCCAATTAATACTCGAAAGCGTTGGTGTAAACCCGCACGCCGGATACAATCTGACATTTTTTCACTCCATGCCCGCCTCGGGCAGGAGCTCAGGATCCCGCTTGACTGAAAGGCGCTGTACAAATATACAGGAGCATAGACGGTTCAGTTGCAGCTGATACGGCATCTAATCGGAGTCAGATTACATACTAGCGTCGAAGAATTGTCCACTCATCGTCGAAGGCACGGGGAGGTCATGCTACAAAGGTTAATGGATAGCGAGATCGTAACAAGACGGTCCACAGAACAGTGTCGGGACTCGTCATGGGGATGCAACAGCTCTGGGAAGCTTAGCAGTTACTTTATAGAGTTTTCCCCCTGCTGTATCTGCCCCACAATAGTTCGAGTCAGACCGACCGCTTGAAGGCTCCCACTGCCCTCATGCCTTTCAGAGTCACGTGTCTAACGCTCTTACACATACTTGCGTGTCCCAGACGGTACATAAAACCAGGTCACTTTGAATCTAGCGCCTCATCCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'.lower():
					
					print(lines.upper())
					toReturn = False
					break
		else:
			no = lines.split("_")[1][0].rstrip()
	outFile = Path(outDirectory + "/corrected_by_MSA1.fa")
	if not outFile.is_file():
		return False
	consensus1 = open(outDirectory + "/corrected_by_MSA1.fa", 'r')
	for lines in consensus1.readlines():
		if ">" not in lines:
			if  no == '0' :
				if not lines.rstrip() == 'AGATGTGTCCCGACGCTGTGTGCAGGTAGTAGGAGAAATCCCTCGCTCTGTTACTTTGTCCTATAGAAGTCTAAAGGTACCTAGATCTCACTACGATGACGCAGCACTACCCACTCCAAGCCCCGACCGCGAGGTTTATTCAACCAGGATCCAGGTGTACATCGCAGAACTCCATGACCAAGGCAAATAAAATCTCGCCGGCATCTCCAGCCATGCAGAAGAAGCTTTAAGGCGCGCTTCCCGGAATAGTACTCTAAGACGGTAACGCAGGCTCTTCGAACACACACGGCTATGATTTATCGCATATCTTCAAGCCGGTATGAGCCCAACACTCCTCAAAACCATGCCACCTTCCAGGCGTCTGGACTCCACGCCCTGGTTTGTCTTTCAGACCGTGATTCTAAACCTTAGTTAGATCAAGGATTGTCGGAACAACCCACCGACGGCCTCCCTCCTTAAGGCGTCGATTAACGAGAGTCCCGTATATGAAAGCGCGACCACCCTCTTTGACTGAACATTATAGAGTTTAATCGAATTTTCTACCACGAACAACGAACTTCAGGGGGCCCAGAATCAATACAACGGCGTCCACTGTCAAGCCACCAGGACGCGTGACAGCCTCGGACTACCGGCTACGGGTATCTGCCGTGTGTCATGTATAGGGCGTTACTACAATGCCCTAAGCAGTAGGAAAGCGGCGTATAATTGAAATAACCATAACTCCTTTGCAACATCAGCTGCTCGTTTTGAAATCCGAAAGACTAAACTCACTTTCGGACAACATGGGTGGCTTATAAATGAACTTGCCGTGTAGGTAAGTTTGGTCGTGCCGGGGCATTTGCGTTAGACATATCGGGCCAGTTACCGGACCTGTGCTACAAAATCTCAAGAGCTTTACCCGGAACGCAGTCGTCCCAGCAACTGCGCCTGGCAGCCAGCGCGACACGATTCAAAGCGTTCCCGCATGACTTTGTAGCGGACTACTGTCCGGGTATTTCACCTATGACTGTATTAAGTTAACGTCTGAGAATCTTAGTCCCACATGCGCAGCGAATTTTGTACATCACACTGCAGCGTTTACCGCCCCAATTAATACTCGAAAGCGTTGGTGTAAACCCGCACGCCGGATACAATCTGACATTTTTTCACTCCATGCCCGCCTCGGGCAGGAGCTCAGGATCCCGCTTGACTGAAAGGCGCTGTACAAATATACAGGAGCATAGACGGTTCAGTTGCAGCTGATACGGCATCTAATCGGAGTCAGATTACATACTAGCGTCGAAGAATTGTCCACTCATCGTCGAAGGCACGGGGAGGTTCATGCTACAAAGGTTAATGGATAGCGAGATCGTAACAAGACGGTCCACAGAACAGTGTCGGGACTCGTCATGGGGATGCAACAGCTCTGGGAAGCTTAGCAGTTACTTTATAGAGTTTTCCCCCTGCTGTATCTGCCCCACAATAGTTCGAGTCAGACCGACCGCTTGAAGGCTCCCACTGCCCTCATGCCTTCAGAGTCACGTGTCTAACGCTCTTACACATACTTGCGTGTCCCAGACGGTACATAAAACCAGGTCACTTTGAATCTAGCGCCTCATCCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'.lower():

				
				
					toReturn = False
					break
		else:
			no = lines.split("_")[1][0].rstrip()
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/corrected_by_MSA*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/cluster*.fa"])
	except subprocess.CalledProcessError:
		return False
	try:
		subprocess.check_output(['bash','-c', "rm "  + outDirectory + "/msaAllReads.fa"])
	except subprocess.CalledProcessError:
		return False
	return toReturn


def getFiles(pathToFiles, name): #for instance name can be "*.txt"
	os.chdir(pathToFiles)
	listFiles = []
	for files in glob.glob(name):
		listFiles.append(files)
	return listFiles

#8
def test_msa_MES(imported, currentDirectory):
	toReturn = True
	installDirectory = os.path.dirname(os.path.abspath(sys.argv[0])) + "/.."
	outDirectory = os.path.dirname(os.path.abspath(sys.argv[0]))
	alignment = imported.computePOA(os.path.dirname(os.path.abspath(sys.argv[0])) + "/msa_test_mes.fa", installDirectory, outDirectory)
	newMsa = imported.MSA()
	newMsa.getMSA(alignment)
	gaps = newMsa.findGapStretches()
	newMsa.prolongateNstretches(gaps)
	regions = newMsa.findRegions()
	newMsa.findPositionRegions(regions)
	newMsa.getIntervals()
	newMsa.writeConsensusByGapProfile(installDirectory, outDirectory)
	listOutFiles = getFiles(currentDirectory, "corrected_by*.fa")
	if len(listOutFiles) != 4:
		toReturn = False
	return toReturn
	


def main():
	currentDirectory = os.path.dirname(os.path.abspath(sys.argv[0]))
	filename = currentDirectory + "/../analyze_MSAv2.py"
	sys.path.append(os.path.dirname(filename))
	mname = os.path.splitext(os.path.basename(filename))[0]
	imported = importlib.import_module(mname)                       
	sys.path.pop()
	print("Starting tests...")
	

	#~ printTest(1, test_string_headers_exonsCorrection(imported))
	#~ printTest(2, test_string_headers_profileCorrection(imported))
	#~ printTest(3, test_errors_profileCorrection(imported))
	#~ printTest(4, test_simulatedPerfect_profileCorrection(imported))
	#~ printTest(5, test_simulatedPerfect_exonCorrection(imported))
	#~ printTest(6,test_simulated1pc_profileCorrection(imported))
	#~ printTest(7,test_simulated5pc_exonCorrection(imported))
	printTest(8,test_msa_MES(imported, currentDirectory))
	
		
if __name__ == '__main__':
	main()

