#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <iterator>
#include <ctime>
#include <unordered_map>
#include <algorithm>
#include <cmath>
#include <chrono>
#include <iostream>
#include <fstream>
#include <string>
#include <iterator>
#include <unordered_map>
#include <unordered_set>
#include <set>
#include <algorithm>
#include <chrono>
#include <map>
#include <set>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <random>


using namespace std;

uint32_t SD_GAUSSIAN(30);
uint32_t SIZE_TO_START_HOMOPOL(5);
uint32_t SIZE_MAX_HOMOPOLY(10);
uint32_t SIZE_MIN_HOMOPOLY(3);

//// get a random nucleotide ////
char randomNucleotide(){
	switch (rand() % 4){
		case 0:
			return 'A';
		case 1:
			return 'C';
		case 2:
			return 'G';
		case 3:
			return 'T';
	}
	return 'A';
}


//// create an insertion in homopolymers ////
string addHomopolymer(char& nuc){
	uint32_t length(0);
	while (length < 1){
		length = rand() % SIZE_MAX_HOMOPOLY;
	}
	string homopolymer(length, nuc);
	return homopolymer;
}


//// create a deletion in homopolymers ////
void removeHomopolymer(uint64_t size, string& result){
	string tmpResult("");
	uint32_t length(0);
	while (length < 1){
		length = rand() % (size - SIZE_MIN_HOMOPOLY);
	}
	for (uint64_t i(0); i < (result.size() - length); ++i){
		tmpResult += result[i];
	}
	result = tmpResult;
}


//// generate a random seq of a given length ////
string randomSequence(const uint32_t length){
	string result(length, 'A');
	for(uint32_t i(0); i < length; ++i){
		result[i] = randomNucleotide();
	}	
	return result;
}


//// add insertion ////
void insertion(double rate, string& result){
	uint64_t dice(rand() % 100*100*100);
	if(dice < rate){
		char newNucleotide(randomNucleotide());
		result.push_back(newNucleotide);
		insertion(rate, result);
	}
}


//// add errors in reads usinf the error profile ////
string mutateSequence(const string& referenceSequence){
	unordered_map<string, double> errorProfile;
	errorProfile.insert({"mismatches", 0.051537});
	errorProfile.insert({"non-homopolymer_ins", 0.007991});
	errorProfile.insert({"homopolymer_ins", 0.003356});
	errorProfile.insert({"non-homopolymer_del", 0.049073});
	errorProfile.insert({"homopolymer_del", 0.030472});

	string result, currentNuc, homopoly;
	result.reserve(5 * referenceSequence.size());
	// get each error value
	// sort mism/ins/del
	double errors [] = {errorProfile["mismatches"], errorProfile["non-homopolymer_ins"], errorProfile["non-homopolymer_del"], errorProfile["homopolymer_ins"], errorProfile["homopolymer_del"]};
	double errorsHomo [] = {errorProfile["homopolymer_ins"], errorProfile["non-homopolymer_del"]};
	vector<double> errorOrder(errors, errors + 3);
	vector<double> errorOrderHomo(errorsHomo, errorsHomo + 2);
	sort(errorOrder.begin(), errorOrder.end());
	sort(errorOrderHomo.begin(), errorOrderHomo.end());
	map<string, double> errorProfileSorted;
	map<string, double> errorProfileHomoSorted;
	double prevValue(0);
	for (auto&& val : errorOrder){
		if (val == errorProfile["mismatches"]){
			errorProfileSorted.insert({"mismatches", val * 100*100*100 + prevValue});
		} else if (val == errorProfile["non-homopolymer_ins"]){
			errorProfileSorted.insert({"non-homopolymer_ins", val * 100*100*100 + prevValue});
		} else if (val == errorProfile["non-homopolymer_del"]){
			errorProfileSorted.insert({"non-homopolymer_del", val * 100*100*100 + prevValue});
		}
		prevValue += val;
	}
	prevValue = 0;
	for (auto&& val : errorOrderHomo){
		if (val == errorProfile["homopolymer_ins"]){
			errorProfileHomoSorted.insert({"homopolymer_ins", val * 100*100*100 + prevValue});
		} else if (val == errorProfile["homopolymer_del"]){
			errorProfileHomoSorted.insert({"homopolymer_del", val * 100*100*100 + prevValue});
		}
		prevValue += val;
	}
	uint32_t i(0);
	bool isError(false);
	while (i < referenceSequence.size()){
		
		isError = false;
		uint dice(rand() % 100*100*100);
		//// homopolymers ////
		if (currentNuc.size() <  SIZE_TO_START_HOMOPOL){
			if (i > 0){
				if (currentNuc.empty()){
					currentNuc.push_back(result.back());
				} else {
					if (result.back() == currentNuc.back()){
						currentNuc.push_back(result.back());
					} else {
						currentNuc.back() = result.back();
					}
				}
			}
		} else {
			uint32_t probaHomopoly(rand() % 100*100*100);
			for (auto it(errorProfileHomoSorted.begin()); it !=errorProfileHomoSorted.end(); ++it){
				if (probaHomopoly < it->second){
					if (it->first == "homopolymer_del"){
						removeHomopolymer(currentNuc.size(), result);
						isError = true;
						break;
					} else if (it->first == "homopolymer_ins"){
						homopoly = addHomopolymer(currentNuc.back());
						result += homopoly;
						isError = true;
						break;
					}
				}
			}
			currentNuc = {};
		}
		if (not isError){  // if not already added an error in homopolymer at this base
			for (auto it(errorProfileSorted.begin()); it !=errorProfileSorted.end(); ++it){
				if (dice < it->second){
					if (it->first == "mismatches"){
						//SUBSTITUTION
						char newNucleotide(randomNucleotide());
						while(newNucleotide == referenceSequence[i]){
							newNucleotide = randomNucleotide();
						}
						result.push_back(newNucleotide);
						isError = true;
						break;
					} else if (it->first == "non-homopolymer_del"){
						// DELETION
						uint dice2(rand() % 100*100*100);
						while (dice2 < it->second and i < referenceSequence.size()){ // deletions larger than 1
							++i;
							dice2 = rand() % 100*100*100;
						}
						isError = true;
						break;
					} else if (it->first == "non-homopolymer_ins"){
						// INSERTION
						char newNucleotide(randomNucleotide());
						result.push_back(referenceSequence[i]);
						result.push_back(newNucleotide);
						insertion(it->second, result); // larger than 1 insertions
						isError = true;
						break;
					}
				} 
			}
			if (not isError){
				//NO ERROR
				result.push_back(referenceSequence[i]);
			}
		}
		++i;
	}
	return result;
}


//~ string mutateSequence(const string& referenceSequence, uint maxMutRate=13, vector <double> ratioMutation={0.37,0.09,0.54}){
	//~ string result;
	//~ result.reserve(5 * referenceSequence.size());
	//~ for(uint i(0); i < referenceSequence.size(); ++i){
		//~ uint mutRate(maxMutRate);
		//~ double substitutionRate(mutRate * ratioMutation[0]);
		//~ double insertionRate(mutRate * ratioMutation[1]);
		//~ double deletionRate(mutRate * ratioMutation[2]);
		//~ uint dice(rand() % 100);
		//~ if (dice <substitutionRate ){
			//~ //SUBSTITUTION
			//~ char newNucleotide(replaceRandomNucleotide(referenceSequence[i]));
			//~ result.push_back(newNucleotide);
			//~ continue;
		//~ } else if(dice < deletionRate+substitutionRate){
			//~ //DELETION
			//~ uint dice2(rand() % 100);
			//~ while (dice2 < deletionRate+substitutionRate){ // deletions larger than 1
				//~ ++i;
				//~ dice2 = rand() % 100;
			//~ }
			//~ continue;
		//~ } else if (dice < deletionRate + substitutionRate + insertionRate){
			//~ //INSERTION
			//~ char newNucleotide(randomNucleotide());
			//~ result.push_back(referenceSequence[i]);
			//~ result.push_back(newNucleotide);
			//~ insertion(deletionRate + substitutionRate + insertionRate, result); // larger than 1 insertions
			//~ continue;
		//~ } else {
			//~ result.push_back(referenceSequence[i]);
		//~ }
	//~ }
	//~ return result;
//~ }



vector<string> generateAlternativeTranscriptReferences(uint sizeSkippedExon, uint sizeExons=200, uint transcriptNumber=2, uint exonNumber=8){
	vector<string> result;
	vector<string> exonList;
	// generate sequences for regular exons
	for(uint i(0); i < exonNumber; ++i){
		exonList.push_back(randomSequence(sizeExons));
	}
	// skipped exons
	exonList.push_back(randomSequence(sizeSkippedExon));
	// transcripts
	for(uint i(0); i < transcriptNumber; ++i){
		string transcript;
		transcript.reserve((exonNumber+2)*sizeExons);
		if (i == 0){  // inclusion isoform
			uint ii(0);
			while (ii < exonList.size()/2){
				transcript += exonList[ii];
				++ii;
			}
			transcript += exonList.back(); // included exon in the middle
			while (ii < exonList.size() - 1){
				transcript += exonList[ii];
				++ii;
			}
		}else{ // exclusion isoform
			for(uint ii(0); ii < exonNumber; ++ii){
				transcript += exonList[ii];
			}
		}
		// add a last exon to inclusion
		if (i == 0){
			transcript += randomSequence(sizeExons);
		}
		// polyA tails
		for (uint a(0); a < 50; ++a){
			transcript += 'A';
		}
		result.push_back(transcript);
	}
	return result; // [0]inclusion, [1]exclusion
}






void generateReads(uint inclusionAbundance, vector<string>& transcripts, string& outSuffix, uint srCoverage=10, uint lrCoverage=100, uint errorRate=13, uint srLength=150){
	ofstream outLR("simulatedLR" + outSuffix + ".fa");
	ofstream outSR("simulatedSR" + outSuffix + ".fa");
	ofstream outRef("refSequences" + outSuffix + ".fa");
	ofstream outPerfectIncl("perfect_reads_inclusion" + outSuffix + ".fa");
	ofstream outPerfectExcl("perfect_reads_exclusion" + outSuffix + ".fa");
	ofstream outUncoIncl("uncorrected_reads_inclusion" + outSuffix + ".fa");
	ofstream outUncoExcl("uncorrected_reads_exclusion" + outSuffix + ".fa");
	string refSequence;
	uint numberSRInclusion(transcripts[0].size() / srLength * inclusionAbundance / 100 * srCoverage);
	uint numberSRExclusion(transcripts[1].size() / srLength * (100-inclusionAbundance) / 100 * srCoverage);
	uint numberLRInclusion(inclusionAbundance * lrCoverage / 100 );
	uint numberLRExclusion((100-inclusionAbundance) * lrCoverage / 100 );
	//~ uint numberSRInclusion(5);
	//~ uint numberSRExclusion(6);
	//~ uint numberLRInclusion(900);
	//~ uint numberLRExclusion((1000);
	cout << numberLRInclusion<< " " << numberLRExclusion << endl;
	cout << numberSRInclusion<< " " << numberSRExclusion << endl;
	string read;
	// LONG READS
	for (uint i(0); i < numberLRInclusion; ++i){
		read = mutateSequence(transcripts[0]);
		//~ read = mutateSequence(transcripts[0], errorRate);
		outLR << ">inclusion_" << i <<  endl << read << endl;
		outPerfectIncl << ">" << i << endl <<  transcripts[0] << endl;
		outUncoIncl << ">" << i << endl << read << endl;
	}
	for (uint i(0); i < numberLRExclusion; ++i){
		//~ read = mutateSequence(transcripts[1], errorRate);
		read = mutateSequence(transcripts[1]);
		outLR << ">exclusion_" << i << endl <<  read << endl;
		outPerfectExcl << ">" << i << endl << transcripts[1] << endl;
		outUncoExcl << ">" << i << endl << read << endl;
	}

	// SHORT READS
	uint start;
	for (uint i(0); i < numberSRInclusion; ++i){
		start = rand() % (transcripts[0].size() - srLength);
		outSR << ">inclusion_" << i << endl << transcripts[0].substr(start, srLength) << endl;
	}
	for (uint i(0); i < numberSRExclusion; ++i){
		start = rand() % (transcripts[1].size() - srLength);
		outSR << ">exclusion_" << i << endl << transcripts[1].substr(start, srLength) << endl;
	}

	// REF FILE
	outRef << ">inclusion" << endl << transcripts[0] << endl;
	outRef << ">exclusion" << endl << transcripts[1] << endl;
}






int main(int argc, char ** argv){
	if (argc > 2){
		uint sizeSkippedExon(stoi(argv[1]));
		uint inclusionAbundance(stoi(argv[2]));
		string outSuffix("");
		uint srCoverage(100), lrCoverage(10);
		if (argc > 3){
			outSuffix = argv[3];
			if (argc > 5){
				srCoverage = stoi(argv[4]);
				lrCoverage = stoi(argv[5]);
			}
		}
		srand (time(NULL));
		auto startChrono = chrono::system_clock::now();
		// generate reference sequences for alternative transcripts
		vector<string> transcripts(generateAlternativeTranscriptReferences(sizeSkippedExon));
		// generate reads (long and short)
		generateReads(inclusionAbundance, transcripts, outSuffix, srCoverage, lrCoverage);
		auto end = chrono::system_clock::now(); auto waitedFor = end - startChrono;
		cout << "Time  in ms : " << (chrono::duration_cast<chrono::milliseconds>(waitedFor).count()) << endl;
	} else {
		cout << "usage: ./ES_simulation <size skipped exon> <relative abudance inclusion isoform (%)> " << endl;
	}
	return 0;
}
