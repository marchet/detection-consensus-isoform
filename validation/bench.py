#!/usr/bin/env python3
import sys
import os
import shlex
import subprocess
from subprocess import Popen, PIPE, STDOUT
import re
import copy
import argparse
import glob
from benchmark_msa import *
from utils import *
from simulation import *












def main():

	currentDirectory = os.path.dirname(os.path.abspath(sys.argv[0]))
	installDirectory = os.path.dirname(os.path.realpath(__file__))

	##### TO MODIFY FOR BENCHMARKS #####
	skipped = [50,100]
	abund = [50,90]
	covLR = [20]
	EStype = "ES" # possible values "ES" (simple exon skipping), "multi" (3 isoforms), "wrong" (simple exon skipping + reads from another gene)
	correctors = ["msa_exon"]
		#order to keep for plots
	#LoRMA",
     #LoRDEC",
     # "Proovread",
     #"MECAT",
     #"PBDagCon",
     #"daccord",
     #"msa_exon",
     #"msa_isoform",
     #msa_both
     # "colorMap"
	####################################
	covSR = 50
	skippedS = [str(r) for r in skipped]
	abundS = [str(r) for r in abund]
	dictFig = {"ES": "/figs/sc1.png", "multi": "/figs/sc2.png", "wrong": "/figs/sc3.png"}
	# Manage command line arguments
	parser = argparse.ArgumentParser(description="Benchmark for quality assessment of long reads correctors.")
	# Define allowed options
	parser = argparse.ArgumentParser()
	parser.add_argument('-output', nargs='?', type=str, action="store", dest="outputDirPath", help="Name for output directory", default=None)
	parser.add_argument('-pdf', nargs='?', type=str, action="store", dest="pdf", help="Name for pdf outfile", default="expe")
	# get options for this run
	args = parser.parse_args()
	outputDirPath = args.outputDirPath
	readsFileName = None

	
	if not outputDirPath is None:
		if not os.path.exists(outputDirPath):
			os.mkdir(outputDirPath)
		else:
			printWarningMsg(outputDirPath+ " directory already exists, we will use it.")
			try:
				cmdRm = "(cd " + outputDirPath + " && rm *)"
				subprocess.check_output(['bash','-c', cmdRm])
			except subprocess.CalledProcessError:
				pass
		samDir = outputDirPath + "/results_sam"
		benchDir = outputDirPath + "/results_benchmark"
		correcDir  = outputDirPath + "/results_correctors"
	else:
		outputDirPath = currentDirectory
		samDir = currentDirectory + "/results_sam"
		benchDir = currentDirectory + "/results_benchmark"
		correcDir = currentDirectory + "/results_correctors"
	createDir(samDir)
	createDir(benchDir)
	createDir(correcDir)
	outputPDFName = args.pdf 

	
	cmdFile = '''echo "reference correction ratio soft coverage ratio_isof size" > ''' + benchDir + '''/all_confusion_matrix.txt'''
	subprocess.check_output(['bash','-c', cmdFile])
	cmdCat = '''echo "isof value soft coverage ratio_isof size mesure" > ''' + benchDir + "/precision_recall_correct_base_rate.txt"
	subprocess.check_output(['bash','-c', cmdCat])	

	if readsFileName is None:
		readsDir = outputDirPath + "/reads"
		createDir(readsDir)
		simulateReads(covSR, covLR, readsDir, skipped, abund, EStype, installDirectory)
	doBenchMsa = []
	doOthers = []
	for c in correctors:
		if "msa" in c:
			doBenchMsa.append(c)
		else:
			doOthers.append(c)
	
	if len(doBenchMsa) > 0:
		benchMsa(covLR, doBenchMsa, skippedS, abundS, currentDirectory,  outputDirPath, outputPDFName, readsDir, samDir, benchDir, correcDir)
	#~ if len(doOthers) > 0:
		#~ benchSelfCorrectors(errorRate, coverage, doOthers, skippedS, abundS, currentDirectory, errorRateToKeep, outputDirPath, outputPDFName)
	
	dictLatex = {"recall_prec": benchDir + "/recall_precision_correct_base_rate_withCoverages.png", "confusion_cov": benchDir + "/confusion_matrix_function_coverage_soft.png", "confusion_size_ratio": benchDir + "/confusion_matrix_function_exonSize_ratioIsoforms.png", "ES_type": currentDirectory + dictFig[EStype]}
	writeLatex(dictLatex, currentDirectory, benchDir, outputPDFName)


if __name__ == '__main__':
	main()
