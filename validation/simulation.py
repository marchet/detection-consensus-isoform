#!/usr/bin/env python3
import sys
import os
import shlex, subprocess
from subprocess import Popen, PIPE, STDOUT
import re
from utils import *

#~ def simulation(sizeSkipped, relAbund, suffix,  installDirectory, ESType="ES", covSR = 100, covLR = 10):
	#~ if ESType == "ES":
		#~ print(sizeSkipped, relAbund, suffix,  installDirectory, ESType, covSR, covLR)
		#~ cmdSimul = installDirectory + "/ES_simulation " + sizeSkipped + " " + relAbund + " " + suffix + " "+  str(covSR) + " " + str(covLR) 
		#~ print("*****cmd", cmdSimul)
		#~ cmdSimul = subprocessLauncher(cmdSimul)

######### utils for read simulation ######### 
def simulation(skipped, abund, coverage, directory, readsDir, EStype="ES",  covSR=100):
	#~ covSR = 10
	#~ for error in errorRate:
	#~ skippedS = [str(r) for r in skipped]
	#~ abundS = [str(r) for r in abund]
	#~ covS = [str(c) for c in coverage]
	for covLR in coverage:
		for sizeSkipped in skipped:
			for relAbund in abund:
				suffix = "_size_" + str(sizeSkipped) + "_abund_" + str(relAbund) + "_cov_" + str(covLR) 
				# simulation
				if checkIfFile(directory + "/simulatedLR"+ suffix +".fa" ):
					cmdRm = "rm " + directory + "/simulatedLR"+ suffix +".fa"
					subprocess.check_output(['bash','-c', cmdRm])
				if EStype == "ES":
					cmdSimul = directory + "/ES_simulation " + str(sizeSkipped) + " " + str(relAbund) + " " + str(suffix) +  " " + str(covLR) 
				elif EStype == "multi": #todo make error rate a parameter for this one
					cmdSimul =  directory + "/scenario2 " + str(sizeSkipped) + " " + str(relAbund) + " " + str(suffix) +  " " + str(covLR)
				elif EStype == "wrong":
					cmdSimul =  directory + "/scenario3 " + str(sizeSkipped) + " " + str(relAbund) + " " + str(suffix)  + " " + str(covLR) 
				cmdSimul = subprocessLauncher(cmdSimul)
				checkReadFiles(directory + "/simulatedLR"+ suffix +".fa")
				cmdMv = "mv *" + suffix + ".fa " + readsDir
				subprocess.check_output(['bash','-c', cmdMv])

def simulateReads(covSR, covLR, readsDir, skipped, abund, ESType, installDirectory):
	# check if dir for reads exist:
	if not readsDir is None:
		if not os.path.exists(readsDir):
			os.mkdir(readsDir)
		else:
		# first remove old reads
			for dirpath, dirnames, files in os.walk(readsDir):
				if files:
					cmdRm = "rm " + readsDir + "/*"
					subprocess.check_output(['bash','-c', cmdRm])
	#simulation
	#~ for sizeSkipped in skipped:
		#~ for relAbund in abund:
			#~ print(sizeSkipped, relAbund)
			#~ skippedString = str(sizeSkipped)
			#~ abundString = str(relAbund)
			#~ suffix = "_size_" + skippedString + "_abund_" + abundString
	covSR = 100
	simulation(skipped, abund, covLR, installDirectory, readsDir, ESType)
	



