#!/usr/bin/env python3
import sys
import os
import shlex, subprocess
from subprocess import Popen, PIPE, STDOUT
import re
from utils import *
import glob

# headers of reads file
def getHeaders(fileName):
	cmdGrep = """grep ">" """ + fileName
	val = subprocess.check_output(['bash','-c', cmdGrep])
	return val.decode('ascii').split("\n")[:-1] #last item is empty


# get sequence from reads file
def getSequences(fileName):
	cmdGrep = """grep ">" -v """ + fileName
	val = subprocess.check_output(['bash','-c', cmdGrep])
	return val.decode('ascii').split("\n")[:-1] #last item is empty
# get consensus sequence from reads file

def getOneSequence(fileName):
	cmdGrep = """grep ">" -v -m 1 """ + fileName
	val = subprocess.check_output(['bash','-c', cmdGrep])
	return val.decode('ascii').rstrip().upper() #last item is empty

def cleanHeaders(listHeaders):
	return [x.split(">")[1].split("_")[0] for x in listHeaders]

def uniqueHeaders(listHeaders):
	listTmp = sorted(listHeaders)
	previous = None
	listHeader = []
	for header in listTmp:
		if header != previous:
			previous = header
			listHeader.append(header)
	return listHeader


def getSequencesFromHeaders(headersList, fileName):
	listSeq = []
	for header in headersList:
		cmdGrep = """grep """ + header + """ -A 1 -m 1 """ + fileName
		val = subprocess.check_output(['bash','-c', cmdGrep])
		listSeq.append(val.decode('ascii').rstrip().split('\n')[1])
	return listSeq



def getFiles(pathToFiles, name, currentDirectory): #for instance name can be "*.txt"
	os.chdir(pathToFiles)
	listFiles = []
	for files in glob.glob(name):
		listFiles.append(files)
	os.chdir(currentDirectory)
	return listFiles
	

def alignOnRef(suffix, samDir, currentDir):
	filesToAlign = sorted(getFiles(samDir, suffix, currentDir)) #corrected is [0], reference is [1]
	samFile = open(samDir + "/results_" + suffix.split("*")[0] + ".sam", 'w')
	cmdAlign = currentDir + "/Complete-Striped-Smith-Waterman-Library/src/ssw_test " + samDir + "/" + filesToAlign[1] +  " " + samDir + "/" + filesToAlign[0] + " -c -s -h"
	p = subprocessLauncher(cmdAlign, samFile)
	samFile.close()


def createSequenceFile(samDir, listSequences, suffix):
	for couple in listSequences:
		out = open(samDir + "/" + couple[0] + "_" + suffix + ".fa", 'w')
		out.write(">" + couple[0] + "\n" + couple[1])
		out.close()



def main():
	currentDirectory = os.path.dirname(os.path.abspath(sys.argv[0]))
	installDirectory = os.path.dirname(os.path.realpath(__file__))
	readsFileName = None
	outDir = None
	if len(sys.argv) > 1:
		correctedFile = sys.argv[1]
		referenceFile = sys.argv[2]
	else:
		print("python3 valid_pierre.py corrected_reads.fa reference_reads.fa")
		sys.exit()
	samDir = currentDirectory + "/results_sam"
	createDir(samDir)

	
	##### get consensus sequences for alignment #####
	#extract each isoform from reference
	headRef = getHeaders(referenceFile)
	headRef = cleanHeaders(headRef)
	seqRef = getSequences(referenceFile)
	references = [[x, y] for x,y in zip(headRef, seqRef)]
	#extract each isoform from corrected reads
	headCor = getHeaders(correctedFile)
	headCor = cleanHeaders(headCor)
	headCor = uniqueHeaders(headCor)
	seqCor = getSequencesFromHeaders(headCor, correctedFile)
	corrected = [[x, y] for x,y in zip(headCor, seqCor)]
	##### align corrections on ref #####
	#create files for alignment
	createSequenceFile(samDir, references, "reference")
	createSequenceFile(samDir, corrected, "corrected")
	# align pairs of files
	for header in headRef:
		suffix = header + "*.fa"
		alignOnRef(suffix, samDir, currentDirectory)
	##### compute results from alignment #####
	# read each sam file and count indel/matches

if __name__ == '__main__':
	main()
