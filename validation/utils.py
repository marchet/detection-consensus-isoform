#!/usr/bin/env python3
import sys
import os
import shlex
import subprocess
from subprocess import Popen, PIPE, STDOUT
import re
import copy
import argparse
import glob



######### utils for warnings, user messages, errors ######### 

#warnings
def printWarningMsg(msg):
	print("[Warning] " + msg)

# to return if an error makes the run impossible
def dieToFatalError (msg):
  print("[FATAL ERROR] " + msg)
  sys.exit(1);


# check if file exists and is not empty
def checkIfFile(pathToFile):
	if not(os.path.exists(pathToFile) and os.path.getsize(pathToFile) > 0):
		return False
	return True


def checkReadFiles(readfiles):
	if readfiles is None:
		return True
	allFilesAreOK = True
	#~ for file in readfiles:
	if not os.path.isfile(readfiles):
		print("[ERROR] File \""+readfiles+"\" does not exist.")
		allFilesAreOK = False
	if not allFilesAreOK:
		dieToFatalError("One or more read files do not exist.")

######### utils for subprocess ######### 



# launch subprocess
def subprocessLauncher(cmd, argstdout=None, argstderr=None,	 argstdin=None):
	args = shlex.split(cmd)
	p = subprocess.call(args, stdin = argstdin, stdout = argstdout, stderr = argstderr)
	return p


######### utils for sequence files ######### 


# find files with a regex
def getFiles(pathToFiles, name, currentDirectory): #for instance name can be "*.txt"
	os.chdir(pathToFiles)
	listFiles = []
	for files in glob.glob(name):
		listFiles.append(files)
	os.chdir(currentDirectory)
	return listFiles

# return number of reads in a fasta
def getFileReadNumber(fileName):
	cmdGrep = """grep ">" -c """ + fileName
	val = subprocess.check_output(['bash','-c', cmdGrep])
	return int(val.decode('ascii'))

# return the sequence of an errorless isoform
def getPerfectSequence(fileName):
	cmdGrep = """grep "[ACGT]" -m 1 """ + fileName
	seq = subprocess.check_output(['bash','-c', cmdGrep])
	return seq.decode('ascii')

	
def getPerfectSequenceLength(fileName):
	cmdWc = """grep "[ACGT]" -m 1 """ + fileName + "| wc"
	seq = subprocess.check_output(['bash','-c', cmdWc])
	return int(seq.decode('ascii').split(" ")[-1].rstrip())

# headers of corrected reads file
def getCorrectedHeaders(fileName):
	cmdGrep = """grep ">" """ + fileName
	val = subprocess.check_output(['bash','-c', cmdGrep])
	return val.decode('ascii').split("\n")[:-1] #last item is empty


# get consensus sequence from corrected reads file
def getCorrectedSequence(fileName):
	cmdGrep = """grep ">" -v -m 1 """ + fileName
	val = subprocess.check_output(['bash','-c', cmdGrep])
	return val.decode('ascii').rstrip().upper() #last item is empty


# associate to isoform type the headers of the reference file
def makeReferenceHeadersList(readsDirectory, skipped, abund, cov, currentDirectory):
	listFilesPerfect = []
	listFilesPerfect = getFiles(readsDirectory, "perfect*_size_" + skipped + "_abund_" + abund + "_cov_" + cov   + ".fa", currentDirectory)
	refIsoformTypesToCounts = dict()
	refIsoformTypesToSeq = dict()
	#~ for fileP in listFilesPerfect:
		#~ typeIsoform = fileP.split("_")[2]
		#~ readNb = getFileReadNumber(readsDirectory + "/" + fileP) #get nb of reads
		#~ headers = [typeIsoform + str(x) for x in range(readNb)]
		#~ refIsoformTypesToCounts[typeIsoform] = headers
		#~ perfectSeq = getPerfectSequence(readsDirectory + "/" + fileP)
		#~ refIsoformTypesToSeq[typeIsoform] = perfectSeq.rstrip()
	return (listFilesPerfect, refIsoformTypesToCounts, refIsoformTypesToSeq)



#~ ######### utils for read simulation ######### 
#~ def simulateReads(skipped, abund, coverage, EStype, currentDirectory, errorRate):
	#~ covSR = 10
	#~ for error in errorRate:
		#~ for covLR in coverage:
			#~ for sizeSkipped in skipped:
				#~ for relAbund in abund:
					#~ suffix = "_size_" + str(sizeSkipped) + "_abund_" + str(relAbund) + "_cov_" + str(covLR) + "_err_" + str(error)
					#~ # simulation
					#~ if checkIfFile(currentDirectory + "/simulatedLR"+ suffix +".fa" ):
						#~ cmdRm = "rm " + currentDirectory + "/simulatedLR"+ suffix +".fa"
						#~ subprocess.check_output(['bash','-c', cmdRm])
					#~ if EStype == "ES":
						#~ cmdSimul = currentDirectory + "/ES_simulation " + str(sizeSkipped) + " " + str(relAbund) + " " + str(suffix) + " " +  str(covSR) + " " + str(covLR) + " " + str(error)
					#~ elif EStype == "MES": #todo make error rate a parameter for this one
						#~ cmdSimul =  currentDirectory + "/MES_simulation " + str(sizeSkipped) + " " + str(relAbund) + " " + str(suffix) +  " " + str(covLR)
					#~ elif EStype == "alt":
						#~ cmdSimul =  currentDirectory + "/AltSE_simulation " + str(sizeSkipped) + " " + str(relAbund) + " " + str(suffix) + " " +  str(covSR) + " " + str(covLR) + " " + str(error)
					#~ cmdSimul = subprocessLauncher(cmdSimul)
					#~ checkReadFiles(currentDirectory + "/simulatedLR"+ suffix +".fa")





def createDir(outDir):
	if not os.path.exists(outDir):
		os.mkdir(outDir)
	else:
		for dirpath, dirnames, files in os.walk(outDir):
			if files:
				cmdRm = "rm -r " + outDir + "/*"
				subprocess.check_output(['bash','-c', cmdRm])




######### utils for sam ######### 
def readSam(soft, suffix, pathSam):
	blockResults = dict()
	lenResults = dict()
	if os.path.exists(pathSam) and os.path.getsize(pathSam) > 0:
		samFile = open(pathSam, 'r')
		readsSize = []
		lines = samFile.readlines()
		queries = dict()
		for line in lines:
			line = line.rstrip().split('\t')
			query = line[0]
			target = line[2]
			cigar = line[5]
			start = int(line[3]) - 1
			length = len(line[9])
			seq = line[9]
			readsSize.append(length)
			blocks = re.compile("[0-9]+").split(cigar)[1:]
			resultAln = re.compile("[A-Z]|=").split(cigar)[:-1]
			alnLength = 0
			gapsLength = 0
			queries[query] = seq
			if len(blocks) == 1 and len(resultAln) == 1 and blocks[0] == '=': #aligned in one block
				blockResults[query] = {1:target}
				alnLength = int(resultAln[0])
			else:
				if query not in blockResults:
					blockResults[query] = {len(blocks):target}
				else:
					if 1 not in blockResults[query]:
						blockResults[query][len(blocks)] = target
				
				for b,r in zip(blocks, resultAln):
					
					if b != 'D' and b!= 'I':
						alnLength += int(r)
					else:
						gapsLength += int(r)
			if query not in lenResults:
				lenResults[query] = {target:[length, alnLength -start, gapsLength]}
			else:
				lenResults[query][target] = [length, alnLength -start, gapsLength]
		return ( start, readsSize, resultAln, gapsLength, blockResults, alnLength, lenResults, queries)
	else:
		return (None,) * 8






######### utils for latex ######### 

#dictLatex = {"recall_prec": benchDir + "/recall_precision_correct_base_rate_withCoverages.png", "confusion_cov": benchDir + "/confusion_matrix_function_coverage_soft.png", "confusion_size_ratio": benchDir + "/confusion_matrix_function_exonSize_ratioIsoforms.png"}
def writeLatex(options, currentDirectory, benchDir, outputPDFName):
	content = r'''\documentclass{article}
	\usepackage{graphicx}

	\begin{document}
	\section{Correction scenario}
	\begin{figure}[ht!]
	\centering\includegraphics[width=\textwidth]{%(ES_type)s}
	\caption{\textbf{Type of isoform cluster to correct.} Different alternative isoforms are presented with the expected alignment result.}
	\label{fig:isof}
	\end{figure}
	
	\section{Recall, precision, correct bases rate}
	\begin{figure}[ht!]
	\centering\includegraphics[width=0.8\textwidth]{%(recall_prec)s}
	\caption{\textbf{Corrector base-wise metrics.} Recall, precision and correct base rate values are computed after correction for each read experiment, using correctors in absciss. The coverages used are presented in vertical columns.}
	\label{fig:recall}
	\end{figure}
	

	\section{Isoform correction}
	
	\begin{figure}[ht!]
	 \centering\includegraphics[width=0.7\textwidth]{%(confusion_cov)s}
	 \caption{\textbf{Confusion matrix of isoforms, for the different coverages used.} Original isoforms are in absciss, corrected isoforms are in ordinate. For each pair we compute the number of corrected reads in a given isoform / original number of isoform. If no isoform was transformed during correction, the ratio is 1. Confusion matrix are presented for each correction method (horizontally).}
	 \label{fig:confusionCov}
	\end{figure}
	
	\begin{figure}[ht!]
	 \centering\includegraphics[width=0.7\textwidth]{%(confusion_size_ratio)s}
	 \caption{\textbf{Confusion matrix of isoforms, for isoform ratios and alternative exons sizes.} Original isoforms are in absciss, corrected isoforms are in ordinate. For each pair we compute the number of corrected reads in a given isoform / original number of isoform. If no isoform was transformed during correction, the ratio is 1. Horizontally we show the isoforms ratio used, and vertically the size of alternative exon. }
	 \label{fig:confusion}
	\end{figure}'''

	


	#~ if len(coverage) > 1:
				#~ content += r''' \subsection{Isoform conservation in function of coverage for exon method}
				#~ \begin{figure}[ht!]
				#~ \centering\includegraphics[width=0.7\textwidth]{%(isoformCoverage)s}
				 #~ \caption{\textbf{Confusion matrix of isoforms, for  exon correction method.} Original isoforms are in absciss, corrected isoforms are in ordinate. For each pair we compute the number of corrected reads in a given isoform / original number of isoform. If no isoform was transformed during correction, the ratio is 1. Confusion matrix are presented for each tested coverage}
				 #~ \label{fig:confusionError}
				#~ \end{figure}
				 #~ '''
		


	
	content += r'''
		\end{document} '''

	print(benchDir + "/" + outputPDFName +'.tex')
	with open(benchDir + "/" + outputPDFName +'.tex','w') as f:
		f.write(content%options)
	proc = subprocess.Popen(['pdflatex', '-output-directory', benchDir, outputPDFName + ".tex"])
	proc.communicate()





######### utils for R functions ######### 
#modified 07/25
def printConfusionMatrix(currentDirectory, outDir):
	#~ Rcmd = "Rscript " + currentDirectory + "/R_scripts/plot_confusion_matrix.R " + confusionFile + " " + corrector  + " " + outDir
	Rcmd = "Rscript " + currentDirectory + "/R_scripts/plot_confusion_matrix_july.R " + outDir + "/all_confusion_matrix.txt " + outDir 
	#~ Rcmd = "Rscript " + currentDirectory + "/R_scripts/plot_all_confusion_matrix.R " + outDir + "/all_confusion_matrix.txt " + outDir + " " + str(coverageToKeep)
	subprocessLauncher(Rcmd)


def printConfusionMatrixFunctionOf(currentDirectory, coverageToKeep, outDir):
	#~ if errorToKeep is not None:
		#~ Rcmd = "Rscript " + currentDirectory + "/plot_confusion_matrix_function_error.R " + currentDirectory + "/all_confusion_matrix.txt " + currentDirectory + " "  + str(coverageToKeep)
		#~ subprocessLauncher(Rcmd)
	if coverageToKeep is not None:
		Rcmd = "Rscript " + currentDirectory + "/R_scripts/plot_confusion_matrix_function_coverage.R " + outDir + "/all_confusion_matrix.txt " + currentDirectory 
		subprocessLauncher(Rcmd)
	

def printMetrics(currentDirectory, benchDir):
	#~ cmdR = "Rscript " + currentDirectory + "/R_scripts/plot_recall.R " + benchDir + "/recall_cov_" + str(cov)  + ".txt " +  benchDir
	#~ subprocessLauncher(cmdR)
	#~ cmdR = "Rscript " + currentDirectory + "/R_scripts/plot_precision.R " +  benchDir + "/precision_cov_" + str(cov) +".txt " +  benchDir
	#~ subprocessLauncher(cmdR)
	#~ cmdR = "Rscript " + currentDirectory + "/R_scripts/plot_correct_base_rate.R " +  benchDir + "/correct_base_rate_cov_" + str(cov)   +".txt " +  benchDir
	#~ subprocessLauncher(cmdR)
	#~ cmdR = "Rscript " + currentDirectory + "/R_scripts/plot_size.R " +  benchDir + "/sizes_reads_cov_" + str(cov) + ".txt " +  benchDir
	#~ subprocessLauncher(cmdR)
	#~ cmdR = "Rscript " + currentDirectory + "/plot_all_metrics_coverage.R " +  benchDir + "/all_recall_precision.txt " +  benchDir
	#~ subprocessLauncher(cmdR)
	cmdR = "Rscript " + currentDirectory + "/R_scripts/plot_recall_precision_correctRate_july.R " +  benchDir + "/precision_recall_correct_base_rate.txt " +  benchDir
	subprocessLauncher(cmdR)


def printGlobalMetrics(currentDirectory, benchDir):
	cmdR = "Rscript " + currentDirectory + "/R_scripts/plot_all_precisions_softs.R " + benchDir + "/all_precisions_softs.txt " + benchDir 
	subprocessLauncher(cmdR)
	cmdR = "Rscript " + currentDirectory + "/R_scripts/plot_all_recalls_softs.R " + benchDir + "/all_recalls_softs.txt " + benchDir 
	subprocessLauncher(cmdR)
	cmdR = "Rscript " + currentDirectory + "/R_scripts/plot_all_correctRate_softs.R " + benchDir + "/all_correctRates_softs.txt " + benchDir 
	subprocessLauncher(cmdR)


#~ def printMetricErrorRates(currentDirectory, cov):
	#~ cmdR = "Rscript " + currentDirectory + "/plot_all_metrics_errorrate.R " + currentDirectory + "/all_recall_precision.txt " + str(cov) + " " + currentDirectory
	#~ subprocessLauncher(cmdR)

