#!/usr/bin/env python3
import sys
import os
import shlex
import subprocess
from subprocess import Popen, PIPE, STDOUT
import re
import copy
import argparse
import glob
from utils import *
#~ from compareTriplets import *



def msa(suffix, msaType, readsDir, outDir = "/home/marchet/detection-consensus-isoform/results"):
	if msaType == "msa_isoform":
		cmdMSA = "python /home/marchet/detection-consensus-isoform/analyze_MSAv2.py -r "+ readsDir + "/simulatedLR" + suffix + ".fa -c isoform -o " + outDir
	elif msaType == "msa_exon":
		cmdMSA = "python /home/marchet/detection-consensus-isoform/analyze_MSAv2.py -r "+ readsDir + "/simulatedLR" + suffix + ".fa -o " + outDir
	elif msaType == "msa_sparc":
		cmdMSA = "python /home/marchet/detection-consensus-isoform/analyze_MSAv2.py -r "+ readsDir + "/simulatedLR" + suffix + ".fa -o " + outDir
	elif msaType == "msa_both":
		cmdMSA = "python /home/marchet/detection-consensus-isoform/analyze_MSAv2.py -r "+ readsDir + "/simulatedLR" + suffix + ".fa -c -o " + outDir
	p = subprocessLauncher(cmdMSA)

# associate to isoform type the headers of the reference file
def makeReferenceHeadersList(readsDir, skipped, abund, cov, currentDirectory):
	listFilesPerfect = getFiles(readsDir, "perfect*_size_" + skipped + "_abund_" + abund + "_cov_" + cov  + ".fa", currentDirectory)
	refIsoformTypesToCounts = dict()
	refIsoformTypesToSeq = dict()
	for fileP in listFilesPerfect:
		typeIsoform = fileP.split("_")[2]
		readNb = getFileReadNumber(readsDir + "/" + fileP) #get nb of reads
		headers = [typeIsoform + str(x) for x in range(readNb)]
		refIsoformTypesToCounts[typeIsoform] = headers
		perfectSeq = getPerfectSequence(readsDir + "/" + fileP)
		refIsoformTypesToSeq[typeIsoform] = perfectSeq.rstrip()
	return (listFilesPerfect, refIsoformTypesToCounts, refIsoformTypesToSeq)



# compare headers coming from an isoform in ref file to headers attributed to an isoform in correction file
def compareRefAndCorrectedHeaders(refIsoformTypesToCounts, correcIsoformTypesToCounts):
	count = dict()
	for typeIsoform in refIsoformTypesToCounts.keys():
		count[typeIsoform] = dict()
		for correcIsoform in correcIsoformTypesToCounts.keys():
			for headersRef in refIsoformTypesToCounts[typeIsoform]:
				for headersCor in correcIsoformTypesToCounts[correcIsoform]:
					if headersRef == headersCor:
						if correcIsoform in count[typeIsoform].keys():
							count[typeIsoform][correcIsoform] += 1
						else:
							count[typeIsoform][correcIsoform] = 1
	return count #for instance {'exclusion': {'exclusion': 5}, 'inclusion': {'inclusion': 5}}
	

# associate to isoform type the headers of the corrected file
def makeCorrectedHeadersList(resultDirectory, currentDirectory, skipped, abund, suffix, refIsoformTypesToCounts):
	listFilesCorrected = getFiles(resultDirectory, "corrected_by_MSA*.fa", currentDirectory)
	correcIsoformTypesToCounts = dict()
	correcIsoformTypesToSeq = dict()
	for fileC in listFilesCorrected:
		fileC = resultDirectory + "/" + fileC
		correctedSequence = getCorrectedSequence(fileC)
		listHeaders = getCorrectedHeaders(fileC)
		listHeaders = [x.split("_")[0][1:] + x.split("_")[1].split(' ')[0] for x in listHeaders] #For instance ['exclusion0', 'exclusion1', 'exclusion2', 'exclusion3', 'exclusion4']
		listIsoforms = [x.split("_")[0][:-1] for x in listHeaders]
		mainIsoform = max(listIsoforms,key=listIsoforms.count)
		correcIsoformTypesToCounts[mainIsoform] = listHeaders
		correcIsoformTypesToSeq[mainIsoform] = correctedSequence
		#~ for isoformType in set(listIsoforms): #unique types of isoforms
			#~ correcIsoformTypesToCounts[isoformType] = listHeaders
			#~ correcIsoformTypesToSeq[isoformType] = correctedSequence
	return(correcIsoformTypesToCounts, correcIsoformTypesToSeq)



	

# compute ratio of isoforms representations in ref and corrected files
# todo files with reference correction ratio soft coverage errorrate 
def computeRatioIsoforms(refIsoformTypesToCounts, correcIsoformTypesToCounts, currentDirectory, suffix, soft, coverage, benchDir, sizeExon, ratioIsof):
	counts = compareRefAndCorrectedHeaders(refIsoformTypesToCounts, correcIsoformTypesToCounts)
	confusionName = benchDir + "/matrix_confusion" + suffix +  ".txt"
	outConf = open(confusionName, 'w')
	outConf.write("reference correction ratio\n")
	isCorrect = True
	ratios = dict()
	for ref in counts:
		for ref2 in counts:
			if ref2 in counts[ref].keys():
				ratio = counts[ref][ref2] * 1.0 / len(refIsoformTypesToCounts[ref]) if len(refIsoformTypesToCounts[ref]) != 0 else 0
				if ratio != 1:
					isCorrect = False
				if ref in ratios.keys():
					if ref2 in ratios[ref].keys():
						ratios[ref][ref2].append(ratio)
					else:
						ratios[ref][ref2] = [ratio]
				else:
					ratios[ref] = dict()
					ratios[ref][ref2] = [ratio]
	for ref in ratios.keys():
		for ref2 in ratios.keys():
			if ref2 in ratios[ref].keys():
				meanRatio = sum(ratios[ref][ref2])/len(ratios[ref][ref2]) if len(ratios[ref][ref2]) > 0 else 0
				outConf.write(ref + " " + ref2 + " " + str(meanRatio) + "\n")
				cmdEcho = "echo " + ref + " " + ref2 + " " + str(meanRatio) + " " + soft + " " + str(coverage) +  " " + str(ratioIsof) +  " " +  str(sizeExon) + " >> " + benchDir + "/all_confusion_matrix.txt"  #reference correction ratio soft coverage ratio_isof size
				subprocess.check_output(['bash','-c', cmdEcho])
			else:
				outConf.write(ref + " " + ref2 + " 0\n")
				cmdEcho = "echo " + ref + " " + ref2 + " 0 " + soft + " " + str(coverage) +  " " + str(ratioIsof) +  " " +  str(sizeExon) + " >> " + benchDir + "/all_confusion_matrix.txt"
				subprocess.check_output(['bash','-c', cmdEcho])

	outConf.close()
	return confusionName, isCorrect




def alignOnRefMsa(soft, skipped, abund, currentDirectory, resultDirectory, cov, readsDir, samDir, correctorsDir):
	suffix = "_size_" + str(skipped) + "_abund_" + str(abund) + "_cov_" + str(cov) 
	listFileNames = getFiles(resultDirectory, "corrected_by_MSA*.fa", currentDirectory)#todo change here MSA => specific
	for fileC in listFileNames:
		isoform = getCorrectedHeaders(resultDirectory + "/" + fileC)[0].split("_")[0][1:]
		cmdGrep = "grep "+ isoform + " " + readsDir + "/refSequences" + suffix + ".fa -A 1 > " + readsDir + "/refSequences" + isoform + suffix + ".fa"
		subprocess.check_output(['bash','-c', cmdGrep])
		cmdGrep = "grep "+ isoform + " " + fileC + " -A 1 > toalign.fa"
		subprocess.check_output(['bash','-c', cmdGrep])
		samFile = open(samDir + "/results" + isoform + soft + suffix + ".sam", 'w')
		cmdAlign = "/home/marchet/bin/Complete-Striped-Smith-Waterman-Library/src/ssw_test " + readsDir +"/refSequences" + isoform + suffix + ".fa toalign.fa -c -s"
		p = subprocessLauncher(cmdAlign, samFile)
		samFile.close()
		cmdCp = "cp " + resultDirectory + "/" +  fileC + " " + correctorsDir + "/corrected_reads_by_" + soft + "_" + isoform + suffix + ".fa"
		subprocess.check_output(['bash','-c', cmdCp])





def computeResultsIsoforms(correc, currentDirectory, skippedExon, abundanceMajor, suffix, refIsoformTypesToCounts, cov, allCoverages, outDirMSA, outDirBench):
	correcIsoformTypesToCounts, correcIsoformTypesToSeq = makeCorrectedHeadersList(outDirMSA, currentDirectory, skippedExon, abundanceMajor, suffix, refIsoformTypesToCounts)
	confusionFile, isCorrect = computeRatioIsoforms(refIsoformTypesToCounts, correcIsoformTypesToCounts, currentDirectory, suffix, correc, cov, outDirBench, skippedExon, abundanceMajor)
	#~ if cov == covToPrint :
	#~ print("*****************",outDirBench)
	#~ printConfusionMatrix(currentDirectory, correc, confusionFile, suffix, cov, outDirBench)
	#~ if len(allCoverages) > 1:
		#~ printConfusionMatrixFunctionOf(currentDirectory, covToPrint, outDirBench)
	return isCorrect, correcIsoformTypesToCounts, confusionFile


def getSequencesFromHeaders(headersList, fileName):
	for header in headersList:
		cmdGrep = """grep """ + header + """ -A 1 """ + fileName
		val = subprocess.check_output(['bash','-c', cmdGrep])
		listSeq = val.decode('ascii').rstrip().split('\n')
	for i in range(len(listSeq)):
		if i % 2 == 0:
			listSeq[i] = ">" + listSeq[i].split("_")[1].split(" ")[0]
	return listSeq


#~ def transformCorrectedReadsHeaders():
	

def computeResultsRecallPrecision(soft, skipped, abund, currentDirectory, refIsoformTypesToSeq, outSize, cov, benchDir, readsDir, samDir, correctorsDir, msaDir):
	
	suffix = "_size_" + str(skipped) + "_abund_" + str(abund) + "_cov_" + str(cov)  
	cmdFile = "> " + benchDir + "/precision_tmp.txt"
	subprocess.check_output(['bash','-c', cmdFile])
	cmdFile = "> " + benchDir + "/recall_tmp.txt"
	subprocess.check_output(['bash','-c', cmdFile])
	cmdFile = "> " + benchDir + "/correct_base_rate_tmp.txt"
	subprocess.check_output(['bash','-c', cmdFile])
	# expectedLengths = getExpectedLength(currentDirectory, suffix, isofType)
	# ratioLen = []
	for isofType in refIsoformTypesToSeq:
		#~ print(isofType)
		corrected = os.path.abspath(msaDir + "/corrected_" + isofType + suffix + ".fa")
		perfect = os.path.abspath(readsDir + "/perfect_reads_" + isofType + suffix + ".fa")
		uncorrected = os.path.abspath(readsDir + "/uncorrected_reads_" + isofType + suffix + ".fa" )
		seq = getSequencesFromHeaders([isofType], msaDir + "/all_corrected.fa")
		toAlignFile = open(corrected, 'w')
		for line in seq:
			toAlignFile.write(line + "\n")
		toAlignFile.close()
		#~ expectedLengths = getPerfectSequenceLength(readsDir + "/perfect_reads_" + isofType + suffix + ".fa")
							
		#~ pathSam = samDir + "/results" + isofType + soft + suffix + ".sam"

		#~ start, readsSize, resultAln, gapsLength, blockResults, alnLength, lenResults, queries = readSam(soft, suffix, pathSam)
		#meanSizes[isofType] = {"realSize" : [], "alignedSize" : []}
		#for querySeq, aln in blockResults.items():
			# meanSizes[isofType]["realSize"].append(lenResults[querySeq][isofType][0])
			#meanSizes[isofType ]["alignedSize"].append(lenResults[querySeq][isofType][1])
		#~ meanReadsSize = round(sum(readsSize)*1.0/len(readsSize),2) if len(readsSize) > 0 else 0
		#~ ratioLen = round(meanReadsSize*100/expectedLengths,2)
		#~ outSize.write(soft + " " + str(ratioLen) + " " +  str(skipped) + " "+ str(abund) +"\n")

		# ratioLenE = round(meanExclusionCorrectedSize*100/expectedLengths["exclusion"],2)
		
		#~ cmdHead = "head -1 "  + readsDir + "/perfect_reads_" + isofType + suffix + ".fa > " + readsDir + "/corrected.fa" # to have the same header
		#~ subprocess.check_output(['bash','-c', cmdHead])
		#~ cmdHead = "head -2 " + correctorsDir + "/corrected_reads_by_" + soft + "_" + isofType + suffix + ".fa | tail -1 >> " + readsDir + "/corrected.fa"
		#~ subprocess.check_output(['bash','-c', cmdHead])
		#~ cmdHead = "head -2 " + readsDir + "/uncorrected_reads_"  + isofType + suffix + ".fa > " + readsDir + "/uncorrected.fa"
		#~ subprocess.check_output(['bash','-c', cmdHead])
		#~ cmdHead = "head -2 " + readsDir + "/perfect_reads_" + isofType + suffix + ".fa > " + readsDir + "/perfect.fa"
		#~ subprocess.check_output(['bash','-c', cmdHead])
		########################### TO MODIFY WITH ELECTOR
		os.chdir("ELECTOR")
		cmdBench = "python elector.py -corrected " + corrected + " -uncorrected " + uncorrected + " -perfect " + perfect
		#~ print("000000000000000000000000000000000000000000000000000000" + cmdBench)
		subprocessLauncher(cmdBench)
		os.chdir(currentDirectory)
		#~ subprocess.Popen("python " + currentDirectory + "/elector.py -corrected " + readsDir + "/corrected.fa -uncorrected " + readsDir + "/uncorrected.fa -perfect " + readsDir + "/perfect.fa", shell=True)
		cmdMv = "mv ELECTOR/log " + benchDir + "/log_elector_" + isofType + suffix
		subprocess.check_output(['bash','-c', cmdMv])
		#~ cmdMv = "mv ../ELECTOR/summary* " + benchDir
		#~ subprocess.check_output(['bash','-c', cmdMv])
		#~ cmdMv = "mv " + currentDirectory + "/plot_recall_precision.png " + benchDir
		#~ subprocess.check_output(['bash','-c', cmdMv])
		#~ cmdMv = "mv " + currentDirectory + "/plot_size_distribution.png " + benchDir
		#~ subprocess.check_output(['bash','-c', cmdMv])
		#~ cmdMv = "mv " + currentDirectory + "/per_read_metrics.txt " + benchDir
		#~ subprocess.check_output(['bash','-c', cmdMv])
		#~ if checkIfFile(currentDirectory + "/skipped_reads.txt "):
			#~ cmdMv = "mv "+ currentDirectory + "/skipped_reads.txt "  + benchDir
			#~ subprocess.check_output(['bash','-c', cmdMv])
		#~ cmdRm = "rm " + currentDirectory + "/msa.fa msa_profile.txt corrected_sorted.fareference_sorted_duplicated.fa reference_sorted.fa"
		#~ subprocess.check_output(['bash','-c', cmdRm])

		#~ #todo move everything and rm msa




		
		cmdGrep  ='''grep Precision ''' + benchDir +  '''/log_elector_''' + isofType + suffix 
		precision =subprocess.check_output(['bash','-c', cmdGrep]).decode('ascii').strip().split(":")[1]
		cmdGrep  ='''grep Recall ''' + benchDir +  '''/log_elector_''' + isofType + suffix 
		recall =subprocess.check_output(['bash','-c', cmdGrep]).decode('ascii').strip().split(":")[1]
		cmdGrep  ='''grep "correct bases rate" ''' + benchDir +  '''/log_elector_''' + isofType + suffix 
		correct =subprocess.check_output(['bash','-c', cmdGrep]).decode('ascii').strip().split(":")[1]

		#~ cmdCat = '''echo "''' + soft + " " + precision + '''" >> ''' + benchDir + "/precision_tmp.txt"
		#~ subprocess.check_output(['bash','-c', cmdCat])
		#~ cmdCat = '''echo "''' + soft + " " + recall + '''" >> ''' + benchDir + "/recall_tmp.txt"
		#~ subprocess.check_output(['bash','-c', cmdCat])

		#~ cmdCat = '''echo "''' + soft + " " + correct + '''" >> ''' + benchDir + "/correct_base_rate_tmp.txt"
		#~ subprocess.check_output(['bash','-c', cmdCat])

		#isof value soft coverage ratio_isof size mesure
		cmdCat = '''echo "''' + isofType + " " + precision + " " + soft + " " + str(cov) + " " + str(abund) + " " + str(skipped) + ''' precision" >> ''' + benchDir + "/precision_recall_correct_base_rate.txt"
		subprocess.check_output(['bash','-c', cmdCat])
		cmdCat = '''echo "''' + isofType + " " + recall + " " + soft + " " + str(cov) + " " + str(abund) + " " + str(skipped) + ''' recall" >> ''' + benchDir + "/precision_recall_correct_base_rate.txt"
		subprocess.check_output(['bash','-c', cmdCat])
		cmdCat = '''echo "''' + isofType + " " + correct + " " + soft + " " + str(cov) + " " + str(abund) + " " + str(skipped) + ''' correct_base_rate" >> ''' + benchDir + "/precision_recall_correct_base_rate.txt"
		subprocess.check_output(['bash','-c', cmdCat])





def benchMsa(coverage, correctors, skippedS, abundS, currentDirectory, outputDirPath, outputPDFName, readsDir, samDir, benchDir, correctorsDir):
	#~ for error in errorRate:
		outDirMSA = correctorsDir + "/results_MSA"
		for covLR in coverage:
			outSize = open(benchDir + "/sizes_reads_cov_"+ str(covLR)+ ".txt", 'w')
			outSize.write("soft size skipped abund\n")
			for correc in correctors:
				for skippedExon in skippedS:
					for abundanceMajor in abundS:
						#~ #1 - get which reads is which isoform
						listFilesPerfect, refIsoformTypesToCounts, refIsoformTypesToSeq = makeReferenceHeadersList(readsDir, str(skippedExon), str(abundanceMajor), str(covLR), currentDirectory)
						#~ print("OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO", refIsoformTypesToSeq, "OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO")
						suffix = "_size_" + str(skippedExon) + "_abund_" + str(abundanceMajor) + "_cov_" + str(covLR)
						#~ #2 - launch msa 
						msa(suffix, correc, readsDir, outDirMSA)
						#3- get to which isoform each read was corrected, then check if all read kept its isoform through correction
						isCorrect, isoforms, confusionFile = computeResultsIsoforms(correc, currentDirectory, skippedExon, abundanceMajor, suffix, refIsoformTypesToCounts, covLR,  coverage, outDirMSA, benchDir)
						computeResultsRecallPrecision(correc, skippedExon, abundanceMajor, currentDirectory, refIsoformTypesToSeq.keys(), outSize, covLR, benchDir, readsDir, samDir, correctorsDir, outDirMSA)
						#~ if isCorrect: # all reads were corrected to the right isoform
		printConfusionMatrix(currentDirectory, benchDir)
		printMetrics(currentDirectory, benchDir)
							#~ # 4- align corrected reads to ref and uncorrected version to compute recall and precision
							#~ alignOnRefMsa(correc, skippedExon, abundanceMajor, currentDirectory, outDirMSA, covLR, readsDir, samDir, correctorsDir)
							#~ computeResultsRecallPrecision(correc, skippedExon, abundanceMajor, currentDirectory, isoforms.keys(), outSize, covLR, benchDir, readsDir, samDir, correctorsDir)
			#~ cmdMv = "mv " + benchDir + "/recall_tmp.txt " + benchDir + "/recall_cov_"+ str(covLR) +  ".txt"
			#~ subprocess.check_output(['bash','-c', cmdMv])
			#~ cmdMv = "mv " + benchDir + "/precision_tmp.txt " + benchDir + "/precision_cov_"+ str(covLR)+ ".txt"
			#~ subprocess.check_output(['bash','-c', cmdMv])
			#~ outSize.close()
			#~ cmdMv = "mv " + benchDir + "/correct_base_rate_tmp.txt " + benchDir + "/correct_base_rate_cov_"+ str(covLR)  +".txt"
			#~ subprocess.check_output(['bash','-c', cmdMv])

			
			#~ cmdAwk = '''awk '{print $0,''' + str(covLR) +  '''}' ''' + benchDir + '''/precision_cov_'''+ str(covLR)  + '''.txt >>''' +  benchDir +  '''/all_precisions_softs.txt'''
			#~ subprocess.check_output(['bash','-c', cmdAwk])
			#~ cmdAwk = '''awk '{print $0,''' + str(covLR) + '''}' ''' +  benchDir + '''/recall_cov_'''+ str(covLR) + '''.txt >>''' +  benchDir +  '''/all_recalls_softs.txt'''
			#~ subprocess.check_output(['bash','-c', cmdAwk])
			#~ cmdAwk = '''awk '{print $0,''' + str(covLR) + '''}' ''' + benchDir + '''/correct_base_rate_cov_'''+ str(covLR)  + '''.txt >>''' +  benchDir +  '''/all_correctRates_softs.txt'''
			#~ subprocess.check_output(['bash','-c', cmdAwk])

			
			#~ cmdAwk = '''awk '{if($1=="msa_exon") {print$2, "recall",''' + str(covLR) +  '''}}' ''' + benchDir + '''/recall_cov_'''+ str(covLR) +'''.txt >>''' +  benchDir +  '''/all_recall_precision.txt'''
			#~ subprocess.check_output(['bash','-c', cmdAwk])
			#~ cmdAwk = '''awk '{if($1=="msa_exon") {print$2, "precision",''' + str(covLR) + '''}}' ''' + benchDir + '''/precision_cov_'''+ str(covLR) +  '''.txt >>''' + benchDir +  '''/all_recall_precision.txt'''
			#~ subprocess.check_output(['bash','-c', cmdAwk])
			#~ cmdAwk = '''awk '{if($1=="msa_exon") {print$2, "correct",''' + str(covLR) + '''}}' ''' +  benchDir + '''/correct_base_rate_cov_'''+ str(covLR) +'''.txt >>''' +  benchDir+  '''/all_recall_precision.txt'''
			#~ subprocess.check_output(['bash','-c', cmdAwk])
			#~ if covLR == covForFigs:
				#~ printMetrics(currentDirectory, covForFigs, benchDir)
	# printGlobalMetrics(currentDirectory)
	# printMetricErrorRates(currentDirectory, covForFigs)
	# dictLatex = {"coverage":str(covLR), "recall": currentDirectory + "/all_recall_softs.png", "precision": currentDirectory + "/all_precision_softs.png", "correctRate": currentDirectory + "/all_correctRate_softs.png", "size":  currentDirectory + "/size.png", "coverage_function": currentDirectory + "/metrics_function_coverage.png", "errorrate_function" : currentDirectory + "/metrics_function_errorrate.png", "coverageToKeep": covForFigs, "errorToKeep": errorRateToKeep, "isoform": currentDirectory + "/all_confusion_matrix.png", "isoformError": currentDirectory+"/confusion_matrix_function_error.png", "isoformCoverage": currentDirectory+"/confusion_matrix_function_coverage.png", "precisionSoft": currentDirectory+"/plot_all_precision_softs.png", "recallSoft": currentDirectory+"/plot_all_recall_softs.png"}
	# writeLatex(dictLatex, currentDirectory, errorRate, coverage, outputDirPath, outputPDFName)
