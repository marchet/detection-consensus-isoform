Isoform detection and consensus in ONT long reads using MSA
===========================================================

# Contact: 
Camille Marchet camille.marchet@irisa.fr

# Requirements:
* GCC 
* Make
* Python3

# Installation:
```
git clone git@gitlab.inria.fr:marchet/detection-consensus-isoform.git
cd detection-consensus-isoform.git
./install.sh
```

# Command line:
`
python3 analyze_MSAv2.py -r reads.fasta
```
# Test:
Launch the tool on a small FASTA file that contains 5 inclusion (on included exon in the middle) and 3 exclusion (exon spliced) reads
```
python3 analyze_MSAv2.py -r sample_example/test.fasta
```
The tool outputs as many corrected files as it detected isoforms for the gene. 
Have a look at the "results" directory.
The tool outputs a file containing 5 consensus reads  (corrected_by_MSA0.fa) that corresponds to the inclusion isoform reads detected, and 3 consensus reads for exclusion in corrected_by_MSA1.fa 
All corrected sequences are also present in file all_corrected.fa

Sequences used to produce the consensus of each exon chain (alternative region) are in exonChain*.fa files. Here there are three, two for the flanking sequences and one for the skipped exon.

msaAllReads.fa shows the multiple alignment results for the reads.
