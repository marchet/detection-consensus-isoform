from analyze_MSAv2 import *
from utils import *

def test_computePOA():
	alignments = computePOA("./test/test.fa", ".", "test")
	assert checkIfFile("./test/msaAllReads.fa")
	assert alignments == [('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]


def test_computePOA2():
	alignments = computePOA("./test/test2.fa", ".", "test")
	assert checkIfFile("./test/msaAllReads.fa")
	assert alignments == [('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa..cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt...acacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt.................................................................................................................................................................................cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt......................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc.....')), ('5', ('zz untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]

	
def test_computePOA3():
	alignments = computePOA("./test/test3.fa", ".", "test")
	assert checkIfFile("./test/msaAllReads.fa")
	assert alignments ==[('0', ('w untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttggggggggggggggggggggggggggggggggggggggggggggggggggg........................cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...................................................acacacacacacacacacacacaccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...................................................acacacacacacacacacacacaccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('z untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...........................................................................cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('zz untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...........................................................................cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]
	



def test_getMSA():
	alignments = [('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]
	msa = MSA()
	msa.getMSA(alignments)
	assert msa.rows == [('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]

	
def test_getMSA2():
	alignments = [('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa..cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt...acacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt.................................................................................................................................................................................cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt......................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc.....')), ('5', ('zz untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]


	msa = MSA()
	msa.getMSA(alignments)
	assert msa.rows  ==[('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa..cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt...acacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt.................................................................................................................................................................................cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt......................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc.....')), ('5', ('zz untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]


def test_findGapStretches():
	alignments = [('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	assert msa.gapsList == [[33, 147], [207, 270], [98, 275]]
	assert msa.readsToGapsIndex == [[], [0, 1], [0, 1], [2], [2]]
	assert msa.gapsIndexToReadsIndex == [[1, 2], [1, 2], [3, 4]]

	
def test_findGapStretches2():
	alignments =[('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa..cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt...acacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt.................................................................................................................................................................................cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt......................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc.....')), ('5', ('zz untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]

	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	assert msa.gapsList == 	[[76, 250], [76, 252], [75, 256], [253, 273]]
	assert msa.readsToGapsIndex == [[], [0], [1], [2], [0, 3], [0, 3]]
	assert msa.gapsIndexToReadsIndex == [[1, 4, 5], [2], [3], [4, 5]]


def test_prolongateNstretches():
	alignments = [('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.prolongateNstretches()
	assert msa.gapsList == [[33, 147],  [207, 270], [98, 275] ]
	assert msa.readsToGapsIndex == [[], [0, 1], [0, 1], [2], [2]]
	assert msa.gapsIndexToReadsIndex == [[1, 2], [1, 2], [3, 4]]


def test_prolongateNstretches2():
	alignments =[('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa..cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt...acacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt.................................................................................................................................................................................cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt......................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc.....')), ('5', ('zz untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.prolongateNstretches()
	#~ print(msa.gapsList)
	#~ print(msa.readsToGapsIndex)
	#~ print(msa.gapsIndexToReadsIndex)
	assert msa.gapsList == 	[[76, 250], [76, 252], [75, 256], [253, 273], [76, 273], [76, 273]]
	assert msa.readsToGapsIndex == [[], [0], [1], [2], [0, 4], [0, 5]]
	assert msa.gapsIndexToReadsIndex == [[1, 4, 5], [2], [3], [], [4], [5]]


	
def test_keepLongGapsOnly():
	alignments = [('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.prolongateNstretches()
	msa.keepLongGapsOnly()
	assert msa.gapsList == [[33, 147], [207, 270], [98, 275]]
	assert msa.readsToGapsIndex == [[], [0, 1], [0, 1], [2], [2]]
	assert msa.gapsIndexToReadsIndex == [[1, 2], [1, 2], [3, 4]]


def test_keepLongGapsOnly2():
	alignments =[('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa..cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt...acacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt.................................................................................................................................................................................cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt......................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc.....')), ('5', ('zz untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.prolongateNstretches()
	msa.keepLongGapsOnly()
	assert msa.gapsList == 	[[76, 250], [76, 252], [75, 256], [76, 273]]
	assert msa.readsToGapsIndex == [[], [0], [1], [2], [0, 3], [0,3]] # sur ?
	assert msa.gapsIndexToReadsIndex == [[1, 4, 5], [2], [3], [4, 5]]

	
def test_groupSimilarRegions():
	alignments = [('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	assert msa.groupSimilarRegions() == [[0], [1], [2]]
	

def test_groupSimilarRegions2():
	alignments =[('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa..cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt...acacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt.................................................................................................................................................................................cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt......................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc.....')), ('5', ('zz untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.prolongateNstretches()
	msa.keepLongGapsOnly()
	assert msa.groupSimilarRegions() == [[0, 1, 2], [3, 2]]

	
def test_makeDistinctGroups():
	alignments =[('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa..cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt...acacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt.................................................................................................................................................................................cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt......................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc.....')), ('5', ('zz untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.prolongateNstretches()
	msa.keepLongGapsOnly()
	msa.groupSimilarRegions()
	assert msa.makeDistinctGroups([[0, 1, 2], [3, 2]]) == [[0, 1, 2], [3]]

	


	
def test_findConsensusPositionsForGaps():
	alignments = [('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.groupSimilarRegions()
	msa.findConsensusPositionsForGaps([[0], [1], [2]])
	assert msa.gapsList == [[33, 147], [207, 270], [98, 275]]
	assert msa.readsToGapsIndex == [[], [0, 1], [0, 1], [2], [2]]
	assert msa.gapsIndexToReadsIndex == [[1, 2], [1, 2], [3, 4]]


def test_findConsensusPositionsForGaps2():

	alignments =[('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa..cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt...acacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt.................................................................................................................................................................................cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt......................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc.....')), ('5', ('zz untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.prolongateNstretches()
	msa.keepLongGapsOnly()
	msa.groupSimilarRegions()
	msa.findConsensusPositionsForGaps([[0, 1, 2], [3]])
	assert msa.gapsList == [[76, 250], [76, 273]]
	
def test_findConsensusPositionsForGaps3():

	alignments = [('0', ('w untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttggggggggggggggggggggggggggggggggggggggggggggggggggg........................cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...................................................acacacacacacacacacacacaccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...................................................acacacacacacacacacacacaccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('z untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...........................................................................cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('zz untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...........................................................................cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.prolongateNstretches()
	msa.keepLongGapsOnly()
	regionsForConsensus = msa.groupSimilarRegions()
	regionsForConsensus = msa.makeDistinctGroups(regionsForConsensus)
	assert msa.gapsList == [[128, 151], [77, 127], [77, 151]]
	assert msa.readsToGapsIndex == [[0], [1], [1], [2], [2]]
	assert msa.gapsIndexToReadsIndex == [[0], [1, 2], [3, 4]]


def test_consensusStartEndGapRegion():
	alignments = [('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.groupSimilarRegions()
	msa.findConsensusPositionsForGaps([[0], [1], [2]])
	msa.consensusStartEndGapRegion()
	assert msa.gapsList == [[33, 147], [207, 272], [98, 272]]



def test_consensusStartEndGapRegion2():
	alignments =[('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa..cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt...acacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt.................................................................................................................................................................................cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt......................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc.....')), ('5', ('zz untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.prolongateNstretches()
	msa.keepLongGapsOnly()
	msa.groupSimilarRegions()
	msa.findConsensusPositionsForGaps([[0, 1, 2], [3]])
	msa.consensusStartEndGapRegion()
	assert msa.gapsList == [[76, 250], [76, 273]]
	#~ print(msa.readsToGapsIndex)
	#~ print(msa.gapsIndexToReadsIndex)

def test_simplifyIncludedGaps():
	alignments =[('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa..cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt...acacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt.................................................................................................................................................................................cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt......................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc.....')), ('5', ('zz untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.prolongateNstretches()
	msa.keepLongGapsOnly()
	msa.groupSimilarRegions()
	msa.findConsensusPositionsForGaps([[0, 1, 2], [3]])
	msa.consensusStartEndGapRegion()
	msa.simplifyIncludedGaps([[0, 1]], [])
	assert msa.readsToGapsIndex == [[], [0], [0], [0], [1], [1]]
	assert msa.gapsIndexToReadsIndex == [[1, 2, 3], [4, 5]]


	
def test_sortPosition():
	alignments = [('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.groupSimilarRegions()
	msa.findConsensusPositionsForGaps([[0], [1], [2]])
	msa.consensusStartEndGapRegion()
	assert msa.sortPosition() == [33, 98, 147, 207, 272]

def test_sortPosition2():
	alignments =[('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa..cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt...acacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt.................................................................................................................................................................................cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt......................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc.....')), ('5', ('zz untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.prolongateNstretches()
	msa.keepLongGapsOnly()
	msa.groupSimilarRegions()
	msa.findConsensusPositionsForGaps([[0, 1, 2], [3]])
	msa.consensusStartEndGapRegion()
	msa.simplifyIncludedGaps([[0, 1]], [])
	assert msa.sortPosition() == [76, 250, 273]
	
def test_allRegions():
	alignments = [('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.groupSimilarRegions()
	msa.findConsensusPositionsForGaps([[0], [1], [2]])
	msa.consensusStartEndGapRegion()
	assert msa.allRegions([33, 98, 147, 207, 272]) ==  [[0, 32], [33, 97], [98, 146], [147, 206], [207, 271], [272, 363]]


def test_allRegions2():
	alignments =[('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa..cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt...acacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt.................................................................................................................................................................................cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt......................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc.....')), ('5', ('zz untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.prolongateNstretches()
	msa.keepLongGapsOnly()
	msa.groupSimilarRegions()
	msa.findConsensusPositionsForGaps([[0, 1, 2], [3]])
	msa.consensusStartEndGapRegion()
	msa.simplifyIncludedGaps([[0, 1]], [])
	assert msa.allRegions([76, 250, 273]) == [[0,75], [76,249], [250,272], [273,344]]

def test_regionsInReads():
	alignments = [('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.groupSimilarRegions()
	msa.findConsensusPositionsForGaps([[0], [1], [2]])
	msa.consensusStartEndGapRegion()
	msa.regionsInReads([[0, 32], [33, 97], [98, 146], [147, 206], [207, 271], [272, 363]])
	assert msa.regionsList == [[0, 32], [33, 97], [98, 146], [147, 206], [207, 271], [272, 363]]
	assert msa.readsToRegionsGaps == [[], [1, 2, 4], [1, 2, 4], [2, 3, 4], [2, 3, 4]]
	assert msa.regionsIndexToReadsIndex == [[], [1, 2], [1, 2, 3, 4], [3, 4], [1, 2, 3, 4], []]


def test_regionsInReads2():
	alignments =[('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa..cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt...acacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt.................................................................................................................................................................................cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt......................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc.....')), ('5', ('zz untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...............................................................................................................................................................................tt.....................ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.prolongateNstretches()
	msa.keepLongGapsOnly()
	msa.groupSimilarRegions()
	msa.findConsensusPositionsForGaps([[0, 1, 2], [3]])
	msa.consensusStartEndGapRegion()
	msa.simplifyIncludedGaps([[0, 1]], [])
	msa.regionsInReads([[0,75], [76,249], [250,272], [273,344]])
	assert msa.regionsList == [[0,75], [76,249], [250,272], [273,344]]
	assert msa.readsToRegionsGaps == [[], [1], [1], [1], [1, 2], [1, 2]]
	assert msa.regionsIndexToReadsIndex == [[], [1, 2, 3, 4, 5], [4, 5], []]


def test_regionsInReads3():
	alignments = [('0', ('w untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttggggggggggggggggggggggggggggggggggggggggggggggggggg........................cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...................................................acacacacacacacacacacacaccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...................................................acacacacacacacacacacacaccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('z untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...........................................................................cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('zz untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt...........................................................................cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.prolongateNstretches()
	msa.keepLongGapsOnly()
	regionsForConsensus = msa.groupSimilarRegions()
	regionsForConsensus = msa.makeDistinctGroups(regionsForConsensus)
	msa.findConsensusPositionsForGaps(regionsForConsensus)
	start, end = msa.consensusStartEndGapRegion()
	#~ print(start,end)
	msa.simplifyIncludedGaps(start, end)
	listPositions = msa.sortPosition()
	listRegions = msa.allRegions(listPositions)
	msa.regionsInReads(listRegions)
	assert msa.regionsList == [[0, 76], [77, 126], [127, 150], [151, 221]]
	assert msa.readsToRegionsGaps == [[2], [1], [1], [1, 2], [1, 2]]
	assert msa.regionsIndexToReadsIndex == [[], [1, 2, 3, 4], [0, 3, 4], []]
	
def test_regionsInReads4():
	alignments = [('0', ('w untitled', 'ttttttttttttttttttttttt.......tttttttttttttttttttttttttttttttttttttttttttttttttttttgggggggggggggggggggtgggggggggggggggggggggggggggggggggccccccccccccccctcccccccccccccc.cccccccgcccccccccccccccccccccccccccccccccc')), ('1', ('ww untitled', 'ttttttttttttttttttttttt.......ttttttttttttttttttttttttttatttttttttttttttttttttttttttgggcgggggggagggggggggggggggggggggggggggggggggggggc....cccccccccccccccccccccccccccc.ccccccccccccccccccccccccccccccccccccccccca')), ('2', ('x untitled', 'ttttttttttttttttttttttt.......ttctttttttttttttttttttttttgtttttttttttttttttttttttttt.......................................................cccccccccccccccccccccccccccc.aacccccccccccccccccccccccccccccccccccccccc')), ('3', ('xx untitled', 'ttttttttttttttttttttttt.......tttttttttttttttttttttttttttttttttttttttttttttttttttcc.......................................................cccccccccccccccccccccccc....acccccccccccccctccccccccccccccgcccccccccccc')), ('4', ('xxx untitled', 'tttttttttttatttttttttttaattttttttttttttttttttttttttttttttttttttttttttttttttttttttcc.......................................................cccccccccccccccccccccccccccc.cccccccccccccccccccccccccccccccccccccccccc')), ('5', ('www untitled', 'ttttttttttttttttttttttt.......ttttttttttttttttttttttttttttttttttttttttttttttttttttttggggggggggggggggggtggggggggggggggggggggggggggggggg....cccccccccccccccccccccccccccc.cccccccccccccccccccccccccccccccccccccccccc'))]
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.prolongateNstretches()
	msa.keepLongGapsOnly()
	regionsForConsensus = msa.groupSimilarRegions()
	regionsForConsensus = msa.makeDistinctGroups(regionsForConsensus)
	msa.findConsensusPositionsForGaps(regionsForConsensus)
	start, end = msa.consensusStartEndGapRegion()
	msa.simplifyIncludedGaps(start, end)
	listPositions = msa.sortPosition()
	listRegions = msa.allRegions(listPositions)
	msa.regionsInReads(listRegions)
	assert msa.regionsList == [[0, 82], [83, 136], [137, 208]]
	assert msa.readsToRegionsGaps == [[], [], [1], [1], [1], []]
	assert msa.regionsIndexToReadsIndex == [[], [2, 3, 4], []]
	
def test_regionsInReads5():
	alignments = computePOA("./test/simulated.fa", ".", "test")
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.prolongateNstretches()
	msa.keepLongGapsOnly()
	regionsForConsensus = msa.groupSimilarRegions()
	regionsForConsensus = msa.makeDistinctGroups(regionsForConsensus)
	msa.findConsensusPositionsForGaps(regionsForConsensus)
	start, end = msa.consensusStartEndGapRegion()
	msa.simplifyIncludedGaps(start, end)
	listPositions = msa.sortPosition()
	listRegions = msa.allRegions(listPositions)
	msa.regionsInReads(listRegions)
	assert msa.gapsList == [[914, 965]]
	assert msa.readsToGapsIndex == [[], [], [], [], [], [], [], [], [], [0], [0], [0], [0]]
	assert msa.gapsIndexToReadsIndex ==  [[9, 10, 11, 12]]
	assert msa.regionsList == [[0, 913], [914, 964], [965, 1977]]
	assert msa.readsToRegionsGaps ==[[], [], [], [], [], [], [], [], [], [1], [1], [1], [1]]
	assert msa.regionsIndexToReadsIndex ==[[], [9, 10, 11, 12], []]

	
def test_regionsInReads7():
	alignments = computePOA("./test/simulatedLR_size_100_abund_50.fa", ".", "test")
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.prolongateNstretches()
	msa.keepLongGapsOnly()
	regionsForConsensus = msa.groupSimilarRegions()
	regionsForConsensus = msa.makeDistinctGroups(regionsForConsensus)
	msa.findConsensusPositionsForGaps(regionsForConsensus)
	start, end = msa.consensusStartEndGapRegion()
	msa.simplifyIncludedGaps(start, end)
	listPositions = msa.sortPosition()
	listRegions = msa.allRegions(listPositions)
	#~ msa.regionsInReads(listRegions)
	#~ print(msa.gapsList)
	#~ print(msa.readsToGapsIndex)
	#~ print(msa.gapsIndexToReadsIndex)
	#~ assert msa.gapsList == [[914, 965]]
	#~ assert msa.readsToGapsIndex == [[], [], [], [], [], [], [], [], [], [0], [0], [0], [0]]
	#~ assert msa.gapsIndexToReadsIndex ==  [[9, 10, 11, 12]]
	#~ assert msa.regionsList == [[0, 913], [914, 964], [965, 1977]]
	#~ assert msa.readsToRegionsGaps ==[[], [], [], [], [], [], [], [], [], [1], [1], [1], [1]]
	#~ assert msa.regionsIndexToReadsIndex ==[[], [9, 10, 11, 12], []]

	
def test_sample():
	alignments = [('0', ('inclusion_0 untitled', 'ac...ttca.gcgccgtcg.gtctcctggacgccgggcgtga.cacg.atct.accggaac.actctctc.c...cacgctattaatgcctttt.tatatatatct.cgtgaacaacaaagcccacaacta.tt....aaagtggaatagtcccggtttaa.gctggtgaggatcaatcaggaggtacgtg.tgt..tcg.gtgacaggtgga.tacaggtgccgt.cttaacgttgc...ca.tatc..gggt..tt.cat.tgtatctgaat.aa..tcacgaaa.gttcatcgtgctct.ttttgtttgta.gaag.tgatcacat..ta.ggccctaagcacatca.gttatactaggtctct.gaggaggc.gccggtcctaaaaaggt...catag.agaggtc.aagc..aag.tcgt.ggtcgccc.tcaagggtctgt.gttg.ggtagataaagcgt.gccgctaagttctca.tgcttttgttgcaaatgc.c.tagttggacg.att.t.a..tcgg.cgtctaaccgtgaacg.ct.g.ct.attgtagc.cc.ta.cacgc.agc..caagaga.ttag.tcatgctagatgc.ttacaggcgga.caa.ct.atct.cacaa.ggtgcccgt...cagtgaagggt.gtgtct.actaacgcgttcacggaccc.ttcg.cgcataaatcaatctgaa.gtgattcacctgaaaactcc.agtccaa..ttacag.tcg.gt.taaaa.ctcgggc.tggttctttc.cgcgc.gcagg.tctcacgctctttgggctaagc.tgaa.gggcactgtc..ctg..t.tctaccatc.t..gtagtgccactt.cc.acg..a.ttcggg.agacctgc.cgac.attctctccgcaattggcacgagcttaaggta.ttc.g.aacagatt..gcgc..gaactcggtgggtt..aa.acag...gt.gggcaaaataatcgggatagaagaaggacg.tagcctcccc..gctcttgtggtatgaaaa.aa.agaccgcgttaaagatttgctatg..gagcaggttatacctacttcggcatacg...aggggaaatc.aatgttattgaaagcggcaataa.cgttttatacgttcagtagtgaacccccgcgg..cgtgcctaagcaacatattatagagtg.acgctccgggcg...ta.tacc.ctg.gg.tctctccatga.tgggtcctcca.gagt..gcacct.agtccc.a.tgtg..tttccgtactataga.gcggcttggactttgcgtcaacgacttc.ttttct...g.attgcgaactt.cgtcgat..tccg.ctcg.tg.ct.ggcttgcaacaaagaacggc.ggctcg.gggatcgggacagtactcctc.gtaccaa.agcct..c.gcgtcagcc.cggttacgaga.tg..gctaaatg.aa.ggaag.cgatcttggacgga.aacaagata.taaaacgcttcagagga.ggttgct.tt.cac.atgat.tg.gcagaaaaactg.tgaacgtatgctcttaga.tc.g.tctaa.tgagct.g.tggtctgttgtcatgtaatgttagg.ctc.ccgaa.gggca.ctaacaccttcgtt.t..caagtt...ggcaatccgcg.ctaagggc..g.cccgctc.actggactctattggg.aaccctggctagtctg.cc..gcg.c.gcggtaacgcct..tacaccg..cta..c.taa.aaacaaaaaaaaaaagaaaaaaaaaaaaaaaaaaaa..taaaaaaaaa')), ('1', ('inclusion_1 untitled', 'accaattca.gcgccgtcgtgtctcctggacgcctggcgtga.ca..gatcagacg..aac.a.tctctg.c...cacg..attg.tgccttat.tatata.atct.ca.gaacaacaaagcccaca.ctacttc..taaag..gaataat.c.ggtttaa.gctggtgaggagca.tcagccggtacgt..tgtgctcg.gt.acgggg.ga.tacagatgccgttcttaacgttgc...cg.tatccggggaggtt.tctgtgt...tga.t.ca..tcactgaa.gttc.tagtgctctgttttttttata.acaggtga.cacatgtta.ggac.taagtacataa.ggtatgctag.tcttttgaagaagccagtg...ctaaaaaggta..caaag.agaaag...atc.caac.tcggcggtcgccccgcaagg.tctgtagttg.gctagataaagcgg.cccgctcagttctccgtgcttt.gttgca..tgctc.tagttggact.att.t.ag.tcag.cgtcta..cgagaacg.ct.g..tcattgtagc.cccta.g.ctc.cgggacaagagactgttg....gctagatg.gttaaaggcg.a.gaa.ct.aact.cacaa.tatgccccc...tggtgaaaggttgcgtct.actg.cgcgttcagggaccccttcg.c..ataaatcaatctgaa.gggattcacc.gtaaacttctagtccaagatcacaggtctaga.taaaa.ctcgggcctggtcttttctggcgccgccgg.ttcc.cgcactttgggctaagcgtgaaagggcactata..ctgc.t.tcc.cta.c.t..gtagtgcc.ttt.cccacgg.a.tccggg.gg.cctgatcgacgattctccccgcaatgg.caagag.ttaatgaa.tacagcaacagata..acgct.gcattcggtgggtt..aa..caacatgtagggcaaataaatcgg.acagaagacggacaaaagc.tctac..gct..tgtggtagttaaa.aatagaccg.g..a..gatttgctatgccgaggaggttattgc..ctt.gacatacgggaaggggaactc.aatgt.at.gaa.gcagccaaaa.cgttt.agaccttcagtagcgaaaccccgcgggacgtagcaaag.aacatatta.agactg.acgcaccgggcaca.ta.tacatctg.ag.tatctccatgaatgg.tcctcaa.ga.ta.gcgcccgcgtgccca.t.tgcatg.cagtactatagacgtgtc.tgg..agtga.tcaacgacttc.ttttcg...a.tttccaa...tgc.tcgatggtctt.ctcgcta.t.tggcgtgcggtaaa..acgg..ggctcg..ggatcgggacaatactaccc.gtacccagagcgtatt.gg.tcagcc.cggtcacgaga..g..actaaatg.a..gcaag.cgatcttgg.cg.ccaacaagat..taaaacgcttaaaagaatggttgctgtt.ctc..tgat.tg.gcagaaaaaatg.t.aacgtatgctcttagaatcag.cctac.tta.cc.g.tgctctggtgtcatgtaatgttag.actc.ccgtatgggca.c.aacaccttctta.ta.caagta...ggcatcc.gcgtctaagcgc.cgatccgcgc.actggactctatgggggaaccctggct.gtct...ctagct.c.gcggtaacgcctggtacaagtttctaggcataa.aaataaaaaaaaaaaaaaaaaaaaaaagaaaaaaga..aaaaaaaata')), ('2', ('inclusion_2 untitled', 'acc..t.ca.gcgccggcgt.tcacctggacgccgggc.tga.cg..gatcagaccggaacg.ctctctcgc...cgccctattaatgc.tttt.tatatatatcg.c.tgaaca.caaagcccacagctacttc..taaag.ggaatact.ccggttttacgctggtgag.agcc.tcagccggtacgt..tatggtcg.gtgacgggtggactacagatgacgc.cttaacgttgcaatca.tatcagggat..t..catgtg.atctgaat.aaactcatgaaa.gtt..tagtgtactgttttttttataagaaggtgatcat...tta.ggaccta.gtacatac.gtt.tgctaggtttcttgacgaagccgccggtcctaaaaaggta..tatagtagaaggc.catctcaag.tcgt.ggtcgcccctcaaggatgtt..gttg.gt.agataaagcgttgccgc.aa.t.ctcg.tgcttttgttgcaaaggc.c.tagt..g.ct.att.t.a..t.ag.cgtctaaccgtgaacgggttg.ctgattgtagc.cccta.ctcgccagc..catgaga.t.agggc..gctggatgc.tt.caggcggaagaa.ct.aact.cacaa.tatgcgcccgctcggtg....gtt.cgtctgacttacgc.ttc..ggacccctac..cgcata..tcaatct.aa.gtgtcacacctgtaaa.tcct.gtgaat..tcacaggtca.ga.taaaaactcggcc.tgg.ctttcc.ggcgccgacgg.gc..acgcactttggg.ta.gcgtgaaagggcactatc..ctgc.gcaccact..c.t..gtagtgccacct.gc..cgg.c.tt.gggtgggtccgatcgacgattctctcagcaa..g.cacg.gcttaag.aa.tac.gcaac.g.ttgtacgct.gaactcggtgcga...aa.acag...gagaggcaa.taaatcgg.acagaagaaggacaatagcctccgcccgctcttgtggtagttaaagaatataacg.g..t..gatttgctatgccgaggaa.ttatagctccttcggcatacgggaaggggaactccaatgt..gt.acatcagccaataacgtctta.acgttcagtagtgaaaccccccgg.acg...ctaagcaacatattatag.gta.acgctccgggcg...ta.ta.atctgcgg.tatctccatgaatgggtcc.caa.gattatgc.cccgagtcccccgt.tggatg.cagtactatag...tggcgtgga.tttgggtc...gactcccttttcgcgga.tttg.gaacctgcgtcgatggtctg.ctcgctgtct.ggcttgcgataaagaacat..ggctcgcgt.atcgggaca.tac.cccg.ag.actagagcgtatc.gtgtcagcc..ggttaa.agg.tg...ctaaatggaa.gcaag.cgatcttggacgga.aacacgatagtaaaacgct.aagataatagttgctgtt.ctc..tgatgta.gcagaaaaaatg.tg...gtatcgtcttaggatc.g.ta.aa.ttagct.g.tg.tcgggtgtcatgtaatgt.agga.tcaacgtatgggcag..aa..ccttcgttatatcaagtt...ggcaatcc.cgtctaagcac.cgacccgctctactggactcta..ggg.aacc.tggtt..acta.cc..gcg.c..cggca.cgcct...acacgt..ctag.cataa.a..caaaagaaaaaaaaaaaaaaagaaaaaaaaaaa..aaaataaa..')), ('3', ('inclusion_3 untitled', 'acc..t.ca.ccgccgtcgtgtc.cctggacgccgggcgtg..cacggatcagaactgaaccacgctctcgc...cacgatattaatgcctctt.tatatatatct.cgtg..caaccaa.cccacatctacctc..taaagaggaatact.c.g.tttaa.actggt...tagca.a.tcccggtaagtagtgt..tccagtgaaggttgga...gagatgccgc.ct.aa.g...c...caatatctggggag.t..cccgt.tatctgaat.at..tcactgaaagttaattgtgctctgtattttt.ata.gaaggtta.taaag...a.ggacctaagttcaataagttat.ctaggcctcttgag.aagccggcg.tcctaaaacgcta..catag.agaaggctaatctcacg.tcgtcggtcgcctct.aagggtctt..gttg.gggaga.aaag.g...ct.ctcagttatcagtccttttgttgcaaatgc...tatttggaca.att.t.ag.tcag.cgcc.aaccgagaagg.ct.g.ct.attgt.gctcccta.ctcg..ag....aagag..ttaggta..gcga.atgcgt.acaggcgga..aa..t.aact.cacaa.tatgcgcccgctag.tga.ggt..gcgtct.actaacgcgttcagggagcccttcg.cgcata..tca..ct.aa.ctgattcacctgtaaactcctagtccaa..tcacaggtca.ga.aaaat.ctcgggc..gc.ctgttcaggcgccgccgggtctcacgc.ctttggt..aagcgtgtaagggcactatc..ctgcctctc.actc.c.t..gta.tgacactttcccacgg.atttcgggttgacctgatc.acga..ctct..gga.ttggcacgagcttaatgg..tac.gcaacagattgtacgttagaactcgg.gggtt..aa.actg...gtgaggcaaatagatcggtacaga.gaaggaccatagcctcgcc..gct.ttatggtagt.aaa.aatagaccg.g.....gttttgctatgccgag.aggt.a.agctc.ttcggcataggg.aagggga.ctc.a.tgt.at.gaaagcagccaata.cgtttta..cgttcagtagtgaacccaccccg.acgtagctaagcaa.atattatagagtg.acg.tcctgg.g...taatagatctg.ggctatctccatgaatgggtcctcaa.gttt..gcgccc.agtccccc.t.tggatg...gaactagtg...tggcc.gg..tttgcgtcaacgactctctttt.g...g.atttcgaacgg.cgtcgag...ctg.ctcgctg.ct.ggcgt.cgatataaaacgt..ggctcg.gggatcggg.cagtactaccccgtacccagag.gtatc.gcgtcaacc..ggttacgagaatg..gctaatgg.aa.gcaag.cg...tttgacgga.aacaagatagtaaaacgct.a..a.aattgttgctgtt.ctcgatgat.cgagcagcaaa.ctg.taaacgt.tgct.taggaat..g.tctacgttaacc.c.tggtctggtggcatgtaatgttgg.actcaccgtt.ggtca.ctaa..cattcgtta.atcaagttatcggcaatccgcgtctaacggc.cgacccgctc.actagactctattggggacccctggctagtcta.cc..ccggc.gcggt..ctcctggcacaac.ttctagga.tacgaaaaaaaaaaaaaaagataaaaaaagaaaaacagaa..aaaaaaaaaa')), ('4', ('inclusion_4 untitled', 'acc..ttc..gttc.gtcgtgt.tcctgcacgccgggcgtga.cacggatca.acgggaacgactctctcac...cacgctatta.tgccttctatatata...cc.cgtgaacaacaaagcctacatctacttcactga.g..gaatactgc.ggtttca.g.tggtgaggagca.a.acccggtacgta.tgt..tcg.gtgacgcgtaga..acacgtgccgctctta.cgttgc...a...atcaggagaggtt.catgtgtatctgaat.aa..tcactgaa.gttcatagtgct.tgttttttttata.gaaggaga.tac...tta.ggacctaag.acaata.gtta..ctaggtctcttgatgaagccgccgaaccgaaaaag.ta..catag.agaaggc.gatc.taag.tcgtcg.tcgcccctcaagg.tgttt.gttgcgtta.ataaggtg...ccgctcagt.ctcagtgcttttgatgcaaatgc.c.tggttggactcact.a.ag.cc...cgtctaaccgagaacggct.gactcattgtagc.cccta.c.cgc.ctg.acaagaga.tgaggtc..gctagatgcgttaa.ggcgga.gaa.ct.a.c..cacaa.tatgcccccgcttggtgaa..gt.gcgtct.actaacgtgatcagggaccccttcg.cgcataaatcaatc.gca.g...ttcacgtgtaa.ctcctagtccaa..tca..ggtctaga.taaac.ctcgggcctggtgttttc.ggcgccgcggg.tctc.agcacttgcggctaagcgtgaaagggcacccttgcctgc.tctccacca.c.tcggta.tgcccttt.cccacgggagttcgggtggacctggtcgacgattctctccacacttg.cc.gag.ttaatgaa.tac.gcaacagattttacgct...aa.cggtgggtt..aa..cag...gtgaggcaaataaatcgc.acagaagaaggacaatagcctccgcc.gctcttgtggtaggtaaa.c.ta...cg.gtta..gatt.gctatgccgagcaggttatagctccttcggcatacggggatgggaactc.aacct..t.gaaagcagccaaaaacgttt.agacgt.cagtagtgaaaccccccgg.acgtagctaagcaacat.ttatatagtg.acgctc..ggcg...ta.aacatctg.tg.tatctacat.aatgggtcctcaa.tt.t..gcgcccgagtcccca.t.tg..tg.cagtactataga.gtggcctgg..tttgcgtcaacgacttcgttttcg...g.attaaaagtttgcgtcgatggtcctgctcgctg.c..ggcgtgcgataaagaacgt..ggctcgcgggatcggggcagtactaccc.cg.cccagagagtatc.gcatcagc....gt.acgagg.tg..gctaaagg.g.tgcaaggcgaacctggacg.a.aacataat...aaaacgcttaagagaatggttgctgt..cac.atgat.ta.gcagaacaaatgctgaacgtatgcacttagaatc.g.tctag.ttagcctg.tg.tctggt.tcatgta.t.tcaggactcaccgtatgggcagc.aag.ccttcgtt.tatcaagtt...ggcaatc.g..tctaagagc.cgacccgctc.actgaa.tctatgggggaaccctggct..tct...c..gcg.c.gcggtaacgc.tggtgcaacgttctaggcataa.aaaaataaaaaaaaaaaaaaaaaaaaaaaaaaaaaa..aaaaaaaaca')), ('5', ('exclusion_0 untitled', 'acc..ttcaagcgtcgtcgtgtctcctgga.gccgg.cgtgaaca..gaacagaccg.aac.acactcacgc...c.cgctattaatgcctttt.tata.ata..c.cgtgaacgacaaagcccacatctg.ttc..taaagaggaatact.ccggtttta.gggggtgag..gca.tcaaccg.tcagt...gt..tcg..tgacggag.ga.tacg.a.gccgt.ctta.cgttgccat.g.tatc..gggg..at.catgtg.atccgaattaaa.tcactgtaagttcatagtgctctgttttttttataaaaaggtgattacct.tta.ggaccta...acaataagttttgctaggtcccttgaggaagccgccggtcctaaaaaggta..cgtag.agaaggc.actc..aag.tcgtcggtcgcccctcaagggtgtgt.gtcg.tgtagataatg.gt.gccgc.a.gttctcagtgcttttgttgcaaatgt.catagttggact.att.t.ac.tcagtcgactaaccgagaacg..t.a.ctcagtgtatc.ccc.a.ctctccag....c..agactg.g.tc...ctagatgcgttacaggcgga.ga..ct.aact.cacaaataa.cccccgctcggtgaagctt.gcgtctcac.g.cct.ttcagggactccttcg.c..ataaatcaatcagaa.gtgactcacc.gtaaactacta.tacta..tctcacgtca.ga.taaaaactcgccc..ggtgtttcc.ggcggcgcgg...tac.cgcactttg.t..a..cgtgaaagggcact..c..ctac.tctc.accatc.t............tg.g..........t.ag.......tgc.c.ac.....t.tcg............agcttactgaa.tac.gcaacag.ttgtacgcg.taactcggtgggtt..aagacag...ctgaggcaaataaatcgg.agagaagaaggacaatagc..ccgc..gctcttgtggtagtttaa.aataga....gtta..gatttgctat.cc.aggaggttctagctc...cggcatacg...agggaa..ta.aatgt.at.gaaagcagccaataacgtttta..cgtt.agtagtgaacccca...g.acgtagcctggcaacatata.tagagta..cgctcctggcg..gtaatacatctg.gt.tatc.ccatgaacgggtcct.aa.gattc.gcgcag.agtcccc..t.tggatg.cagtactatag....gcc.tgga.tttgc.tcaacgacttc.ttt.cg...a.tttc.gaacttgcgtcgatggt..c.c..gctg.ct.ggcgtgcgataaagaacggt.ggc..g..ggatcgggacagtactaccccgtaccccgagc.tatc.gcgtcagcc..tgttacgagagtg..gctaaatggaa.gcaag.cgatcttggaccga.aacaagatagtaaaac..gtaagagaatg.ttgctgtt.ctcgatgat.tgagcagaaaa..tg.tgaactta.gctc.aggaatc.g.tctag.ttagcctgatggtgtcgtgtcat.taatgttaggactcaccgca.ggg.a.c.cag.ccttcgatataacagggt...ggcaaac.gcgt.taagcgc.cgagcc.cgc.cctgg.ctctatgggtgaaccctggctagacta.c...gcg.c.gca.taacgcc.ggtacaatgatcgcg.cataa.a....aaaaaaaaaaacaaaaaaaaaaaaaaaaaaa.gaaaaaaaa..')), ('6', ('exclusion_1 untitled', 'ac...ttca.gtgccgtc.t.tctc.tggacgccaggcgtga.cg..gat.agaccg.aac.actctctc.c...caaactattaatgcctttt.tata.atatgt.cgtgaacaacaaagcccacatc.acttc..taaagaggaatactcccg.tgtaa.gctggtg.gtagcaatcagccgctacgtaatgt..tcg.gt.acgggtg.a.tacagatgcccctcttaa.gttgc...ca.tatcaggggaggtttcatgtgtatctga.t.aaa.tcactgaa..ttcctagtgctcg.ctttttttataggaacgtgaacat...tta.tga..tcagtaca.aa.gtta..ttaggtctcttgaggaagccgccggtcc..aaaaagtaaccataa.agaaggc.aacctcaagctcgtcggtcacccctcaagggtgtgt.gttg..tt.gataaagcgt.gccgcta.gttctcagtgctttt.ttgcaaatgc.c.tagttg..ct.attgtca..tc...cctctaaccgagaaccg.t.g.ctc.t.gtagc.cccta.ctcgccagc..ca.gaga.taaggtc..gctg..tgcgttacag.cgga.gaatct.a.ct.cacaaatatgcccc.gctcggtga.ggtt.g.gtc..actaa.gcgtt.agggaccccttcg.tgcctaaatcaatttgaatg...ttc.cctgtaa.ctcctagtccaa..tcataggtcta...taaaa.ctcggcc.tgg.gttttccggcgccgcggg.tctcaggcactttgg.ctaa.cgagaaagggcactatc..ctgc.tctc...t..cgt............ct.g..........t.ag.......tgc.a.......ctccga..............ct.aatgaaatac.gcaaca.....tacgcg.gaactcg.tgggc.taaa.acag...gttaggcaa.tat..cgg.acagaaga.ggaca.tagcctccgcc.gctcgtgtggt...taaa.aatagaccg.gtta..gatt..ctatggcgaggaggt.atggctccttctgcatagggcaagggga.ct...atga.aatgaaaacc....aaa.cgttttagacgttcagtcgtgaatcaccc.gt.acgagcctaagcaacatgttatagagtgtacgctc.gggcg...taata.atc...gg.tatctc.a.caatgggtcctcaa.gatt..gggcccgtgtccccc.t.tgcatg.cagtac.agaga.atggcct....tttgcgtcaacgacttc.ttt.cg...g.atttcgagtctgcgtcgatggt..c.ctcgctg.ct.ggcgtgcg.taaaga.cgt..g.ctcg.gggatcgaga.agtactaccc.gtaccag.agcgtatccgg.tcagcc..ggttacgaga..gttgc.aaatggaa.gcaac.cgaccctgc.cgga.aacaagatagtaaaacggttaagagaa.g..tgcagtttctc..tgat.tgagcagaaaaaa.g....acgtct.ctcttagaatc.gctctag.ttagcctg.tggtctggtgtcatgta.tgttaggactcaccgtatgg.ca.ctagcaccttcgta.tatcaagtt...agcactccgcgtcttagcgcgcgacccgctc.actgg.ctctatgggggaacccggg...gtcta.cc..gcg.c.gcgggaacgcctg.tacaacgtt.taggcatca.aaaaaaaaaaaaacaaaaaaaacaaaaaaaaaaaaacgaaaaaaaaaa')), ('7', ('exclusion_2 untitled', 'acc..ttc..gc.c.gtcgtgcctgctggactc.gggcgtga.cacc.atcgcgccggaac.a.tctctcgcgtccgcg..ctta.tgcatttt.t.tatatctcttcgtgaacaacaaa.cccacatctacttc..taaagaggaatactcccg.tataa.gctggtgaggagcaatcagccggtacgta..gt..tct.gtgacgggtgga...cggatgc.gctcttaacgt.gc...cg.tat.aggggaggtt.tct..gta.ctaaat.aaa.tcactaaa.gttcatagtgctctgttttttt...aagaagg.gt.tacat..caaggac.taagtacaataagttatgctaggtctctt.aggaagccgc.gctcc..aaaaggta..catag.agagg.c.aa.ctcaag.tcgtcggtggcccctcaaaggtgtgt.gttg.gt.agataaagcat.gcc.ctcagtt.tcagtgcttttgttgcaaatgc.c.tagttg.act.att.t.aggtcag.cgtctaacggag...g.ct.g.ttc.ttg.agc.ccgtatctcgccagc..caagaga.ttacg....cctagat...ttacaggcgga.ga..ctgaactacaca..tatgcccc.gctcggtgatggtt.gcgt.t.actaa.gcgatcagg.accc.ttcgacgcataaagcaatctgaa.gtgattctcctgtaaactcctagtccaa..tcacagg.ca.gagtaaaaactcgggcctggtgtttcc.ggcgc.gcggggtctcacgcactttggt.taagcgtgaaagggcactttc..ctgc.tctc.acca.c.t............ct.g..........t.gg.......tgc.c.......ct.tca............agcttaatgaa.t.c.gtaagagat.gtacgct.gaacgcggtggga.caaa.aaag...gtgag.caaatat.tcgg.acagaagaaagacaat.gcctccgcc..ctcttgtggtagttaaa.aatcgaccg.gtca..gatttgc.atgccgaggcggttatagcttcttcggc.t.cgggtagggaaact...at.t.at.gcaagccgccaataat.ttttagacgttca.t.gtgaaaccccccgg.ccgagcctaagcaacatataatagagtg.actctccgggcg...tc.tacatctg.gg.tatctcca.gaatggctcttcaaagatt..tc.ccg.agtcccca.t.tggatg.c.gtactataga.gtgcc.tgga.tttgcgtcaacgacttc.ttttcg...ggactacgaacttgcgtggatggt..g.ctcgcta.t..ggcg.gcgataaagaacgt.tggc.cg.gggatcgggacagtactaccc.gtacccggagcgtatc.gcgtcagcaacggctacgaga..gt.gctaaatg.aa.gcaag.cgatcttgg.cgga.aacaagatagtaaaac..t.a.ga.aatggttgctgc..ctc.atgat.tgagcagaagagatg.tgaacacatgcgctaggaatc.g.tctag.ttagcctg.tggtctgt.gtcatgtaatattacgactcac..tatgg.ca.ctagcaccttcgttatatcaagtt...ggcaatccgcttctaagggc.cgacccg.tctactagactctatgggggaacc.tggctattctatcc..gcg.cagcggtatcgcctggta.aacgttctaggcataa.a....aaaaaaaaaaaaaaaaaaaaaagaaaaaaaa..aag.......'))]

	#~ print(alignments)
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.prolongateNstretches()
	msa.keepLongGapsOnly()
	regionsForConsensus = msa.groupSimilarRegions()
	regionsForConsensus = msa.makeDistinctGroups(regionsForConsensus)
	msa.findConsensusPositionsForGaps(regionsForConsensus)
	start, end = msa.consensusStartEndGapRegion()
	msa.simplifyIncludedGaps(start, end)
	listPositions = msa.sortPosition()
	listRegions = msa.allRegions(listPositions)
	msa.regionsInReads(listRegions)
	assert msa.gapsList ==[[853, 878]]
	assert msa.readsToGapsIndex ==[[], [], [], [], [], [0], [0], [0]]
	assert msa.gapsIndexToReadsIndex == [[5, 6, 7]]
	assert msa.regionsList == [[0, 852], [853, 877], [878, 1833]]
	assert msa.readsToRegionsGaps == [[], [], [], [], [], [1], [1], [1]]
	assert msa.regionsIndexToReadsIndex == [[], [5, 6, 7], []]


	#~ assert msa.gapsList == [[914, 965]]
	#~ assert msa.readsToGapsIndex == [[], [], [], [], [], [], [], [], [], [0], [0], [0], [0]]
	#~ assert msa.gapsIndexToReadsIndex ==  [[9, 10, 11, 12]]
	#~ assert msa.regionsList == [[0, 913], [914, 964], [965, 1977]]
	#~ assert msa.readsToRegionsGaps ==[[], [], [], [], [], [], [], [], [], [1], [1], [1], [1]]
	#~ assert msa.regionsIndexToReadsIndex ==[[], [9, 10, 11, 12], []]


	






def test_consensusByExon():
	alignments = [('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.groupSimilarRegions()
	msa.findConsensusPositionsForGaps([[0], [1], [2]])
	msa.consensusStartEndGapRegion()
	msa.regionsInReads([[0, 32], [33, 97], [98, 146], [147, 206], [207, 271], [272, 363]])
	msa.consensusByExon(".", "test")
	assert checkIfFile("test/exonChainFasta0.fa")
	assert checkIfFile("test/exonChainFasta1.fa")
	assert checkIfFile("test/exonChainFasta3.fa")
	assert checkIfFile("test/exonChainFasta5.fa")

	assert msa.readToConsensus == [['ttttttttttttttttttttttttttttttttt', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'], ['ttttttttttttttttttttttttttttttttt', 'gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg', 'cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'], ['ttttttttttttttttttttttttttttttttt', 'gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg', 'cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'], ['ttttttttttttttttttttttttttttttttt', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt', 'cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'], ['ttttttttttttttttttttttttttttttttt', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt', 'cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc']]


def test_writeConsensusByGapProfile():
	alignments = [('0', ('w untitled', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.cacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('1', ('x untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('2', ('y untitled', 'ttttttttttttttttttttttttttttttttt...................................................................................................................gcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcgcg................................................................ccacacacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('3', ('ww untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc')), ('4', ('xx untitled', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt..................................................................................................................................................................................cacacacacacacacacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'))]
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.groupSimilarRegions()
	msa.findConsensusPositionsForGaps([[0], [1], [2]])
	msa.consensusStartEndGapRegion()
	msa.regionsInReads([[0, 32], [33, 97], [98, 146], [147, 206], [207, 271], [272, 363]])
	msa.consensusByExon(".", "test")
	msa.writeConsensusByGapProfile(".", "test")
	assert msa.isoforms == {(2, 3, 4): [3, 4], (): [0], (1, 2, 4): [1, 2]}
	assert checkIfFile("test/corrected_by_MSA.fa")
	assert checkIfFile("test/corrected_by_MSA0.fa")
	assert checkIfFile("test/corrected_by_MSA1.fa")
	assert checkIfFile("test/corrected_by_MSA2.fa")


def test_writeConsensusByGapProfile6():
	alignments = computePOA("./test/test6.fa", ".", "test")
	msa = MSA()
	msa.getMSA(alignments)
	msa.findGapStretches()
	msa.prolongateNstretches()
	msa.keepLongGapsOnly()
	if len(msa.gapsList) > 0:
		regionsForConsensus = msa.groupSimilarRegions()
		regionsForConsensus = msa.makeDistinctGroups(regionsForConsensus)
		msa.findConsensusPositionsForGaps(regionsForConsensus)
		start, end = msa.consensusStartEndGapRegion()
		msa.simplifyIncludedGaps(start, end)
		listPositions = msa.sortPosition()
		listRegions = msa.allRegions(listPositions)
		msa.regionsInReads(listRegions)
	else:
		msa.simpleRegion()
	msa.consensusByExon(".", "test")
	msa.writeConsensusByGapProfile(".", "test")
	assert msa.isoforms == {(): [0, 1, 2, 3]}



def runTests():
	#tests on first file
	# test_computePOA()
	test_getMSA()
	test_findGapStretches()
	test_prolongateNstretches()
	test_keepLongGapsOnly()
	test_groupSimilarRegions()
	test_findConsensusPositionsForGaps()
	test_consensusStartEndGapRegion()
	test_sortPosition()
	test_allRegions()
	test_regionsInReads()
	test_consensusByExon()
	test_writeConsensusByGapProfile()
	#~ cmdRm = "rm test/msaAllReads.fa"
	#~ subprocess.check_output(['bash','-c', cmdRm])
	#~ cmdRm = "rm test/corrected_by_MSA*"
	#~ subprocess.check_output(['bash','-c', cmdRm])
	#~ cmdRm = "rm test/exonChainFasta*"
	#~ subprocess.check_output(['bash','-c', cmdRm])
	#~ cmdRm = "rm test/msa*"
	#~ subprocess.check_output(['bash','-c', cmdRm])

	#tests on second file
	#test_computePOA2()
	#~ test_getMSA2()
	test_findGapStretches2()
	test_prolongateNstretches2()
	test_keepLongGapsOnly2()
	test_groupSimilarRegions2()
	test_makeDistinctGroups()
	test_findConsensusPositionsForGaps2()
	test_consensusStartEndGapRegion2()
	test_simplifyIncludedGaps()
	test_sortPosition2()
	test_allRegions2()
	test_regionsInReads2()
	# cmdRm = "rm test/msaAllReads.fa"
	# subprocess.check_output(['bash','-c', cmdRm])

	#~ #tests on third file
	#~ # test_computePOA3()
	#~ test_findConsensusPositionsForGaps3()
	#~ test_regionsInReads3()

	#~ #forth file
	test_regionsInReads4()

	#~ test_regionsInReads5()

	#~ test_regionsInReads7()
	
	#~ test_writeConsensusByGapProfile6()

	test_sample()


	print("\nAll test passed.")

