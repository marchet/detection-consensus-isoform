#!/usr/bin/env python

#----------------------------------------------------------------------#
#Isoform detection and consensus in ONT long reads using MSA
#Copyright (C) Camille Marchet

#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#contact: camille.marchet@irisa.fr
#----------------------------------------------------------------------#

from __future__ import print_function

try:
    from builtins import zip
    from builtins import str
    from builtins import object
except ImportError:
    pass
import argparse
import sys
import simplefasta
import sys
import os
import shlex, subprocess
from subprocess import Popen, PIPE, STDOUT
import re
from collections import Counter
from test import *


THRESH = 8
THRESH_REGIONS = 20
THRESHOLD_FOR_CONSENSUS = 3


# launch subprocess, used to call POA and some scripts
def subprocessLauncher(cmd, argstdout=None, argstderr=None,	 argstdin=None):
		args = shlex.split(cmd)
		p = subprocess.call(args, stdin = argstdin, stdout = argstdout, stderr = argstderr)
		return p

# detect whether reads are in fasta/or fastq
def detectFastaFastq(fileToRead):
	sequencefile = open(fileToRead, 'r')
	line = sequencefile.readline()
	if not line: 
		print("Can't open read file", fileToRead)
		exit(1)
	if line[0]!='@' and line[0]!='>': 
		print("File", fileToRead, "not correctly formatted")
		exit(1)
	fasta = True
	linesperread=2 #fasta by default
	if line[0]=='@':
		linesperread=4 # fastq
		fasta = False
	sequencefile.close()
	return fasta

# convert reads to fasta format using a script
def convertReadsToFasta(inFile, installDirectory, currentDirectory):
	cmdConvert = installDirectory + "/scripts/fastq2fasta.sh -q " + inFile + " -f " + currentDirectory + "/reads_fasta_for_msa.fasta"
	subprocessLauncher(cmdConvert)
	return currentDirectory + "/reads_fasta_for_msa.fasta"

# compute a consensus from a MSA with voting
def getConsensusFromAlignment(alignment):
	consensusTmp =  ""
	if len(alignment) > 0 and len(alignment[0]) > 1:
		for i in range(len(alignment[0][1][1])):
			colString = ""
			for r in alignment:
				seq = r[1][1]
				colString += seq[i]
			val = 0
			ntToKeep = '-'
			for nt in ['a', 'c', 'g', 't', '.']:
				tmpVal = colString.count(nt)
				if tmpVal > val:
					val = tmpVal
					ntToKeep = nt
			# special case for ".": if another caracter is present a lot, use this character instead of "."
			if ntToKeep == '.':
				val = len(alignment)/3.0
				for nt in ['a', 'c', 'g', 't']:
					tmpVal = colString.count(nt)
					if tmpVal > val:
						val = tmpVal
						ntToKeep = nt
			if ntToKeep != '.':
				consensusTmp  += ntToKeep
		return consensusTmp


# computes msa with POA
def getPOA(reads, matrixFile, installDirectory, outFileName, suffix = ""):
	cmdPOA = installDirectory + "/poa-graph/poa -reads_fasta " + reads + " -pathMatrix " + matrixFile
	subprocessLauncher(cmdPOA)
	cmdMv = "mv default_output_msa.fasta " + outFileName 
	subprocess.check_output(['bash','-c', cmdMv])

class MSA(object):
	def __init__(self):
		self.rows = []

		#created 2018/07/09:
		self.readsToGapsIndex = list()
		self.gapsList = list()
		self.gapsIndexToReadsIndex = list()

		self.readsToRegionsGaps = list()
		self.regionsList = list()
		self.regionsIndexToReadsIndex = list()

		self.isoforms = dict()
		self.readToConsensus = list()
		
	def getReadsCluster(self, cluster):
		readCluster = []
		for read in cluster:
			readCluster.append((self.rows[read][1][0], self.rows[read][1][1],))
		return readCluster
		

	def __str__(self):
		toW = ""
		for i,r in enumerate(self.rows):
			toW += ">" + str(i) + "\n" + str(r) + "\n"
		return toW[:-1]





	#load msa from aligned sequences stored in a vector
	def getMSA(self, alignment):
		for line in alignment:
			self.rows.append(line)
			self.readsToGapsIndex.append([])


	#find stretches of "." in each read of the msa
	def findGapStretches(self):
		positionsStretch = dict()
		# for each row of the MSA (read)
		for r,row in enumerate(self.rows):
			positionsStretch[r] = []
			countGap = 0
			prevInStretch = False
			positionsStretch[r].append([])
			# for each character at successive positions in the row
			for pos, nt in enumerate(row[1][1]):
				if nt == '.':
					prevInStretch = True
					countGap += 1
					if countGap > THRESH: # report a gap using start and end position
						if len(positionsStretch[r][-1]) == 2:
								positionsStretch[r][-1][1] = pos
						else:
							positionsStretch[r][-1] = [pos-THRESH, pos]
				else:
					if prevInStretch:
						positionsStretch[r].append([]) # a gap has ended. Prepare list for a future new gap. /!\ some are left empty, to remove afterwards
						prevInStretch = False
					countGap = 0
		positionsStretchDict = {}
		done = dict()
		# for each read in which a gap was reported
		maxIndex = 0
		for read in positionsStretch.keys():
			for gap in positionsStretch[read]:
				if gap != []:# exclude empty list
					if tuple(gap) not in done.keys(): 
						self.gapsList.append(gap)
						self.gapsIndexToReadsIndex.append([])
						index =  len(self.gapsList) - 1
						done[tuple(gap)] = index
						self.readsToGapsIndex[read].append(index)
						self.gapsIndexToReadsIndex[-1] = [read]
						maxIndex += 1
					else:
						index = done[tuple(gap)]
						self.readsToGapsIndex[read].append(index)
						self.gapsIndexToReadsIndex[index].append(read)
		

	#~ def prolongateNstretches(self):
		#~ self.gapsList = sorted(self.gapsList,  key=lambda x:x[0])
		#~ for read in range(len(self.readsToGapsIndex)):
			#~ if len(self.readsToGapsIndex[read]) > 1:
				#~ region1Index = 0
				#~ while region1Index < len(self.readsToGapsIndex[read]) - 1:

	# find gaps that are separated by small islands and merge
	def prolongateNstretches(self):
		#~ print("****",self.gapsList)
		#~ print("****",self.readsToGapsIndex)
		#~ input()
		#~ self.gapsList = sorted(self.gapsList,  key=lambda x:x[0])
		for read in range(len(self.readsToGapsIndex)):
			# for reads with more than one gap
			if len(self.readsToGapsIndex[read]) > 1:
				region1Index = 0
				merged = False
				startToKeep = None
				while region1Index < len(self.readsToGapsIndex[read]) - 1:
					#~ print(read, self.readsToGapsIndex[read])
					merged = False
					indexRegion1 = self.readsToGapsIndex[read][region1Index]
					indexRegion2 = self.readsToGapsIndex[read][region1Index+1]
					region1 = self.gapsList[indexRegion1] # first gap A
					region2 = self.gapsList[indexRegion2] # following gap B
					#~ if region2[0] - region1[1] < THRESH_REGIONS: # merge
					if region2[0] - region1[1] <= THRESH: # merge
						keep1 = False
						otherReads1 = self.gapsIndexToReadsIndex[indexRegion1]
						keep2 = False
						otherReads2 = self.gapsIndexToReadsIndex[indexRegion2]
						for r in otherReads1:
							if r < read:
								keep1 = True
						for r in otherReads2:
							if r < read:
								keep2 = True
						if keep1 and keep2: #both gaps are already used and not merged in some reads already passed
							#~ print("a")
							self.gapsList.append([region1[0], region2[1]]) # create a new gap
							self.readsToGapsIndex[read].remove(indexRegion1)
							self.readsToGapsIndex[read].remove(indexRegion2)
							self.readsToGapsIndex[read].append(len(self.gapsList) - 1)
							self.gapsIndexToReadsIndex[indexRegion1].remove(read)
							self.gapsIndexToReadsIndex[indexRegion2].remove(read)
							self.gapsIndexToReadsIndex.append([read])
							region1Index += 1
						elif keep1:
							#~ print("b")
							#~ print(self.gapsList)
							self.gapsList.append([region1[0], region2[1]]) # create a new gap
							self.readsToGapsIndex[read].remove(indexRegion2)
							self.readsToGapsIndex[read].append(len(self.gapsList) - 1)
							self.gapsIndexToReadsIndex[indexRegion2].remove(read)
							self.gapsIndexToReadsIndex.append([read])
							region1Index += 1
						else:  #simply replace the gaps by the merge of the two
							#~ print("c")
							self.gapsList[indexRegion1][1] = region2[1]
							self.gapsList[indexRegion2][0] = region1[0] # same info, TODO remove doublons after
							if len(self.readsToGapsIndex[read]) == 2:
								break
							else:
								region1Index += 1
						if region1Index == len(self.readsToGapsIndex[read]) - 2:
							break
					else:
						region1Index += 1


	def keepLongGapsOnly(self):
		# remove doublons + remove small gaps
		newGapsList = list()
		newReadsToGapsIndexList = list()
		for i in range(len(self.readsToGapsIndex)):
			newReadsToGapsIndexList.append([])
		newGapsIndexToReadsIndexList = list()
		#initialize
		#~ if self.gapsList[0][1] - self.gapsList[0][0] >= THRESH_REGIONS:
			#~ newGapsList.append(self.gapsList[0])
			#~ newGapsIndexToReadsIndexList.append(self.gapsIndexToReadsIndex[0])
			#~ for reads in newGapsIndexToReadsIndexList[0]:
				#~ newReadsToGapsIndexList[reads] = [0]
		# fill
		gapsIndex = 0
		while gapsIndex < len(self.gapsList):
			if (len(newGapsList) == 0 or not newGapsList[-1] == self.gapsList[gapsIndex] ): # not a doublon
				if not len(self.gapsIndexToReadsIndex[gapsIndex]) == 0:
					if self.gapsList[gapsIndex][1] - self.gapsList[gapsIndex][0] >= THRESH_REGIONS:
						newGapsList.append(self.gapsList[gapsIndex])
						newGapsIndexToReadsIndexList.append(self.gapsIndexToReadsIndex[gapsIndex])
						for reads in newGapsIndexToReadsIndexList[-1]:
							newReadsToGapsIndexList[reads].append(len(newGapsList) - 1)
			else:
				newGapsIndexToReadsIndexList[-1] += self.gapsIndexToReadsIndex[gapsIndex]
				for reads in self.gapsIndexToReadsIndex[gapsIndex]:
					newReadsToGapsIndexList[reads].append(len(newGapsList) - 1)
			gapsIndex += 1
		#remove doublons in newGapsIndexToReadsIndexList
		newGapsIndexToReadsIndexList2 = list()
		gapsIndex = 0
		
		while gapsIndex < len(newGapsIndexToReadsIndexList):
			newGapsIndexToReadsIndexList2.append([])
			prevRead = -1
			for reads in sorted(newGapsIndexToReadsIndexList[gapsIndex]):
				#~ print(reads)
				if reads != prevRead:
					prevRead = reads
					newGapsIndexToReadsIndexList2[-1].append(reads)
			gapsIndex += 1
		self.gapsList = newGapsList
		self.readsToGapsIndex = newReadsToGapsIndexList
		self.gapsIndexToReadsIndex = newGapsIndexToReadsIndexList2
						

	# find similar regions in the different reads
	def groupSimilarRegions(self):
		done = set()
		consensusRegions = []
		for gapsIndex in range(len(self.gapsList)):
			if gapsIndex not in done:
				consensusRegions.append([gapsIndex])
				done.add(gapsIndex)
				for gapsIndex2 in range(len(self.gapsList)):
					if gapsIndex != gapsIndex2:
						if abs(self.gapsList[gapsIndex][0] - self.gapsList[gapsIndex2][0]) <= THRESH_REGIONS:
							if abs(self.gapsList[gapsIndex][1] - self.gapsList[gapsIndex2][1]) <= THRESH_REGIONS:
								consensusRegions[-1].append(gapsIndex2)
								done.add(gapsIndex2)
		return consensusRegions

	def getElementsInGroup(self, index, consensusRegions):
		gapsIndexInGroup = consensusRegions[index]
		elt = []
		for index in gapsIndexInGroup:
			elt.append(self.gapsList[index])
		return elt

	def getDistanceGroup(self, region, elts):
		dist = 0
		gapRegion = self.gapsList[region]
		for gaps in elts:
			dist += abs(gapRegion[0] - gaps[0] + gapRegion[1] - gaps[1])
		return dist

	# if regions are in several groups, choose only one
	def makeDistinctGroups(self, consensusRegions):
		newConsensusRegions = []
		regionsToGroups = dict()
		if len(consensusRegions) > 1: #look for regions in different groups
			for index,group in enumerate(consensusRegions):
				for region in group:
					if region in regionsToGroups.keys():
						regionsToGroups[region].append(index) #index in consensusRegion vector
					else:
						regionsToGroups[region] = [index]
			#~ done = set()
			regionsToGroupToKeep = dict()
			for regions, groups in regionsToGroups.items(): #decide to place regions if they are in several groups
				if len(groups) > 1:
					distances = []
					for gr in groups:
						elts = self.getElementsInGroup(gr, consensusRegions) #get all gaps
						distances.append(self.getDistanceGroup(region, elts))
					groupToKeep = distances.index(min(distances)) #index of the group we keep
					regionsToGroupToKeep[regions] = groupToKeep
			for regions, group in regionsToGroupToKeep.items():
				for otherGroups in regionsToGroups[regions]:
					if otherGroups != group:
						consensusRegions[otherGroups].remove(regions)
		return consensusRegions
		


	# find positions most reported for a region
	def findPositionRegions(self, region, left):
		counts = Counter(region)
		localMax = 0
		posiMax = None
		for pos in region:
			if counts[pos] > localMax:  #positions that is the most reported among all for this region
				localMax = counts[pos] 
				posiMax = pos
		if posiMax is None:
			if left:
				return region[-1]
			else:
				return region[0]
		return posiMax

	# find the consensus positions for gaps and report to reads
	def findConsensusPositionsForGaps(self, consensusRegions):
		for reg in consensusRegions: # for each group of gaps
			if len(reg) > 1: #if we need to compute a consensus
				# look for most reported start and end positions to find a consensus for this group of gaps
				gaps = []
				for index in reg:
					gaps.append(self.gapsList[index])
				left = sorted([x[0] for x in gaps])
				right = sorted([x[1] for x in gaps])
				maxLeft = self.findPositionRegions(left, True)
				maxRight = self.findPositionRegions(right, False)
				listReads = []
				for index in reg:
					self.gapsList[index] = [maxLeft, maxRight]
					for read in self.gapsIndexToReadsIndex[index]:
						listReads.append(read)
				for index in reg:
					self.gapsIndexToReadsIndex[index] = listReads
				for reads in listReads:
					self.readsToGapsIndex[reads] = reg

		#remove doublons after consensus
		newGapsList = list()
		newReadsToGapsIndexList = list()
		for i in range(len(self.readsToGapsIndex)):
			newReadsToGapsIndexList.append([])
		newGapsIndexToReadsIndexList = list()
		for reg in consensusRegions: # for each group of gaps
			index = reg[0]
			newGapsList.append(self.gapsList[index])
			newGapsIndexToReadsIndexList.append(self.gapsIndexToReadsIndex[index])
			for reads in newGapsIndexToReadsIndexList[-1]:
				newReadsToGapsIndexList[reads].append(len(newGapsList)-1)

		self.gapsList = newGapsList
		self.readsToGapsIndex = newReadsToGapsIndexList
		self.gapsIndexToReadsIndex = newGapsIndexToReadsIndexList
			
			

	#if some start/end of different gap regions are very similar, make a consensus of them (for instance (57,209) and (147,211) -> (57,210) and (147,210)
	def consensusStartEndGapRegion(self):
		done = set()
		# modif 07/16
		startToGaps = []
		endToGaps = []
		###
		for gapsIndices in range(len(self.gapsList)):
			listSimilarStarts = [gapsIndices]
			listSimilarEnds = [gapsIndices]
			done.add(gapsIndices)
			for otherGapsIndices in range(len(self.gapsList)):
				if otherGapsIndices not in done:
					gap1 = self.gapsList[gapsIndices]
					gap2 = self.gapsList[otherGapsIndices]
					if abs(gap1[0] - gap2[0]) < THRESH: # similar starts
						listSimilarStarts.append(otherGapsIndices)
					if abs(gap1[1] - gap2[1]) < THRESH: # similar ends
						listSimilarEnds.append(otherGapsIndices)
			if len(listSimilarStarts) > 1:
				startToGaps.append(listSimilarStarts)
				listCoord = []
				for index in listSimilarStarts:
					listCoord.append(self.gapsList[index][0])
				position = int(sum(x for x in listCoord)/2)
				for index in listSimilarStarts:
					self.gapsList[index][0] = position
			if len(listSimilarEnds) > 1:
				endToGaps.append(listSimilarEnds)
				listCoord = []
				for index in listSimilarEnds:
					listCoord.append(self.gapsList[index][1])
				position = int(sum(x for x in listCoord)/2)
				for index in listSimilarEnds:
					self.gapsList[index][1] = position
		# if a start and a end are similar
		done = set()
		for gapsIndices in range(len(self.gapsList)):
			listSimilarStarts = set()
			listSimilarEnds = set()
			done.add(gapsIndices)
			for otherGapsIndices in range(len(self.gapsList)):
				if otherGapsIndices not in done:
					gap1 = self.gapsList[gapsIndices]
					gap2 = self.gapsList[otherGapsIndices]
					if abs(gap1[1] - gap2[0]) < THRESH: #same end for current gap and same start for the other
						listSimilarStarts.add(otherGapsIndices)
						listSimilarEnds.add(gapsIndices)
					if abs(gap1[0] - gap2[1]) < THRESH:
						listSimilarStarts.add(gapsIndices)
						listSimilarEnds.add(otherGapsIndices)
			if len(listSimilarStarts) > 0 and len(listSimilarEnds) > 0:
				listCoord = []
				for index1,index2 in zip(listSimilarStarts,listSimilarEnds):
					listCoord.append(self.gapsList[index1][0])
					listCoord.append(self.gapsList[index2][1])
				position = int(sum(x for x in listCoord)/2)
				for index in listSimilarStarts:
					self.gapsList[index][0] = position
				for index in listSimilarEnds:
					self.gapsList[index][1] = position
		return startToGaps, endToGaps


	# for a read that has both gaps [12,350] and [12,400] for instance, keep only the larger gap ([12,400]) in order to have redundance
	def simplifyIncludedGaps(self, startToGaps, endToGaps):
		for gapsList in startToGaps:
			#order them by increasing end position
			coordsList = [self.gapsList[gap] for gap in gapsList]
			sortedIndex = [i[0] for i in sorted(enumerate(coordsList), key=lambda x:x[1])]
			gapsIndexToSortedIndex = dict()  #gaps index and their ranks
			for gaps in range(len(gapsList)):
				gapsIndexToSortedIndex[gapsList[gaps]] = sortedIndex[gaps]
			readsToSeveralGaps = dict()
			for gaps in gapsList:
				for reads in self.gapsIndexToReadsIndex[gaps]: # for reads that have these gaps
					if reads in readsToSeveralGaps:
						readsToSeveralGaps[reads].append(gaps)
					else:
						readsToSeveralGaps[reads] = [gaps]
			listReadsToModify = []
			for read in readsToSeveralGaps.keys():
				if len(readsToSeveralGaps[read]) > 1: #a read contains several overlapping gaps
					listReadsToModify.append(read)
					toKeep = [None,-1]
					for gaps in readsToSeveralGaps[read]:
						if gapsIndexToSortedIndex[gaps] > toKeep[1]:
							toKeep = [gaps,gapsIndexToSortedIndex[gaps]]
			#keep only the larger gap:
			for gaps in readsToSeveralGaps[read]:
				for read in listReadsToModify:
					if gaps != toKeep[0]:
						self.readsToGapsIndex[read].remove(gaps)
						self.gapsIndexToReadsIndex[gaps].remove(read)

		#same for ends
		for gapsList in startToGaps:
			#order them by increasing start position
			coordsList = [self.gapsList[gap] for gap in gapsList]
			sortedIndex = [i[0] for i in sorted(enumerate(coordsList), key=lambda x:x[0])]
			gapsIndexToSortedIndex = dict()  #gaps index and their ranks
			for gaps in range(len(gapsList)):
				gapsIndexToSortedIndex[gapsList[gaps]] = sortedIndex[gaps]
			readsToSeveralGaps = dict()
			for gaps in gapsList:
				for reads in self.gapsIndexToReadsIndex[gaps]: # for reads that have these gaps
					if reads in readsToSeveralGaps:
						readsToSeveralGaps[reads].append(gaps)
					else:
						readsToSeveralGaps[reads] = [gaps]
			listReadsToModify = []
			for read in readsToSeveralGaps.keys():
				if len(readsToSeveralGaps[read]) > 1: #a read contains several overlapping gaps
					listReadsToModify.append(read)
					toKeep = [None, len(readsToSeveralGaps[read])]
					for gaps in readsToSeveralGaps[read]:
						if gapsIndexToSortedIndex[gaps] < toKeep[1]:
							toKeep = [gaps,gapsIndexToSortedIndex[gaps]]
			#keep only the larger gap:
			for gaps in readsToSeveralGaps[read]:
				for read in listReadsToModify:
					if gaps != toKeep[0]:
						self.readsToGapsIndex[read].remove(gaps)
						self.gapsIndexToReadsIndex[gaps].remove(read)

	# positions of start/end of exons for the whole data set in order
	def sortPosition(self):
		listPositionsTmp = []
		for gap in self.gapsList:
			for se, position in enumerate(gap):
				listPositionsTmp.append(position)
		listPositionsTmp = sorted(listPositionsTmp)
		listPositions = []
		#remove doublons
		#~ for elt in range(len(listPositionsTmp) - 1):
		elt = 0
		while elt < len(listPositionsTmp) - 1:
			if listPositionsTmp[elt] != listPositionsTmp[elt + 1]:
				#~ print(listPositionsTmp[elt], listPositionsTmp[elt + 1])
				#~ if abs(listPositionsTmp[elt] - listPositionsTmp[elt + 1]) < THRESH:
					#~ listPositions.append(int((listPositionsTmp[elt] + listPositionsTmp[elt + 1])/2))
					#~ elt+=1 #to skip two indexes
				#~ else:
					listPositions.append(listPositionsTmp[elt])
			if elt >= len(listPositionsTmp) - 2:
				#~ print(listPositionsTmp[elt+1])
				listPositions.append(listPositionsTmp[elt+1])
				break
			else:
				elt+=1
		return listPositions


	# define all regions in the data set
	def allRegions(self, listPositions):
		listRegions = []
		index = 0
		while index < len(listPositions) - 1:
			if index == 0:
				if listPositions[0] > THRESH:
					listRegions.append([0,listPositions[0]-1]) # first region before the gap
					listRegions.append([listPositions[0],listPositions[1]-1])
				else:
					listRegions.append([0,listPositions[1]-1]) # merge the few nt in the beginning to the gap that starts
			elif index == len(listPositions) - 2: #last interval
				if len(self.rows[0][1][1]) - listPositions[index+1] > THRESH: # more than a few nt after the last gap
					listRegions.append([listPositions[index],listPositions[index+1]-1])
					listRegions.append([listPositions[index+1], len(self.rows[0][1][1])-1])
				else:
					listRegions.append([listPositions[index],len(self.rows[0][1][1])-1]) # merge the few nt in the end
			else:
				listRegions.append([listPositions[index],listPositions[index+1]-1])
			index += 1
		if len(listPositions) == 2:
			if len(self.rows[0][1][1]) - listPositions[1] > THRESH: # more than a few nt after the last gap
					#~ listRegions.append([listPositions[0],listPositions[1]-1])
				listRegions.append([listPositions[1], len(self.rows[0][1][1])-1])
			else:
				listRegions.append([listPositions[1],len(self.rows[0][1][1])-1]) # merge the few nt in the end
		return listRegions


	# associate to each read the regions where it has gaps
	def regionsInReads(self, listRegions):
		#initialize
		for i in range(len(self.readsToGapsIndex)):
			self.readsToRegionsGaps.append([])
		for i in range(len(listRegions)):
			self.regionsList.append(listRegions[i])
			self.regionsIndexToReadsIndex.append([])

		for read in range(len(self.readsToGapsIndex)):
			regionIndex = 0
			inGap = False
			for gapsIndex in self.readsToGapsIndex[read]:
				gap = self.gapsList[gapsIndex]
				while regionIndex < len(listRegions):
					if regionIndex == 0:
						if listRegions[regionIndex][1] + 1 == gap[1]: #this read has the first gap
							self.readsToRegionsGaps[read].append(regionIndex)
							self.regionsIndexToReadsIndex[regionIndex].append(read)
							regionIndex += 1
							break
					elif regionIndex == len(listRegions) - 1:
						if listRegions[regionIndex][0] == gap[1]: #this read has the last gap
							self.readsToRegionsGaps[read].append(regionIndex)
							self.regionsIndexToReadsIndex[regionIndex].append(read)
							regionIndex += 1
							break
					else:
						if listRegions[regionIndex][0] == gap[0]:
							self.readsToRegionsGaps[read].append(regionIndex)
							self.regionsIndexToReadsIndex[regionIndex].append(read)
							inGap = True
							if listRegions[regionIndex][1] == gap[1] - 1:
								regionIndex += 1
								inGap = False
								break
						else:
							if inGap:
								self.readsToRegionsGaps[read].append(regionIndex)
								self.regionsIndexToReadsIndex[regionIndex].append(read)
								if listRegions[regionIndex][1] == gap[1] - 1:
									regionIndex += 1
									inGap = False
									break
								
					regionIndex += 1
		
		
				

	
	def simpleRegion(self):
		self.regionsList = [[0,len(self.rows[0][1][1])-1]]
		self.readsToRegionsGaps = [[]] * len(self.rows)
		self.regionsIndexToReadsIndex = [[]]

	# cluster group of reads that have sequences (not gaps) in the same regions
	def clusterIsoforms(self):
		for read in range(len(self.readsToRegionsGaps)):
			gaps = tuple(self.readsToRegionsGaps[read])
			#~ print(gaps)
			if gaps in self.isoforms:
				self.isoforms[gaps].append(read)
			else:
				self.isoforms[gaps] = [read]
		#~ print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%", len(self.isoforms))
	

	# get a list of reads that have a sequence (not gap) in a given region
	def getListReadsWithSequenceInRegion(self, regionIndex):
		listReads = []
		for reads in range(len(self.readsToRegionsGaps)):
			if reads not in self.regionsIndexToReadsIndex[regionIndex]:
				listReads.append(reads)
		return listReads

	# compute consensus by exon
	def consensusByExon(self,installDirectory, outputDirPath):
		#initialize
		for i in range(len(self.readsToGapsIndex)):
			self.readToConsensus.append([])

		#for all regions
		for regionIndex in range(len(self.regionsList)):
			listReads = self.getListReadsWithSequenceInRegion(regionIndex) #get reads that have a sequence in this region for consensus
			if not len(listReads) == 0:
				start = self.regionsList[regionIndex][0]
				end = self.regionsList[regionIndex][1]
				exonSubsequences = self.extractSubSequencesListReads(start, end, listReads)#extract sequences for reads
				if len(exonSubsequences) >= THRESHOLD_FOR_CONSENSUS: # if enough sequences to compute a consensus 
					exonAlignment = self.computePartialMsa(installDirectory, outputDirPath,exonSubsequences, regionIndex) # align the seq of the region
					consensusRegion = getConsensusFromAlignment(exonAlignment) # get a consensus from alignment
					for read in listReads:
						self.readToConsensus[read].append(consensusRegion) # add this consensus to the subsequences of the read
				else:
					for read in listReads: # keep the initial sequence in the read
						self.readToConsensus[read].append(exonSubsequences[read])

	# extract a read from the msa
	def extractSeqFromMSA(msaRow):
		sequence = ''
		for nt in msaRow:
			if nt != "." and nt != "-":
				sequence += nt
		return sequence

	# extract a set of sequences from the same start/end positions in the MSA
	def extractSubSequencesListReads(self, start, end, listReads): # start and end included
		subsequencesTmp = []
		subsequences = []
		for r in self.rows:
			subsequencesTmp.append(r[1][1][start:end+1])
		for seq in subsequencesTmp:
			seqExon = ''
			for nt in seq:
				if nt != "." and nt != "-":
					seqExon += nt
			if not len(seqExon) < THRESH_REGIONS - 10:
				subsequences.append(seqExon)
		return subsequences



	# write results in files
	def writeConsensusByGapProfile(self, installDirectory, outputDirPath):
		if os.path.isfile(outputDirPath + "/all_corrected.fa"):
			subprocess.check_output(['bash','-c', "rm " + outputDirPath +  "/all_corrected.fa"])
		readsOutput = []
		self.clusterIsoforms()
		# main file
		out = open(outputDirPath + "/all_corrected.fa", 'w')
		for i, reads in enumerate(self.isoforms.values()):
			# one file per detected isoform
			outi = open(outputDirPath + "/corrected_by_MSA" + str(i) +".fa", 'w')
			#~ print("############################################################", i)
			for r in reads:
				sequence = ''
				for exon in self.readToConsensus[r]:
					sequence += exon
				outi.write(">" + self.rows[r][1][0] + "\n"  + sequence.upper() + "\n")
				out.write(">" + self.rows[r][1][0] + "\n"  + sequence.upper() + "\n")
			outi.close()
		out.close()
		

	# compute MSA per exon 
	def computePartialMsa(self,  installDirectory, outputDirPath, exonSubsequences, regionIndex):
		exonFastaFile = open(outputDirPath + "/exonChainFasta" + str(regionIndex) + ".fa", 'w')
		for index,exon in enumerate(exonSubsequences):
			exonFastaFile.write(">" + str(index) + "exon\n")
			exonFastaFile.write(exon+"\n")
		exonFastaFile.close()
		subAlignment =  computePOA(outputDirPath + "/exonChainFasta" + str(regionIndex) + ".fa", installDirectory, outputDirPath, "Exon")
		return subAlignment
		


		

def getFasta(subAlignment):
	fasta = []
	i = 0
	for i,s in enumerate(subAlignment):
		if '>' not in s:
			tu = (str(i), s,)
			fasta.append(tu)
			i += 1
	return fasta


def findPositionsInRead(read, msaread, start, end):
	nbGapsStart = msaread[1][1][:start].count(".")
	nbGapsIn = msaread[1][1][start:end+1].count(".")
	posStart = start - nbGapsStart
	posEnd = end - nbGapsIn -nbGapsStart
	return [posStart, posEnd]

def computePOA(readFileName, installDirectory, outputDirPath, typeA = "Splice", suffix=""):
	matrixFileName = installDirectory + "/poa-graph/blosum80.mat"
	if typeA == "Splice":
		outFileName = outputDirPath + "/msaAllReads.fa"
		getPOA(readFileName, matrixFileName, installDirectory, outFileName)
	elif typeA == "Exon":
		outFileName = outputDirPath + "/msa" + suffix + ".fa"
		getPOA(readFileName, matrixFileName, installDirectory, outFileName, suffix)
	msa = open(outFileName, 'r')
	msaFasta = simplefasta.readfasta(msa)
	alignments = getFasta(msaFasta)
	return alignments



def writeReads(reads, fileName):
	out = open(fileName, 'w')
	for read in reads:
		out.write(">" + read[1][0] + "\n" + read[1][1] + "\n")
	out.close()

# print a warning message
def printWarningMsg(msg):
	print("[Warning] " + msg)


def copyReads(reads, outDir):
	out = open(outDir + "/reads.fa", 'w')
	with open(reads) as inFile:
		for line in inFile:
			#~ line = line.rstrip()
			if ">" in line:
				out.write(line)
			else:
				out.write(line.rstrip().lower()+"\n")
	out.close()

def tests():
	runTests()

if __name__ == "__main__":
	##### parse arguments #####
	currentDirectory = os.path.dirname(os.path.abspath(sys.argv[0]))
	installDirectory = os.path.dirname(os.path.realpath(__file__))
	parser = argparse.ArgumentParser()
	parser.add_argument('-r', nargs='?', type=str, action="store", dest="infile", help="Reads (one line fasta format)")
	parser.add_argument('-t','--tests', action="store",dest="doTests", nargs='?', type=bool, default=False, help='Perform tests')
	#~ parser.add_argument('-g','--globalAlign', action='store_true', help='Global alignment (default), or  local alignment')
	#~ parser.add_argument('-H','--html', nargs='?', type=argparse.FileType('w'), default='poa.html', help='html output')
	parser.add_argument('-c','--correctionMode', action="store",	dest="correctionMode", nargs='?', type=str, default='exon', help='Correction mode. Either compute a consensus per detected exon chain (mode "exon") or by detected isoform ("isoform", mode "exon", default)')
	parser.add_argument('-s','--sparc', action="store",	dest="sparc", nargs='?', type=bool, default=False, help='For test only. Use sparc to generate consensus')
	parser.add_argument('-o','--output', action="store",dest="outputDir", nargs='?', type=str, default=None, help='Path to output directory')
	args = parser.parse_args()
	if len(sys.argv) < 2:
		parser.print_help()
		sys.exit()
	doTests = args.doTests
	if doTests:
		tests()
		sys.exit()
	outputDirPath = installDirectory + "/results"
	if args.outputDir is not None:
		outputDirPath = args.outputDir
	if not os.path.exists(outputDirPath):
		os.mkdir(outputDirPath)
	else:
		printWarningMsg(outputDirPath+ " directory already exists, we will use it.")
		try:
			#~ cmdRm = "(cd " + outputDirPath + " && rm *)"
			cmdRm = "(cd " + outputDirPath + " && rm exonChain* reads.fa corrected_by_MSA* all_corrected.fa)"
			subprocess.check_output(['bash','-c', cmdRm])
		except subprocess.CalledProcessError:
			pass
	inFileTmp = args.infile
	seqNo = 0
	if args.sparc:
		correctionMode = "isoform"
	else:
		correctionMode = args.correctionMode
	##### read fasta file #####
	print("Reading fasta file...")
	isFasta = detectFastaFastq(inFileTmp)
	if not isFasta:
		inFileTmp = convertReadsToFasta(inFileTmp, installDirectory, currentDirectory)
	#copy of reads in lower case to pass to msa
	copyReads(inFileTmp, outputDirPath)
	##### compute alignment and build msa matrix from alignment #####
	print("Computing MSA...")
	alignments = computePOA(outputDirPath + "/reads.fa", installDirectory, outputDirPath)
	msa = MSA()
	msa.getMSA(alignments)
	##### find gap stretches in matrix #####
	print("Finding motifs in MSA...")
	msa.findGapStretches()
	msa.prolongateNstretches()
	msa.keepLongGapsOnly()
	##### find regions and associate to reads #####
	if len(msa.gapsList) > 0:
		regionsForConsensus = msa.groupSimilarRegions()
		regionsForConsensus = msa.makeDistinctGroups(regionsForConsensus)
		msa.findConsensusPositionsForGaps(regionsForConsensus)
		startToGaps, endToGaps = msa.consensusStartEndGapRegion()
		msa.simplifyIncludedGaps(startToGaps, endToGaps)
		listPositions = msa.sortPosition()
		listRegions = msa.allRegions(listPositions)
		msa.regionsInReads(listRegions)
	else:
		msa.simpleRegion()
	print("Computing and writing consensus...")
	##### find consensus for each region #####
	msa.consensusByExon(installDirectory, outputDirPath)
	msa.writeConsensusByGapProfile(installDirectory, outputDirPath)
	print("All done. Results in " + outputDirPath)


# todo : can we see if a read is too different from others ?
