#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <iterator>
#include <ctime>
#include <unordered_map>
#include <algorithm>
#include <cmath>
#include <chrono>
#include <unordered_set>
#include <set>
#include <algorithm>
#include <map>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sstream>
#include <mutex>
#include <functional>
#include <utility>
#include <sstream>


using namespace std;


char launch_command(const char* cmd, bool exit_if_fails=true) {
       cout << "\t" << cmd << endl << flush;
       if (system(cmd) != 0) {
              if  (exit_if_fails){
                     cerr << " \t FAILED command " << cmd << "  EXIT" << endl;
                     exit(1);
              }
       }      
       return 1;
}

vector<string> split(const string &s, char delim){
	stringstream ss(s);
	string item;
	vector<string> elems;
	while (getline(ss, item, delim)) {
		elems.push_back(move(item)); 
	}
	return elems;
}

vector<string> getFilesNames(ifstream& fileList){
	string file;
	vector<string> fastaFilesList;
	while (not fileList.eof()){
		getline(fileList, file);
		fastaFilesList.push_back(file);
	}
	return fastaFilesList;
}


void launchParallel(vector<string>& fastaFilesList){
	string cmd;
	char c;
	uint fileNo(0);
	#pragma omp for
	for (fileNo = 0; fileNo < fastaFilesList.size(); ++fileNo){
		if (not fastaFilesList[fileNo].empty()){
			stringstream sscmd;
			sscmd << "python3 analyze_MSAv2.py -r " << fastaFilesList[fileNo] << " -o results" << fileNo; //todo ouput file
			cmd = sscmd.str();
			c = launch_command(cmd.c_str(), false);
		}
	}
}


int main(int argc, char *argv[]) {
	if(argc < 2){
		exit(0);
	}
	string inFileName(argv[1]);
	ifstream fileList(inFileName);
	vector<string> fastaFilesList(getFilesNames(fileList));
	launchParallel(fastaFilesList);
	return 0;
}
