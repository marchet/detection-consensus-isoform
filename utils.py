#!/usr/bin/env python3
import sys
import os
import shlex, subprocess
from subprocess import Popen, PIPE, STDOUT
import re

def checkWrittenFiles(files):
	allFilesAreOK = True
	if not os.path.isfile(files):
		print("[ERROR] There was a problem writing \"" + files + "\".")
		allFilesAreOK = False
	if not allFilesAreOK:
		dieToFatalError("One or more files could not be written.")

# check if file exists and is not empty
def checkIfFile(pathToFile):
	if not(os.path.exists(pathToFile) and os.path.getsize(pathToFile) > 0):
		return False
	return True


def createDir(outDir):
	if not os.path.exists(outDir):
		os.mkdir(outDir)
	else:
		for dirpath, dirnames, files in os.walk(outDir):
			if files:
				cmdRm = "rm " + outDir + "/*"
				subprocess.check_output(['bash','-c', cmdRm])
